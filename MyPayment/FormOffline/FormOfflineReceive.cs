﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using MyPayment.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace MyPayment.FormOffline
{
    public partial class FormOfflineReceive : Form
    {
        //private static string ReportPath = ConfigurationManager.AppSettings["ReportPath"].ToString();
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        private static double VAT = double.Parse(ConfigurationManager.AppSettings["VAT"].ToString());
        private static OffLineClass OFC = new OffLineClass();

        public CultureInfo usCulture = new CultureInfo("en-US");
        public string CustomerName;
        public double Countamount;
        public double CountamountAll;
        public bool GB_IsReadBarcode = false;
        public string GB_StrQrCode = "";
        public string GB_Chno = "";
        ResultCostTDebt resul = new ResultCostTDebt();
        ArrearsController arr = new ArrearsController();
        List<ClassCheque> _cheque = new List<ClassCheque>();
        List<ClassListGridview> _lstGrid = new List<ClassListGridview>();
        public byte Key_Error;
        public string qNo;
        public FormOfflineReceive()
        {
            InitializeComponent();
        }

        private void FormOfflineReceive_Load(object sender, EventArgs e)
        {
            try
            {
                //GlobalClass.BranchId = "013";
                //GlobalClass.BranchName = "ราษฎร์บูรณะ";
                //GlobalClass.CashierNo = "7";
                //GlobalClass.UserName = "ทดสอบ ออกมาดี";
                //GlobalClass.UserCode = "18922";


                SetControl();

            }
            catch (Exception ex)
            {

            }

        }

        public void SetControl()
        {
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;



        }

        public void SetAmount(string Amount)
        {
            try
            {
                this.txtAmount.Text = Amount;
            }
            catch (Exception ex)
            {

            }
        }

        private void ClearChequeData()
        {
            try
            {
                txtCheque.Text = "0.00";
                txtChequeNumber.Text = "";
                dpChequeDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtBankName.Text = "";
                txtChequeName.Text = "";
                txtChequeTel.Text = "";
                txtBankId.Text = "";
                chCashierCheque.Checked = false;


                this.btnAdd.Enabled = true;
                this.btnUpdate.Enabled = false;
            }
            catch (Exception ex)
            {

            }

        }
        private void ClearDataGrid()
        {
            try
            {
                GridDetail.Columns.Clear();


            }
            catch (Exception ex)
            {

            }
        }

        private void ClearDataItem()
        {
            try
            {

                txtAmount.Text = "";
                txtCash.Text = "";


                txtChequeNumber.Text = "";
                txtBankId.Text = "";
                txtBankName.Text = "";
                txtCheque.Text = "";
                txtChequeName.Text = "";
                txtChequeTel.Text = "";
                chCashierCheque.Checked = false;
                btnAdd.Enabled = true;
                btnUpdate.Enabled = false;


                lblAmountCheq.Text = "0.00";
                lblAmountTotalCheq.Text = "0.00";
                lblAmountReceived.Text = "0.00";
                txtTelContact.Text = "";
                lblAmountChange.Text = "0.00";

            }
            catch (Exception ex)
            {

            }
        }
        private void AddChequeMain()
        {
            try
            {
                if (CheckTextEmpty())
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    if (string.IsNullOrEmpty(this.GB_Chno) && CheckData())
                        MessageBox.Show("ข้อมูลเลขที่เช็คและรหัสธนาคาร/สาขา ไม่สามารถซ้ำกันได้ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    else
                    {
                        bool status = false;
                        double AmountCredit = 0;
                        double TotalAmountCheq = 0;
                        double TotalAmount = 0;

                        if (lblAmountTotalCheq.Text != "")
                            TotalAmountCheq = Convert.ToDouble(lblAmountTotalCheq.Text);
                        if (txtCash.Text != "")
                            TotalAmount = Convert.ToDouble(txtCash.Text);

                        if (this.GridDetail.SelectedRows.Count > 0)
                        {
                            if (TotalAmount == 0 && AmountCredit == 0 && TotalAmountCheq > 0)
                            {
                                CountAmountCheq();
                                double amountSum = (TotalAmountCheq + Convert.ToDouble(txtCheque.Text)) - Convert.ToDouble(txtAmount.Text);
                                if (amountSum > 10)
                                    status = true;
                            }
                        }
                        else
                        {
                            if (TotalAmount == 0 && AmountCredit == 0 && Convert.ToDouble(txtCheque.Text) > 0)
                            {
                                double amountSum = Convert.ToDouble(txtCheque.Text) - Convert.ToDouble(txtAmount.Text);
                                if (amountSum > 10)
                                    status = true;
                            }
                        }
                        if (status)
                        {
                            MessageBox.Show("มูลค่าเช็คที่ชำระเกินจากจำนวนเงินที่ต้องชำระมากกว่า 10 บาทไม่สามารถรับได้ ", "ชำระเกิน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        else
                        {

                            if (!string.IsNullOrEmpty(this.GB_Chno))
                            {
                                var rec = _cheque.Where(m => m.Chno == this.GB_Chno).FirstOrDefault();
                                _cheque.Remove(rec);
                                this.GB_Chno = "";
                            }
                            /***********************/
                            ClassCheque cheque = new ClassCheque();
                            cheque.UserId = GlobalClass.UserCode;
                            cheque.Chno = txtChequeNumber.Text.Trim();
                            cheque.Bankno = txtBankId.Text.Trim();
                            cheque.Bankname = txtBankName.Text.Trim();
                            cheque.Chdate = dpChequeDate.Text.ToString();
                            cheque.Chamount = txtCheque.Text.Trim();
                            cheque.Chname = txtChequeName.Text.Trim();
                            cheque.Tel = txtChequeTel.Text.Trim();
                            cheque.Chcheck = chCashierCheque.Checked;
                            _cheque.Add(cheque);


                            var source = new BindingSource(_cheque, null);

                            this.GridDetail.DataSource = source;


                            CountamountAll = _cheque.Sum(m => double.Parse(m.Chamount));
                            //GlobalClass.LstCheque = _cheque;
                            //foreach (var item in GlobalClass.LstCheque)
                            //{
                            //    CountamountAll += Convert.ToDouble(item.Chamount);
                            //}
                            /***********************/


                            CountAmountCheq();
                            SumAmount();

                            ClearChequeData();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void SumAmount()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtAmount.Text.Trim()) && double.Parse(txtAmount.Text.Trim()) >= 0.00)
                {
                    double totalAmount = 0;
                    double amount = 0;
                    double amountCheque = 0;
                    double amountCreditCard = 0;
                    double sumAmount = 0;
                    if (txtCash.Text != "")
                        amount = Convert.ToDouble(txtCash.Text);
                    if (lblAmountTotalCheq.Text != "0.00")
                        amountCheque = Convert.ToDouble(lblAmountTotalCheq.Text);


                    totalAmount = amount + amountCheque + amountCreditCard;
                    lblAmountReceived.Text = totalAmount.ToString("#,###,###,##0.00");
                    if (totalAmount > 0)
                        sumAmount = totalAmount - Convert.ToDouble(txtAmount.Text.Trim());
                    if (sumAmount < 0)
                    {
                        label25.Text = sumAmount.ToString("#,###,###,##0.00");
                        lblAmountChange.Text = "00";
                    }
                    else
                    {
                        lblAmountChange.Text = sumAmount.ToString("#,###,###,##0.00");
                        label25.Text = "0.00";
                    }
                }

                if (double.Parse(label25.Text.Trim()) == 0)
                {
                    this.btnConfirm.Enabled = true;
                }
                else
                {
                    this.btnConfirm.Enabled = false;
                }



            }
            catch (Exception ex)
            {

            }

        }
        public void CountAmountCheq()
        {
            double Amount = 0;
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                Amount += Convert.ToDouble(GridDetail.Rows[i].Cells["colAmount"].Value);
            }
            lblAmountTotalCheq.Text = Amount.ToString("#,###,###,##0.00");
            lblAmountCheq.Text = Amount.ToString("#,###,##0.00");
        }
        private bool CheckTextEmpty()
        {
            bool status = false;
            if (txtChequeNumber.Text == string.Empty)
                status = true;
            if (txtBankId.Text == string.Empty)
                status = true;
            if (txtBankName.Text == string.Empty)
                status = true;
            if (dpChequeDate.Text == string.Empty)
                status = true;
            if (txtCheque.Text == string.Empty)
                status = true;
            else
            {
                var Cheque = double.Parse(txtCheque.Text);
                if (Cheque <= 0.0)
                {
                    status = true;
                }
            }
            if (txtChequeName.Text == string.Empty)
                status = true;
            if (txtChequeTel.Text == string.Empty)
                status = true;
            return status;
        }
        private bool CheckData()
        {
            bool tf;
            var checkData = _cheque.Where(c => c.Chno == txtChequeNumber.Text).FirstOrDefault();
            if (checkData != null)
                tf = true;
            else
                tf = false;
            return tf;
        }
        public void AddChequeToList()
        {
            if (_cheque != null)
            {
                List<ClassCheque> _lst = new List<ClassCheque>();
                foreach (var item in _cheque)
                {
                    ClassCheque cheque = new ClassCheque();

                    cheque.Chno = item.Chno;
                    cheque.Bankno = item.Bankno;
                    cheque.Bankname = item.Bankname;
                    cheque.Chdate = item.Chdate;
                    cheque.Chamount = item.Chamount;
                    cheque.Chname = item.Chname;
                    cheque.Tel = item.Tel;
                    cheque.Chcheck = item.Chcheck;
                    _lst.Add(cheque);
                }
                ClassCheque chequeNew = new ClassCheque();

                chequeNew.Chno = txtChequeNumber.Text.Trim();
                chequeNew.Bankno = txtBankId.Text.Trim();
                chequeNew.Bankname = txtBankName.Text.Trim();
                chequeNew.Chdate = dpChequeDate.Text.ToString();
                chequeNew.Chamount = txtCheque.Text.Trim();
                chequeNew.Chname = txtChequeName.Text.Trim();
                chequeNew.Tel = txtChequeTel.Text.Trim();
                chequeNew.Chcheck = chCashierCheque.Checked;
                _lst.Add(chequeNew);
                _cheque = _lst;
            }
            else
            {
                ClassCheque cheque = new ClassCheque();

                cheque.Chno = txtChequeNumber.Text.Trim();
                cheque.Bankno = txtBankId.Text.Trim();
                cheque.Bankname = txtBankName.Text.Trim();
                cheque.Chdate = dpChequeDate.Text.ToString();
                cheque.Chamount = txtCheque.Text.Trim();
                cheque.Chname = txtChequeName.Text.Trim();
                cheque.Tel = txtChequeTel.Text.Trim();
                cheque.Chcheck = chCashierCheque.Checked;
                _cheque.Add(cheque);
            }


            this.GridDetail.DataSource = _cheque;

            GlobalClass.LstCheque = _cheque;
            foreach (var item in GlobalClass.LstCheque)
            {
                CountamountAll += Convert.ToDouble(item.Chamount);
            }
        }
        private void GridDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    ClassCheque item = new ClassCheque();
                    string Chno = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                    item = _cheque.Where(m => m.Chno == Chno).FirstOrDefault();
                    /***********************/
                    txtChequeNumber.Text = item.Chno;
                    txtBankId.Text = item.Bankno;
                    txtBankName.Text = item.Bankname;
                    dpChequeDate.Text = item.Chdate;
                    txtCheque.Text = item.Chamount;
                    txtChequeName.Text = item.Chname;
                    txtChequeTel.Text = item.Tel;


                    this.GB_Chno = item.Chno;
                    this.btnUpdate.Enabled = true;
                    this.btnAdd.Enabled = false;
                    return;
                }
                if (e.ColumnIndex == 1)
                {
                    string Chno = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                    List<ClassCheque> LstNewCheque = new List<ClassCheque>();
                    LstNewCheque = _cheque.Where(m => m.Chno != Chno).ToList();
                    _cheque = LstNewCheque;
                    this.GridDetail.DataSource = _cheque;

                    GlobalClass.LstCheque = _cheque;
                    foreach (var item in GlobalClass.LstCheque)
                    {
                        CountamountAll += Convert.ToDouble(item.Chamount);
                    }
                    CountAmountCheq();
                    SumAmount();
                    return;
                }

                //if (e.ColumnIndex == 1)
                //{
                //    FormArrears.FormMapCheque detail = new FormArrears.FormMapCheque();
                //    detail.labelNumber.Text = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                //    detail.labelNo.Text = GridDetail.CurrentRow.Cells["ColNo"].Value.ToString();
                //    detail.labelName.Text = GridDetail.CurrentRow.Cells["ColName"].Value.ToString();
                //    detail.labelDate.Text = GridDetail.CurrentRow.Cells["ColDate"].Value.ToString();
                //    detail.labelAmount.Text = GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString();
                //    detail.labelPayeeName.Text = GridDetail.CurrentRow.Cells["ColPayeeName"].Value.ToString();
                //    detail.labelTel.Text = GridDetail.CurrentRow.Cells["ColTel"].Value.ToString();
                //    detail.CheckCh.Checked = Convert.ToBoolean(GridDetail.CurrentRow.Cells["colchCash"].Value.ToString());
                //    detail.labelBalance.Text = (GridDetail.CurrentRow.Cells["ColBalance"].Value.ToString() != "") ? GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString() : GridDetail.CurrentRow.Cells["ColBalance"].Value.ToString();
                //    detail.ShowDialog();
                //    detail.Dispose();
                //    LoadData();
                //}

            }
            catch (Exception ex)
            {

            }

        }
        public void LoadData()
        {


            if (GlobalClass.LstCheque != null)
                GridDetail.DataSource = GlobalClass.LstCheque;


            /***************************************/




        }

        public void SetDataListGridview()
        {
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                ClassListGridview grid = new ClassListGridview();
                DataGridViewTextBoxCell amountChequeCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colAmount"];
                DataGridViewTextBoxCell ChequeNoCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNumber"];
                grid.ChNo = ChequeNoCell.EditedFormattedValue.ToString();
                grid.ChAmount = amountChequeCell.EditedFormattedValue.ToString();
                _lstGrid.Add(grid);
            }
        }


        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {


                AddChequeMain();


            }
            catch (Exception ex)
            {

            }
        }

        private void TestAddCheque()
        {
            try
            {
                ClassCheque cheque = new ClassCheque();

                cheque.Chno = txtChequeNumber.Text.Trim();
                cheque.Bankno = txtBankId.Text.Trim();
                cheque.Bankname = txtBankName.Text.Trim();
                cheque.Chdate = dpChequeDate.Text.ToString();
                cheque.Chamount = txtCheque.Text.Trim();
                cheque.Chname = txtChequeName.Text.Trim();
                cheque.Tel = txtChequeTel.Text.Trim();
                cheque.Chcheck = chCashierCheque.Checked;
                _cheque.Add(cheque);
            }
            catch (Exception ex)
            {

            }
        }



        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                // OFC.ReadQrCode("");
                AddChequeMain();
            }
            catch (Exception ex)
            {

            }
        }







        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }

                SumAmount();
            }
            catch (Exception ex)
            {

            }
        }

        private void txtCash_TextChanged(object sender, EventArgs e)
        {
            //if (System.Text.RegularExpressions.Regex.IsMatch(txtCash.Text, "^[0-9]"))
            //    SumAmount();

            try
            {
                //  txtCash.Text = string.Format("{0:#,##0.00}", double.Parse(txtCash.Text));
                SumAmount();
            }
            catch (Exception ex)
            {

            }
        }
        private void txtCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void txtCheque_KeyPress(object sender, KeyPressEventArgs e)
        {

            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {

            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {


                AddChequeMain();


            }
            catch (Exception ex)
            {

            }
        }


        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 453;
                }
                string strYear = year + DateTime.Now.ToString("MMddHHmmss");
                string strDateTime = strYear;

                if (this.txtTelContact.Text.Trim() == "")
                {
                    MessageBox.Show("รบกวนระบุหมายเลขติดต่อกลับ", "แจ้งเตือน");

                    return;
                }


                /*******************************************/

                List<ItemtSummaryChequeModel> LstSummaryCheque = new List<ItemtSummaryChequeModel>();
                ItemOfflineModel OfflineRec = new ItemOfflineModel();
                ItemtSummaryChequeModel SummaryChequeRec = new ItemtSummaryChequeModel();
                if (_cheque != null && _cheque.Count > 0)
                {
                    foreach (var item in _cheque)
                    {
                        SummaryChequeRec = new ItemtSummaryChequeModel();

                        SummaryChequeRec.UserId = item.UserId;
                        SummaryChequeRec.ChequeNumber = item.Chno;
                        SummaryChequeRec.ChequeName = item.Chname;
                        SummaryChequeRec.BankId = item.Bankno;
                        SummaryChequeRec.BankName = item.Bankname;
                        SummaryChequeRec.ChequeDate = item.Chdate;
                        SummaryChequeRec.Cheque = item.Chamount;
                        LstSummaryCheque.Add(SummaryChequeRec);
                    }

                    OFC.WriteChequeOffLineToCSV(LstSummaryCheque, strDateTime);
                }

                string ServiceType = "A";


                /*********************/
                //FormProcessingFeeElectricity FeeElectServiceType = new FormProcessingFeeElectricity();

                //FeeElectServiceType.ShowDialog();
                //FeeElectServiceType.Dispose();
                //ServiceType = GlobalClass.Status ? "A" : "D";


                //if(ServiceType == "A")
                //{
                //    FormInputMoney frmInputMoney = new FormInputMoney();

                //    frmInputMoney.ShowDialog();
                //    frmInputMoney.Dispose();

                //}
                /*********************/


                var LstShortTaxInviceSelectedModel = GlobalClass.LstShortTaxInviceSelectedModel;

                ItemOfflineLogToServiceModel OfflineLogToServiceRec = new ItemOfflineLogToServiceModel();

                List<ItemOfflineModel> LstOfflineRec = new List<ItemOfflineModel>();
                string Transact = DateTime.Now.ToString("yyyyMMddHHmmss");
                bool isFirst = true;
                foreach (var item in LstShortTaxInviceSelectedModel)
                {
                    OfflineRec = new ItemOfflineModel();
                    OfflineRec.UserId = GlobalClass.UserCode;
                    OfflineRec.PaymentDate = DateTime.Now.ToString("dd/MM/yyyy");
                    OfflineRec.Time = DateTime.Now.ToString("HH:mm");
                    OfflineRec.Branch = GlobalClass.BranchName;
                    OfflineRec.UserId = GlobalClass.UserCode;
                    OfflineRec.CashierNo = GlobalClass.CashierNo;

                    OfflineRec.ContractAccount = item.ContractAccount.ToString();
                    OfflineRec.InvoiceNumber = item.InvoiceNumber != null ? item.InvoiceNumber.ToString() : "";
                    OfflineRec.TaxInvoice = item.BillNumber;

                    OfflineRec.Amount = item.AmountIncVat.ToString();

                    OfflineRec.ServiceType = ServiceType;

                    if (isFirst)
                    {
                        OfflineRec.Cash = txtCash.Text.Trim();
                        OfflineRec.CashChange = "";
                        OfflineRec.ContactNumber = txtTelContact.Text;
                        isFirst = false;
                    }



                    /**************************************************/
                    //OfflineLogToServiceRec = new ItemOfflineLogToServiceModel();
                    OfflineRec.PaymentDate = DateTime.Now.ToString("ddMMyyyy");
                    OfflineRec.ServiceType = ServiceType;
                    OfflineRec.ContractAccount = item.ContractAccount.ToString();
                    OfflineRec.BillNo = item.BillNumber;
                    OfflineRec.InvoiceNumber = item.InvoiceNumber != null ? item.InvoiceNumber.ToString() : "";
                    OfflineRec.AmountIncVat = double.Parse(item.AmountIncVat.ToString()).ToString();
                    OfflineRec.Interest = "xxxx.xx";
                    OfflineRec.DayOfInterest = "y";
                    OfflineRec.DB_OPS = "z";
                    OfflineRec.Transact = Transact;

                    LstOfflineRec.Add(OfflineRec);

                }
                OFC.WriteOffLineToCSV(LstOfflineRec, strDateTime);

                /********************************************************/





                /********************************************************/

                //OfflineRec = new ItemOfflineModel();
                //OfflineRec.UserId = "";
                //OfflineRec.PaymentDate = DateTime.Now.ToString("dd/MM/yyyy");
                //OfflineRec.Time = DateTime.Now.ToString("HH:mm");
                //OfflineRec.Branch = GlobalClass.BranchName;
                //OfflineRec.CashierNo = GlobalClass.CashierNo;
                //OfflineRec.ContactNumber = txtTelContact.Text;
                //OfflineRec.ContractAccount = "001234";
                //OfflineRec.InvoiceNumber = "005678";
                //OfflineRec.TaxInvoice = "";
                //OfflineRec.Amount = txtAmount.Text;
                //OfflineRec.Cash = txtCash.Text;
                //OfflineRec.CashChange = "";


                /*************************************************************************/
                //var confirmResult = MessageBox.Show("รบกวนสอบถามลูกค้าว่าถูกงดจ่ายกระแสไฟฟ้าหรือไม่\r\nกด YES เพื่อดำเนินการขอต่อกลับ\r\nกด NO หากไม่ต้องการขอต่อกลับ",
                //                     "แจ้งเตือน",
                //                     MessageBoxButtons.YesNo);
                //if (confirmResult == DialogResult.Yes)
                //{
                //    // If 'Yes', do something here.
                //    OfflineRec.ServiceType = "A";
                //}
                //else
                //{
                //    // If 'No', do something here.
                //    OfflineRec.ServiceType = "D";
                //}


                // OFC.WriteChequeOffLineToCSV(LstSummaryCheque , strDateTime);
                // OFC.WriteOffLineToCSV(OfflineRec);

                ClearDataItem();
                ClearDataGrid();



                /*************************************************************************/
                string Path = "";
                Path = ReportPath + "\\" + "rptTaxInvoiceOfflineStopElectricity.rpt";

                ReportDocument cryRpt = new ReportDocument();
                CrystalDecisions.Shared.PageMargins objPageMargins;

                objPageMargins = cryRpt.PrintOptions.PageMargins;
                objPageMargins.bottomMargin = 100;
                objPageMargins.leftMargin = 100;
                objPageMargins.rightMargin = 100;
                objPageMargins.topMargin = 100;

                cryRpt.Load(Path);

                if (GlobalClass.LstStopElectricityModel != null && GlobalClass.LstStopElectricityModel.Count > 0)
                {
                    cryRpt.SetDataSource(GlobalClass.LstStopElectricityModel);
                    cryRpt.SetParameterValue("Param_Payee", GlobalClass.UserName);
                    cryRpt.SetParameterValue("Param_BranchName", GlobalClass.BranchName);
                    cryRpt.SetParameterValue("Param_UserId", GlobalClass.UserId);
                    cryRpt.SetParameterValue("Param_ReceiptId", "Param_ReceiptId");
                    cryRpt.SetParameterValue("Param_BranchId", GlobalClass.BranchId);



                    //cryRpt.PrintOptions.ApplyPageMargins(objPageMargins);
                    //cryRpt.PrintOptions.PrinterName = "Microsoft Print to PDF";
                    //cryRpt.PrintToPrinter(1, false, 0, 0);

                }


                /*************************************************************************/

                Path = ReportPath + "\\" + "rptTaxInvoiceOffline_Test.rpt";
                cryRpt = new ReportDocument();
                cryRpt.Load(Path);
                objPageMargins = cryRpt.PrintOptions.PageMargins;
                objPageMargins.bottomMargin = 100;
                objPageMargins.leftMargin = 100;
                objPageMargins.rightMargin = 100;
                objPageMargins.topMargin = 100;

                cryRpt.SetDataSource(GlobalClass.LstShortTaxInviceSelectedModel);

                cryRpt.SetParameterValue("Param_Payee", GlobalClass.UserName);
                cryRpt.SetParameterValue("Param_BranchName", GlobalClass.BranchName);
                cryRpt.SetParameterValue("Param_UserId", GlobalClass.UserId);
                cryRpt.SetParameterValue("Param_ReceiptId", "Param_ReceiptId");
                cryRpt.SetParameterValue("Param_BranchId", GlobalClass.BranchId);

                //cryRpt.PrintOptions.ApplyPageMargins(objPageMargins);
                //cryRpt.PrintOptions.PrinterName = "Microsoft Print to PDF";
                //cryRpt.PrintToPrinter(1, false, 0, 0);

                /*************************************************************************/
                cryRpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\ASD.pdf");

                /*************************************************************************/

                Timer MyTimer = new Timer();
                MyTimer.Interval = 500; // 45 mins
                MyTimer.Tick += new EventHandler(Timer_CloseForm_Tick);
                MyTimer.Start();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {


                this.Close();
                return;

                OFC.WriteOfflineTxtLog();
                return;

                GlobalClass.ReportName = "rptTaxInvoiceOffline.rpt";
                GlobalClass.ReportOfflineType = "TAXINVOICE";


                FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();

                //formm receive = new FormReceiveMoney();
                FVR.ShowDialog();
            }
            catch (Exception ex)
            {

            }
        }

        private void GridDetail_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            Image editImage = Image.FromFile(@"E:\Workspace\MyPayment_2\MyPayment\Resources\icons8-edit-32.png");
            Image deleteImage = Image.FromFile(@"E:\Workspace\MyPayment_2\MyPayment\Resources\close3.png");

            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = 20;
                var h = 20;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(editImage, new Rectangle(x, y, w, h));
                e.Handled = true;
            }

            if (e.ColumnIndex == 1)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = 20;
                var h = 20;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(deleteImage, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }

        private void Timer_CloseForm_Tick(object sender, EventArgs e)
        {


            this.Close();
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {

        }

    }
}
