﻿namespace MyPayment.FormOffline
{
    partial class FormTemp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PN1 = new System.Windows.Forms.Panel();
            this.groupBoxEmployee = new System.Windows.Forms.GroupBox();
            this.txtCashierNo = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtBranch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PN2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.PN3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelAmountTotalCheq = new System.Windows.Forms.Label();
            this.labelAmountCheq = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.GridDetail = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPayeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colchCash = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PN_CHEQUE_HEAD = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.CheckCh = new System.Windows.Forms.CheckBox();
            this.TextBoxTel = new System.Windows.Forms.TextBox();
            this.TextBoxPayeeName = new System.Windows.Forms.TextBox();
            this.TextBoxBankBranch = new System.Windows.Forms.TextBox();
            this.lblB1 = new System.Windows.Forms.Label();
            this.lblB2 = new System.Windows.Forms.Label();
            this.DateTimePickerCheque = new System.Windows.Forms.DateTimePicker();
            this.lblB5 = new System.Windows.Forms.Label();
            this.TextAmountCheque = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TextBoxName = new System.Windows.Forms.TextBox();
            this.lblB3 = new System.Windows.Forms.Label();
            this.lblB6 = new System.Windows.Forms.Label();
            this.TextBoxChequeNo = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCash = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.PN4 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txTelContact = new System.Windows.Forms.TextBox();
            this.lblStatusC = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.LabelAmountChange = new System.Windows.Forms.Label();
            this.LabelAmountReceived = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.ButtonConfirm = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonUpdate = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonAdd = new Custom_Controls_in_CS.ButtonZ();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.PN1.SuspendLayout();
            this.groupBoxEmployee.SuspendLayout();
            this.PN2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.PN3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).BeginInit();
            this.PN_CHEQUE_HEAD.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.PN4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN1
            // 
            this.PN1.Controls.Add(this.groupBoxEmployee);
            this.PN1.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN1.Location = new System.Drawing.Point(0, 0);
            this.PN1.Name = "PN1";
            this.PN1.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN1.Size = new System.Drawing.Size(1008, 55);
            this.PN1.TabIndex = 1;
            // 
            // groupBoxEmployee
            // 
            this.groupBoxEmployee.Controls.Add(this.txtCashierNo);
            this.groupBoxEmployee.Controls.Add(this.textBox3);
            this.groupBoxEmployee.Controls.Add(this.textBox2);
            this.groupBoxEmployee.Controls.Add(this.txtBranch);
            this.groupBoxEmployee.Controls.Add(this.label1);
            this.groupBoxEmployee.Controls.Add(this.label2);
            this.groupBoxEmployee.Controls.Add(this.label5);
            this.groupBoxEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxEmployee.Location = new System.Drawing.Point(10, 0);
            this.groupBoxEmployee.Name = "groupBoxEmployee";
            this.groupBoxEmployee.Size = new System.Drawing.Size(988, 55);
            this.groupBoxEmployee.TabIndex = 1000000023;
            this.groupBoxEmployee.TabStop = false;
            // 
            // txtCashierNo
            // 
            this.txtCashierNo.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCashierNo.Location = new System.Drawing.Point(889, 12);
            this.txtCashierNo.Name = "txtCashierNo";
            this.txtCashierNo.Size = new System.Drawing.Size(73, 33);
            this.txtCashierNo.TabIndex = 1000000024;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox3.Location = new System.Drawing.Point(476, 12);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(342, 33);
            this.textBox3.TabIndex = 1000000023;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox2.Location = new System.Drawing.Point(380, 12);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(94, 33);
            this.textBox2.TabIndex = 1000000022;
            // 
            // txtBranch
            // 
            this.txtBranch.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtBranch.Location = new System.Drawing.Point(46, 12);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.Size = new System.Drawing.Size(259, 33);
            this.txtBranch.TabIndex = 1000000021;
            this.txtBranch.TextChanged += new System.EventHandler(this.txtBranch_TextChanged);
            this.txtBranch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtBranch_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(311, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 28);
            this.label1.TabIndex = 1000000009;
            this.label1.Text = "รหัสพนง.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(15, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 28);
            this.label2.TabIndex = 1000000008;
            this.label2.Text = "ฟข.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(822, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 28);
            this.label5.TabIndex = 1000000010;
            this.label5.Text = "ช่องชำระ.";
            // 
            // PN2
            // 
            this.PN2.Controls.Add(this.groupBox1);
            this.PN2.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN2.Location = new System.Drawing.Point(0, 55);
            this.PN2.Name = "PN2";
            this.PN2.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN2.Size = new System.Drawing.Size(1008, 55);
            this.PN2.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAmount);
            this.groupBox1.Controls.Add(this.Label3);
            this.groupBox1.Controls.Add(this.Label4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(10, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(988, 55);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("TH Sarabun New", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtAmount.Location = new System.Drawing.Point(163, 15);
            this.txtAmount.Multiline = true;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(311, 30);
            this.txtAmount.TabIndex = 1000000024;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(15, 17);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(148, 28);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "จำนวนเงินที่ต้องชำระ";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(476, 17);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(39, 28);
            this.Label4.TabIndex = 6;
            this.Label4.Text = "บาท";
            // 
            // PN3
            // 
            this.PN3.Controls.Add(this.groupBox2);
            this.PN3.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN3.Location = new System.Drawing.Point(0, 110);
            this.PN3.Name = "PN3";
            this.PN3.Padding = new System.Windows.Forms.Padding(10);
            this.PN3.Size = new System.Drawing.Size(1008, 400);
            this.PN3.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox2.ForeColor = System.Drawing.Color.Red;
            this.groupBox2.Location = new System.Drawing.Point(10, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.groupBox2.Size = new System.Drawing.Size(988, 380);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "วิธีการชำระเงิน";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelAmountTotalCheq);
            this.groupBox4.Controls.Add(this.labelAmountCheq);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.GridDetail);
            this.groupBox4.Controls.Add(this.PN_CHEQUE_HEAD);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox4.Location = new System.Drawing.Point(10, 89);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox4.Size = new System.Drawing.Size(968, 266);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "รายการเช็ค";
            // 
            // labelAmountTotalCheq
            // 
            this.labelAmountTotalCheq.BackColor = System.Drawing.Color.White;
            this.labelAmountTotalCheq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmountTotalCheq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmountTotalCheq.ForeColor = System.Drawing.Color.Blue;
            this.labelAmountTotalCheq.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelAmountTotalCheq.Location = new System.Drawing.Point(759, 229);
            this.labelAmountTotalCheq.Name = "labelAmountTotalCheq";
            this.labelAmountTotalCheq.Size = new System.Drawing.Size(193, 28);
            this.labelAmountTotalCheq.TabIndex = 5;
            this.labelAmountTotalCheq.Text = "0.00";
            this.labelAmountTotalCheq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAmountCheq
            // 
            this.labelAmountCheq.BackColor = System.Drawing.Color.White;
            this.labelAmountCheq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmountCheq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmountCheq.ForeColor = System.Drawing.Color.Blue;
            this.labelAmountCheq.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelAmountCheq.Location = new System.Drawing.Point(455, 229);
            this.labelAmountCheq.Name = "labelAmountCheq";
            this.labelAmountCheq.Size = new System.Drawing.Size(158, 28);
            this.labelAmountCheq.TabIndex = 4;
            this.labelAmountCheq.Text = "0.00";
            this.labelAmountCheq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(613, 229);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(144, 28);
            this.label12.TabIndex = 6;
            this.label12.Text = "รวมมูลค่าเช็คทั้งหมด";
            // 
            // GridDetail
            // 
            this.GridDetail.AllowUserToAddRows = false;
            this.GridDetail.BackgroundColor = System.Drawing.Color.White;
            this.GridDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.colNumber,
            this.colNo,
            this.colName,
            this.colDate,
            this.colAmount,
            this.colTel,
            this.colPayeeName,
            this.colchCash,
            this.ColBalance});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridDetail.DefaultCellStyle = dataGridViewCellStyle8;
            this.GridDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.GridDetail.Location = new System.Drawing.Point(3, 126);
            this.GridDetail.MultiSelect = false;
            this.GridDetail.Name = "GridDetail";
            this.GridDetail.ReadOnly = true;
            this.GridDetail.RowHeadersVisible = false;
            this.GridDetail.RowHeadersWidth = 10;
            this.GridDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridDetail.Size = new System.Drawing.Size(962, 100);
            this.GridDetail.TabIndex = 3;
            this.GridDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridDetail_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "แก้ไข";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "ลบ";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // colNumber
            // 
            this.colNumber.DataPropertyName = "Chno";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = null;
            this.colNumber.DefaultCellStyle = dataGridViewCellStyle1;
            this.colNumber.HeaderText = "เลขที่เช็ค";
            this.colNumber.Name = "colNumber";
            this.colNumber.ReadOnly = true;
            // 
            // colNo
            // 
            this.colNo.DataPropertyName = "Bankno";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.colNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.colNo.HeaderText = "รหัสธนาคาร";
            this.colNo.Name = "colNo";
            this.colNo.ReadOnly = true;
            this.colNo.Width = 120;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Bankname";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.colName.DefaultCellStyle = dataGridViewCellStyle3;
            this.colName.HeaderText = "ชื่อธนาคาร/สาขา";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colDate
            // 
            this.colDate.DataPropertyName = "Chdate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colDate.DefaultCellStyle = dataGridViewCellStyle4;
            this.colDate.HeaderText = "วันที่เช็ค";
            this.colDate.Name = "colDate";
            this.colDate.ReadOnly = true;
            this.colDate.Width = 120;
            // 
            // colAmount
            // 
            this.colAmount.DataPropertyName = "Chamount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,###,###,##0.00";
            this.colAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.colAmount.HeaderText = "จำนวนเงิน";
            this.colAmount.Name = "colAmount";
            this.colAmount.ReadOnly = true;
            // 
            // colTel
            // 
            this.colTel.DataPropertyName = "Tel";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colTel.DefaultCellStyle = dataGridViewCellStyle6;
            this.colTel.HeaderText = "เบอร์โทร";
            this.colTel.Name = "colTel";
            this.colTel.ReadOnly = true;
            // 
            // colPayeeName
            // 
            this.colPayeeName.DataPropertyName = "Chname";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colPayeeName.DefaultCellStyle = dataGridViewCellStyle7;
            this.colPayeeName.HeaderText = "ชื่อผู้สั่งจ่าย";
            this.colPayeeName.Name = "colPayeeName";
            this.colPayeeName.ReadOnly = true;
            this.colPayeeName.Width = 200;
            // 
            // colchCash
            // 
            this.colchCash.DataPropertyName = "Chcheck";
            this.colchCash.HeaderText = "แคชเชียร์เช็ค";
            this.colchCash.Name = "colchCash";
            this.colchCash.ReadOnly = true;
            this.colchCash.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colchCash.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colchCash.Width = 120;
            // 
            // ColBalance
            // 
            this.ColBalance.DataPropertyName = "Balance";
            this.ColBalance.HeaderText = "คงเหลือ";
            this.ColBalance.Name = "ColBalance";
            this.ColBalance.ReadOnly = true;
            this.ColBalance.Visible = false;
            // 
            // PN_CHEQUE_HEAD
            // 
            this.PN_CHEQUE_HEAD.Controls.Add(this.ButtonUpdate);
            this.PN_CHEQUE_HEAD.Controls.Add(this.label8);
            this.PN_CHEQUE_HEAD.Controls.Add(this.CheckCh);
            this.PN_CHEQUE_HEAD.Controls.Add(this.ButtonAdd);
            this.PN_CHEQUE_HEAD.Controls.Add(this.TextBoxTel);
            this.PN_CHEQUE_HEAD.Controls.Add(this.TextBoxPayeeName);
            this.PN_CHEQUE_HEAD.Controls.Add(this.TextBoxBankBranch);
            this.PN_CHEQUE_HEAD.Controls.Add(this.lblB1);
            this.PN_CHEQUE_HEAD.Controls.Add(this.lblB2);
            this.PN_CHEQUE_HEAD.Controls.Add(this.DateTimePickerCheque);
            this.PN_CHEQUE_HEAD.Controls.Add(this.lblB5);
            this.PN_CHEQUE_HEAD.Controls.Add(this.TextAmountCheque);
            this.PN_CHEQUE_HEAD.Controls.Add(this.label11);
            this.PN_CHEQUE_HEAD.Controls.Add(this.TextBoxName);
            this.PN_CHEQUE_HEAD.Controls.Add(this.lblB3);
            this.PN_CHEQUE_HEAD.Controls.Add(this.lblB6);
            this.PN_CHEQUE_HEAD.Controls.Add(this.TextBoxChequeNo);
            this.PN_CHEQUE_HEAD.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_CHEQUE_HEAD.Location = new System.Drawing.Point(3, 29);
            this.PN_CHEQUE_HEAD.Name = "PN_CHEQUE_HEAD";
            this.PN_CHEQUE_HEAD.Size = new System.Drawing.Size(962, 97);
            this.PN_CHEQUE_HEAD.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(566, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 26);
            this.label8.TabIndex = 66;
            this.label8.Text = "บาท";
            // 
            // CheckCh
            // 
            this.CheckCh.AutoSize = true;
            this.CheckCh.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CheckCh.ForeColor = System.Drawing.Color.Black;
            this.CheckCh.Location = new System.Drawing.Point(568, 65);
            this.CheckCh.Name = "CheckCh";
            this.CheckCh.Size = new System.Drawing.Size(115, 30);
            this.CheckCh.TabIndex = 7;
            this.CheckCh.Text = "แคชเชียร์เช็ค";
            this.CheckCh.UseVisualStyleBackColor = true;
            // 
            // TextBoxTel
            // 
            this.TextBoxTel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxTel.Location = new System.Drawing.Point(356, 65);
            this.TextBoxTel.Name = "TextBoxTel";
            this.TextBoxTel.Size = new System.Drawing.Size(208, 26);
            this.TextBoxTel.TabIndex = 6;
            // 
            // TextBoxPayeeName
            // 
            this.TextBoxPayeeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxPayeeName.Location = new System.Drawing.Point(79, 65);
            this.TextBoxPayeeName.Name = "TextBoxPayeeName";
            this.TextBoxPayeeName.Size = new System.Drawing.Size(204, 26);
            this.TextBoxPayeeName.TabIndex = 5;
            // 
            // TextBoxBankBranch
            // 
            this.TextBoxBankBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxBankBranch.Location = new System.Drawing.Point(356, 5);
            this.TextBoxBankBranch.Name = "TextBoxBankBranch";
            this.TextBoxBankBranch.Size = new System.Drawing.Size(133, 26);
            this.TextBoxBankBranch.TabIndex = 1;
            // 
            // lblB1
            // 
            this.lblB1.AutoSize = true;
            this.lblB1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB1.Location = new System.Drawing.Point(5, 5);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(69, 26);
            this.lblB1.TabIndex = 6;
            this.lblB1.Text = "เลขที่เช็ค";
            // 
            // lblB2
            // 
            this.lblB2.AutoSize = true;
            this.lblB2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB2.Location = new System.Drawing.Point(5, 34);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(114, 26);
            this.lblB2.TabIndex = 61;
            this.lblB2.Text = "วันที่ในเช็ค(พ.ศ.)";
            this.lblB2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DateTimePickerCheque
            // 
            this.DateTimePickerCheque.CustomFormat = "dd/MM/yyyy";
            this.DateTimePickerCheque.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePickerCheque.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerCheque.Location = new System.Drawing.Point(121, 34);
            this.DateTimePickerCheque.Name = "DateTimePickerCheque";
            this.DateTimePickerCheque.Size = new System.Drawing.Size(162, 26);
            this.DateTimePickerCheque.TabIndex = 3;
            // 
            // lblB5
            // 
            this.lblB5.AutoSize = true;
            this.lblB5.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB5.Location = new System.Drawing.Point(5, 65);
            this.lblB5.Name = "lblB5";
            this.lblB5.Size = new System.Drawing.Size(76, 26);
            this.lblB5.TabIndex = 62;
            this.lblB5.Text = "ชื่อผู้สั่งจ่าย";
            // 
            // TextAmountCheque
            // 
            this.TextAmountCheque.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmountCheque.Location = new System.Drawing.Point(356, 34);
            this.TextAmountCheque.Name = "TextAmountCheque";
            this.TextAmountCheque.Size = new System.Drawing.Size(208, 26);
            this.TextAmountCheque.TabIndex = 4;
            this.TextAmountCheque.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxAmount_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(281, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 26);
            this.label11.TabIndex = 63;
            this.label11.Text = "จำนวนเงิน";
            // 
            // TextBoxName
            // 
            this.TextBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxName.Location = new System.Drawing.Point(495, 5);
            this.TextBoxName.Name = "TextBoxName";
            this.TextBoxName.Size = new System.Drawing.Size(454, 26);
            this.TextBoxName.TabIndex = 2;
            // 
            // lblB3
            // 
            this.lblB3.AutoSize = true;
            this.lblB3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB3.Location = new System.Drawing.Point(234, 5);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(120, 26);
            this.lblB3.TabIndex = 64;
            this.lblB3.Text = "รหัสธนาคาร/สาขา";
            // 
            // lblB6
            // 
            this.lblB6.AutoSize = true;
            this.lblB6.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB6.Location = new System.Drawing.Point(289, 66);
            this.lblB6.Name = "lblB6";
            this.lblB6.Size = new System.Drawing.Size(65, 26);
            this.lblB6.TabIndex = 65;
            this.lblB6.Text = "เบอร์โทร";
            // 
            // TextBoxChequeNo
            // 
            this.TextBoxChequeNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxChequeNo.Location = new System.Drawing.Point(74, 5);
            this.TextBoxChequeNo.Name = "TextBoxChequeNo";
            this.TextBoxChequeNo.Size = new System.Drawing.Size(153, 26);
            this.TextBoxChequeNo.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtCash);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox3.Location = new System.Drawing.Point(10, 29);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox3.Size = new System.Drawing.Size(968, 60);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "เงินสด";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(397, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 26);
            this.label7.TabIndex = 2;
            this.label7.Text = "บาท";
            // 
            // txtCash
            // 
            this.txtCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCash.Location = new System.Drawing.Point(105, 21);
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(290, 31);
            this.txtCash.TabIndex = 0;
            this.txtCash.TextChanged += new System.EventHandler(this.TextAmount_TextChanged);
            this.txtCash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(35, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 26);
            this.label6.TabIndex = 1;
            this.label6.Text = "จำนวนเงิน";
            // 
            // PN4
            // 
            this.PN4.Controls.Add(this.ButtonConfirm);
            this.PN4.Controls.Add(this.ButtonCancel);
            this.PN4.Controls.Add(this.groupBox6);
            this.PN4.Controls.Add(this.groupBox5);
            this.PN4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN4.Location = new System.Drawing.Point(0, 510);
            this.PN4.Name = "PN4";
            this.PN4.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN4.Size = new System.Drawing.Size(1008, 219);
            this.PN4.TabIndex = 4;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.txTelContact);
            this.groupBox6.Controls.Add(this.lblStatusC);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.LabelAmountChange);
            this.groupBox6.Controls.Add(this.LabelAmountReceived);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(10, 60);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(988, 98);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(35, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 28);
            this.label9.TabIndex = 1000000026;
            this.label9.Text = "หมายเลขติดต่อกลับ";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txTelContact
            // 
            this.txTelContact.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txTelContact.Location = new System.Drawing.Point(169, 55);
            this.txTelContact.Name = "txTelContact";
            this.txTelContact.Size = new System.Drawing.Size(305, 33);
            this.txTelContact.TabIndex = 1000000025;
            // 
            // lblStatusC
            // 
            this.lblStatusC.BackColor = System.Drawing.Color.LightGray;
            this.lblStatusC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatusC.Font = new System.Drawing.Font("TH Sarabun New", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblStatusC.ForeColor = System.Drawing.Color.Red;
            this.lblStatusC.Location = new System.Drawing.Point(40, 16);
            this.lblStatusC.Name = "lblStatusC";
            this.lblStatusC.Size = new System.Drawing.Size(215, 35);
            this.lblStatusC.TabIndex = 1000000024;
            this.lblStatusC.Text = "***ห้ามรับเช็ค***";
            this.lblStatusC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblStatusC.Visible = false;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.White;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label28.Location = new System.Drawing.Point(346, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(103, 28);
            this.label28.TabIndex = 1000000023;
            this.label28.Text = "0.00";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelAmountChange
            // 
            this.LabelAmountChange.BackColor = System.Drawing.Color.White;
            this.LabelAmountChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelAmountChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAmountChange.ForeColor = System.Drawing.Color.Red;
            this.LabelAmountChange.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelAmountChange.Location = new System.Drawing.Point(579, 55);
            this.LabelAmountChange.Name = "LabelAmountChange";
            this.LabelAmountChange.Size = new System.Drawing.Size(383, 40);
            this.LabelAmountChange.TabIndex = 1000000020;
            this.LabelAmountChange.Text = "0.00";
            this.LabelAmountChange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelAmountReceived
            // 
            this.LabelAmountReceived.BackColor = System.Drawing.Color.White;
            this.LabelAmountReceived.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelAmountReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAmountReceived.ForeColor = System.Drawing.Color.Blue;
            this.LabelAmountReceived.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelAmountReceived.Location = new System.Drawing.Point(579, 10);
            this.LabelAmountReceived.Name = "LabelAmountReceived";
            this.LabelAmountReceived.Size = new System.Drawing.Size(383, 40);
            this.LabelAmountReceived.TabIndex = 1000000019;
            this.LabelAmountReceived.Text = "0.00";
            this.LabelAmountReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(469, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 28);
            this.label13.TabIndex = 68;
            this.label13.Text = "จำนวนเงินที่รับ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(514, 61);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 28);
            this.label15.TabIndex = 65;
            this.label15.Text = "เงินทอน";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(259, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 28);
            this.label16.TabIndex = 66;
            this.label16.Text = "การปัดเศษ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.Label14);
            this.groupBox5.Controls.Add(this.Label20);
            this.groupBox5.Controls.Add(this.Label18);
            this.groupBox5.Controls.Add(this.Label17);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(10, 0);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(988, 60);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.White;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label25.Location = new System.Drawing.Point(579, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(383, 35);
            this.label25.TabIndex = 1000000021;
            this.label25.Text = "0.00";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.White;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label24.Location = new System.Drawing.Point(140, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(147, 35);
            this.label24.TabIndex = 1000000021;
            this.label24.Text = "0.00";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label14.Location = new System.Drawing.Point(518, 21);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(59, 28);
            this.Label14.TabIndex = 68;
            this.Label14.Text = "เงินขาด";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label20.Location = new System.Drawing.Point(302, 21);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(29, 28);
            this.Label20.TabIndex = 65;
            this.Label20.Text = "ใบ";
            this.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label18.Location = new System.Drawing.Point(35, 21);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(106, 28);
            this.Label18.TabIndex = 66;
            this.Label18.Text = "จำนวนใบเสร็จ";
            this.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label17.Location = new System.Drawing.Point(352, 21);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(150, 28);
            this.Label17.TabIndex = 67;
            this.Label17.Text = "จำนวนเงินรับล่วงหน้า";
            this.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Label17.Visible = false;
            // 
            // ButtonConfirm
            // 
            this.ButtonConfirm.BackColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderWidth = 1;
            this.ButtonConfirm.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonConfirm.ButtonText = "ยืนยัน";
            this.ButtonConfirm.CausesValidation = false;
            this.ButtonConfirm.EndColor = System.Drawing.Color.Silver;
            this.ButtonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonConfirm.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonConfirm.ForeColor = System.Drawing.Color.Black;
            this.ButtonConfirm.GradientAngle = 90;
            this.ButtonConfirm.Image = global::MyPayment.Properties.Resources.ok1;
            this.ButtonConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonConfirm.Location = new System.Drawing.Point(779, 164);
            this.ButtonConfirm.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonConfirm.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonConfirm.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonConfirm.Name = "ButtonConfirm";
            this.ButtonConfirm.ShowButtontext = true;
            this.ButtonConfirm.Size = new System.Drawing.Size(90, 28);
            this.ButtonConfirm.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.TabIndex = 3;
            this.ButtonConfirm.TextLocation_X = 35;
            this.ButtonConfirm.TextLocation_Y = 1;
            this.ButtonConfirm.Transparent1 = 80;
            this.ButtonConfirm.Transparent2 = 120;
            this.ButtonConfirm.UseVisualStyleBackColor = true;
            this.ButtonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.Silver;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close2;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(876, 164);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(90, 28);
            this.ButtonCancel.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.TabIndex = 4;
            this.ButtonCancel.TextLocation_X = 35;
            this.ButtonCancel.TextLocation_Y = 1;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.BackColor = System.Drawing.Color.Transparent;
            this.ButtonUpdate.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonUpdate.BorderWidth = 1;
            this.ButtonUpdate.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonUpdate.ButtonText = "อัพเดท";
            this.ButtonUpdate.CausesValidation = false;
            this.ButtonUpdate.Enabled = false;
            this.ButtonUpdate.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonUpdate.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonUpdate.ForeColor = System.Drawing.Color.Black;
            this.ButtonUpdate.GradientAngle = 90;
            this.ButtonUpdate.Image = global::MyPayment.Properties.Resources.add;
            this.ButtonUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonUpdate.Location = new System.Drawing.Point(773, 63);
            this.ButtonUpdate.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonUpdate.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonUpdate.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonUpdate.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.ShowButtontext = true;
            this.ButtonUpdate.Size = new System.Drawing.Size(83, 28);
            this.ButtonUpdate.StartColor = System.Drawing.Color.LightGray;
            this.ButtonUpdate.TabIndex = 67;
            this.ButtonUpdate.TextLocation_X = 32;
            this.ButtonUpdate.TextLocation_Y = 1;
            this.ButtonUpdate.Transparent1 = 80;
            this.ButtonUpdate.Transparent2 = 120;
            this.ButtonUpdate.UseVisualStyleBackColor = true;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // ButtonAdd
            // 
            this.ButtonAdd.BackColor = System.Drawing.Color.Transparent;
            this.ButtonAdd.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonAdd.BorderWidth = 1;
            this.ButtonAdd.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonAdd.ButtonText = "เพิ่ม";
            this.ButtonAdd.CausesValidation = false;
            this.ButtonAdd.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdd.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonAdd.ForeColor = System.Drawing.Color.Black;
            this.ButtonAdd.GradientAngle = 90;
            this.ButtonAdd.Image = global::MyPayment.Properties.Resources.add;
            this.ButtonAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonAdd.Location = new System.Drawing.Point(684, 64);
            this.ButtonAdd.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonAdd.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonAdd.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonAdd.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonAdd.Name = "ButtonAdd";
            this.ButtonAdd.ShowButtontext = true;
            this.ButtonAdd.Size = new System.Drawing.Size(83, 28);
            this.ButtonAdd.StartColor = System.Drawing.Color.LightGray;
            this.ButtonAdd.TabIndex = 8;
            this.ButtonAdd.TextLocation_X = 32;
            this.ButtonAdd.TextLocation_Y = 1;
            this.ButtonAdd.Transparent1 = 80;
            this.ButtonAdd.Transparent2 = 120;
            this.ButtonAdd.UseVisualStyleBackColor = true;
            this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormTemp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.PN4);
            this.Controls.Add(this.PN3);
            this.Controls.Add(this.PN2);
            this.Controls.Add(this.PN1);
            this.Name = "FormTemp";
            this.Text = "FormTemp";
            this.Load += new System.EventHandler(this.FormTemp_Load);
            this.PN1.ResumeLayout(false);
            this.groupBoxEmployee.ResumeLayout(false);
            this.groupBoxEmployee.PerformLayout();
            this.PN2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.PN3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).EndInit();
            this.PN_CHEQUE_HEAD.ResumeLayout(false);
            this.PN_CHEQUE_HEAD.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.PN4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN1;
        private System.Windows.Forms.Panel PN2;
        private System.Windows.Forms.Panel PN3;
        private System.Windows.Forms.Panel PN4;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        private System.Windows.Forms.GroupBox groupBoxEmployee;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtBranch;
        private System.Windows.Forms.TextBox txtCashierNo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCash;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel PN_CHEQUE_HEAD;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.CheckBox CheckCh;
        private Custom_Controls_in_CS.ButtonZ ButtonAdd;
        private System.Windows.Forms.TextBox TextBoxTel;
        private System.Windows.Forms.TextBox TextBoxPayeeName;
        private System.Windows.Forms.TextBox TextBoxBankBranch;
        internal System.Windows.Forms.Label lblB1;
        internal System.Windows.Forms.Label lblB2;
        private System.Windows.Forms.DateTimePicker DateTimePickerCheque;
        internal System.Windows.Forms.Label lblB5;
        private System.Windows.Forms.TextBox TextAmountCheque;
        internal System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TextBoxName;
        internal System.Windows.Forms.Label lblB3;
        internal System.Windows.Forms.Label lblB6;
        private System.Windows.Forms.TextBox TextBoxChequeNo;
        internal System.Windows.Forms.DataGridView GridDetail;
        internal System.Windows.Forms.Label labelAmountTotalCheq;
        internal System.Windows.Forms.Label labelAmountCheq;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Label Label17;
        private System.Windows.Forms.GroupBox groupBox6;
        internal System.Windows.Forms.Label lblStatusC;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label LabelAmountChange;
        internal System.Windows.Forms.Label LabelAmountReceived;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label16;
        private Custom_Controls_in_CS.ButtonZ ButtonConfirm;
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        private System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txTelContact;
        private System.Windows.Forms.DataGridViewButtonColumn Column1;
        private System.Windows.Forms.DataGridViewButtonColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPayeeName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colchCash;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBalance;
        private Custom_Controls_in_CS.ButtonZ ButtonUpdate;
        private System.Windows.Forms.Timer timer1;
    }
}