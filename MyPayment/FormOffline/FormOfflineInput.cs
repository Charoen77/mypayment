﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using MyPayment.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
namespace MyPayment.FormOffline
{
    public partial class FormOfflineInput : Form
    {
        private static OffLineClass OFC = new OffLineClass();
      
        private static double VAT = double.Parse(ConfigurationManager.AppSettings["VAT"].ToString());
        public bool GB_IsReadBarcode = false;

        public static ItemCodeData GB_ItemCodeData;
        public static List<ItemCodeData> GB_LstStopElectricityModel = new List<ItemCodeData>();
        public FormOfflineInput()
        {
            InitializeComponent();
        }
        private void FormOfflineInput_Load(object sender, EventArgs e)
        {
            try
            {

              //  ResetContractAccountEvent();


                this.GridInputData.AutoGenerateColumns = false;
                this.GridInputData.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                foreach (DataGridViewColumn col in this.GridInputData.Columns)
                {
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                  //  col.HeaderCell.Style.Font = new Font("Arial", 12F, FontStyle.Bold, GraphicsUnit.Pixel);
                }

            }
            catch(Exception ex)
            {

            }
        }

        private void txtContractAccount_TextChanged(object sender, EventArgs e)
        {
            try
            {


                if (!GB_IsReadBarcode)
                {

                   
                    this.txtContractAccount.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.txtContractAccount_KeyDown);

                    GB_IsReadBarcode = true;
                    timer1.Interval = 500;
                    timer1.Enabled = true;
                    timer1.Start();
                }

            }
            catch (Exception ex)
            {

            }
        }
        private void ReadData()
        {
            try
            {
                //ItemCodeData rec =   OFC.ReadQrCode("");

                // var source = new BindingSource(rec, null);

                // this.GridInputData.DataSource = source;


                ItemCodeData rec = OFC.ReadQrCode("");
                List<ItemCodeData> LstItem = new List<ItemCodeData>();
                LstItem.Add(rec);
               // var source = new BindingSource(rec, null);

                this.GridInputData.DataSource = LstItem;


                  CountItem();
            }
            catch(Exception ex)
            {

            }
        }

      

        private void GridInputData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    BindingSource bs = new BindingSource();
                    bs = (BindingSource)this.GridInputData.DataSource;
                    List<ItemCodeData> LstItem = new List<ItemCodeData>();
                    LstItem = (List<ItemCodeData>)bs.DataSource;


                    string ContractAccount = GridInputData.CurrentRow.Cells["ContractAccount"].Value.ToString();
                    bool Selected = GridInputData.CurrentRow.Cells["Selected"].Value.ToString() == "True" ? true : false;

                    LstItem.Where(m => m.ContractAccount == ContractAccount).ToList().ForEach(x=>x.Selected = !Selected);

                    this.GridInputData.DataSource = bs;

                    /********************************/
                    CountItem();
                }
            }
            catch(Exception ex)
            {

            }

            
        }


        private void CountItem()
        {
            try
            {

                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.GridInputData.DataSource;
                List<ItemCodeData> LstItem = new List<ItemCodeData>();
                LstItem = (List<ItemCodeData>)bs.DataSource;
                DateTime DueDate;
                DateTime NowDate = DateTime.Now;
                List<ItemCodeData> LstItemSelected = LstItem.Where(m => m.Selected == true).ToList();




                /*********************************************************************/

                string TotalAmountExVat = LstItem.Sum(m => double.Parse(m.AmountExVat)).ToString("N2");
                string PayAmount = LstItemSelected.Sum(m => double.Parse(m.AmountExVat)).ToString("N2");
                string ItemSelect = LstItemSelected.Count.ToString();
                string TotalItem = LstItem.Count.ToString();
                string SumFine = LstItem.Where(m => m.FineFlakedOut != null).Sum(m => double.Parse(m.FineFlakedOut)).ToString("N2");
                string SumSelectedFine = LstItemSelected.Where(m => m.FineFlakedOut != null).Sum(m => double.Parse(m.FineFlakedOut)).ToString("N2");
                string TotalVat = LstItem.Where(m => m.Vat != null).Sum(m => double.Parse(m.Vat)).ToString("N2");
                string TotalSelectedVat = LstItemSelected.Where(m => m.Vat != null).Sum(m => double.Parse(m.Vat)).ToString("N2");
                string TotalStopElectricity = LstItemSelected.Where(m => m.StopElectricityAmount != null).Sum(m => double.Parse(m.StopElectricityAmount)).ToString("N2");

                this.txtPayAmount.Text = PayAmount;
                this.txtTotalAmountExVat.Text = TotalAmountExVat;
                this.txtTotalItem.Text = TotalItem;
                this.txtItemSelect.Text = ItemSelect;
                this.txtFine.Text = SumFine;
                this.txtTotalSumAmount.Text = (double.Parse(PayAmount) + double.Parse(TotalSelectedVat) + double.Parse(SumSelectedFine)).ToString("N2");
                this.txtTotalVat.Text = TotalVat;
                this.txtTotalAmountIncVat.Text = (double.Parse(TotalVat) + double.Parse(TotalAmountExVat) + double.Parse(TotalStopElectricity)).ToString("N2");


            }
            catch (Exception ex)
            {

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
 

                ItemCodeData obj = OFC.TestGenOfflineInputRec();

                if(isDuplicateData(obj.ContractAccount, obj.InvoiceNumber , ""))
                {
                    return;
                }

                /***********************************/
                double tmpAmount = 0;
                double tmpAmountVat = 0;
                double fineResult = 0;

                int diffDay = 0;
                DateTime DueDate;
                DateTime NowDate = DateTime.Now;

                tmpAmount = double.Parse(obj.AmountExVat);
                DueDate = DateTime.ParseExact(obj.DueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                //if (tmpAmount >= 10000 && NowDate.Date > DueDate.Date)
                //{
                //    tmpAmountVat = tmpAmount + ((tmpAmount * VAT)) / 100;

                //    diffDay = NowDate.Date.Day - DueDate.Date.Day;


                //    fineResult = ((tmpAmountVat - tmpAmount) * diffDay * (diffDay / 100)) / 365;

             
                //    obj.FineFlakedOut = fineResult.ToString();
                //}


                if ( NowDate.Date > DueDate.Date)
                {
                    
                    fineResult = 10.000;
                    obj.FineFlakedOut = fineResult.ToString("N2");
                }


                /***********************************/
                double VatAmount = 0;
                tmpAmount = double.Parse(obj.AmountExVat);
                VatAmount = tmpAmount * (VAT / 100.00);
                obj.Vat = VatAmount.ToString();
                obj.AmountIncVat = (tmpAmount + VatAmount).ToString("N2");

                /***********************************/
                BindingSource bs = new BindingSource();


                bs = (BindingSource) this.GridInputData.DataSource;

                List<ItemCodeData> LstItem = new List<ItemCodeData>();

                if (bs != null)
                {
                    LstItem = (List<ItemCodeData>)bs.DataSource;

                    if(LstItem == null)
                    {
                        LstItem = new List<ItemCodeData>();
                    }
                }
                else
                {
                    bs = new BindingSource();
                }
                LstItem.Add(obj);

                bs = new BindingSource();
                bs.DataSource = LstItem;
                this.GridInputData.DataSource = bs;



                CountItem();


            }
            catch(Exception ex)
            {

            }
        }
        private bool isDuplicateData(string ContractAccount , string InvoiceNumber , string CodeType)
        {
            try
            {
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.GridInputData.DataSource;
                List<ItemCodeData> LstItem = new List<ItemCodeData>();
                if (bs == null )
                {
                    return false;
                }
                LstItem = (List<ItemCodeData>)bs.DataSource;
                if (LstItem == null)
                {
                    return false;
                }

                if(CodeType == "") //เป็นการ key ตรง เช็คแค่ ContractAccount
                {
                    if (LstItem.Where(m => m.ContractAccount == ContractAccount).Any())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                if(CodeType == "QRCODE")
                {
                    InvoiceNumber = InvoiceNumber.Substring(1);
                }

                if (LstItem.Where(m=>m.ContractAccount == ContractAccount && m.InvoiceNumber == InvoiceNumber).Any())
                {
                    return true;
                }
                return false;

            }
            catch(Exception ex)
            {
                return true;
            }
        }

        private void btnReceiveMoney_Click(object sender, EventArgs e)
        {
            try
            {


                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.GridInputData.DataSource;
                List<ItemCodeData> LstItem = new List<ItemCodeData>();
                LstItem = (List<ItemCodeData>)bs.DataSource;
                List<ItemCodeData> LstItemSelected = LstItem.Where(m => m.Selected == true).ToList();

                /***************************************************************************/

                var arrCA = LstItemSelected.Select(m => m.ContractAccount).Distinct().ToArray();
                double TotalAmountExVat = 0;
                double TotalVat = 0;
                double TotalAmountIncVat = 0;
                double TotalFine = 0;
                double TotalAll = 0;

                foreach (var item in arrCA)
                {
                    TotalAmountExVat = LstItemSelected.Where(m => m.ContractAccount == item).Sum(m => double.Parse(m.AmountExVat));
                    TotalVat = LstItemSelected.Where(m => m.ContractAccount == item).Sum(m => double.Parse(m.Vat));
                    TotalAmountIncVat = LstItemSelected.Where(m => m.ContractAccount == item).Sum(m => double.Parse(m.AmountIncVat));
                    TotalFine = LstItemSelected.Where(m => m.ContractAccount == item && m.FineFlakedOut != null).Sum(m => double.Parse(m.FineFlakedOut));

                    TotalAll =  TotalAmountIncVat + TotalFine;
                    LstItemSelected.Where(m => m.ContractAccount == item).ToList().ForEach(m =>
                    {
                        m.TotalAmountExVat = TotalAmountExVat.ToString("N2");
                        m.TotalVat = TotalVat.ToString("N2");
                        m.TotalAmountIncVat = TotalAmountIncVat.ToString("N2");
                        m.TotalFine = TotalFine.ToString("N2");
                        m.TotalAll = TotalAll.ToString("N2");
                        m.BillNumber = OFC.CreateBillNumber();
                    });
                                                                                               
                }


                /***************************************************************************/

                GlobalClass.LstShortTaxInviceSelectedModel = LstItemSelected;
                GlobalClass.LstStopElectricityModel = GB_LstStopElectricityModel;
                GB_LstStopElectricityModel = new List<ItemCodeData>();

                FormOfflineReceive OfflineReceive = new FormOfflineReceive();

                OfflineReceive.SetAmount(this.txtTotalAmountExVat.Text);
              
                OfflineReceive.ShowDialog();
                OfflineReceive.Dispose();
                ClearData();

            }
            catch(Exception ex)
            {

            }
        }
        

        public void ClearData()
        {
            try
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = null;
                this.GridInputData.DataSource = bs;

                this.txtItemSelect.Text = "";
                this.txtPayAmount.Text = "";
                this.txtTotalItem.Text = "";
                this.txtTotalSumAmount.Text = "";
                this.txtTotalAmountExVat.Text = "";
                this.txtTotalVat.Text = "";
                this.txtTotalAmountIncVat.Text = "";
                this.txtFine.Text = "";
            }
            catch(Exception ex)
            {

            }
        }

        private void btnSendService_Click(object sender, EventArgs e)
        {

            OFC.WriteOfflineTxtLog();
            return;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                GB_IsReadBarcode = false;
                timer1.Stop();
                // MessageBox.Show(txtBranch.Text, "Time Elapsed");

                if (this.txtContractAccount.Text != "")
                {
                    ItemCodeData ItemCode = OFC.ReadQrCode(this.txtContractAccount.Text);
                    

                    //  MessageBox.Show(ItemCode.ContractAccount + " " + ItemCode.DueDate + " " + ItemCode.InvoiceNumber + " " + ItemCode.AmountExVat, "Time Elapsed");

                    AddData(ItemCode , "QRCODE");
                    //ResetContractAccountEvent();
                }


                // AddData(ItemCode);
            }
            catch (Exception ex)
            {

            }
          
         
        }

        private void AddData(ItemCodeData obj , string CodeType)
        {
            try {
              //  ItemCodeData obj = OFC.TestGenOfflineInputRec();

                if (isDuplicateData(obj.ContractAccount, obj.InvoiceNumber , CodeType))
                {
                    this.txtContractAccount.Text = "";
                    this.txtContractAccount.Focus();
                    return;
                }

                /***********************************/
                double tmpAmount = 0;
                double tmpAmountVat = 0;
                double fineResult = 0;

                int diffDay = 0;
                DateTime DueDate;
                DateTime NowDate = DateTime.Now;

                tmpAmount = double.Parse(obj.AmountExVat);

                if (obj.DueDate != null)
                {
                    DueDate = DateTime.ParseExact(obj.DueDate, "dd/MM/yy", CultureInfo.InvariantCulture);

                    //if (tmpAmount >= 10000 && NowDate.Date > DueDate.Date)
                    //{
                    //    tmpAmountVat = tmpAmount + ((tmpAmount * VAT)) / 100;

                    //    diffDay = NowDate.Date.Day - DueDate.Date.Day;


                    //    fineResult = ((tmpAmountVat - tmpAmount) * diffDay * (diffDay / 100)) / 365;


                    //    obj.FineFlakedOut = fineResult.ToString();
                    //}


                    if (NowDate.Date > DueDate.Date)
                    {

                        fineResult = 10.000;
                        obj.FineFlakedOut = fineResult.ToString("N2");
                    }

                }
               


                /***********************************/
                double VatAmount = 0;
               
                tmpAmount = double.Parse(obj.AmountExVat);
                VatAmount = tmpAmount * (VAT / 100.00);
                obj.Vat = VatAmount.ToString("N2");
                obj.AmountIncVat = (tmpAmount + double.Parse( VatAmount.ToString("N2"))).ToString("N2");
                
                /***********************************/
                BindingSource bs = new BindingSource();


                bs = (BindingSource)this.GridInputData.DataSource;

                List<ItemCodeData> LstItem = new List<ItemCodeData>();

                if (bs != null)
                {
                    LstItem = (List<ItemCodeData>)bs.DataSource;

                    if (LstItem == null)
                    {
                        LstItem = new List<ItemCodeData>();
                    }
                }
                else
                {
                    bs = new BindingSource();
                }
                LstItem.Add(obj);

                bs = new BindingSource();
                bs.DataSource = LstItem;
                this.GridInputData.DataSource = bs;



                CountItem();


                this.txtContractAccount.Text = "";
                this.txtContractAccount.Focus();
            }
            catch (Exception ex)
            {

            }
        }

        private void txtContractAccount_KeyDown(object sender, KeyEventArgs e)
        {
            
    

            try
            {

             
                string ServiceType = "";
                string CodeType = "";
                if (e.KeyCode == Keys.Enter)
                {
                    if(this.txtContractAccount.Text == "")
                    {
                        MessageBox.Show("กรุณาระบุ Contract Number", "Warnning");
                    }
                    else if(this.txtContractAccount.Text.Trim().Length == 9)
                    {
                        //  MessageBox.Show(this.txtContractAccount.Text, "Warnning");

                        /*********************/

                        GB_ItemCodeData = new ItemCodeData();

                        GB_ItemCodeData.Selected = true;
                        GB_ItemCodeData.BillNumber = "1476210987123";
                        GB_ItemCodeData.ContractAccount = this.txtContractAccount.Text;
                        FormProcessingFeeElectricity FeeElectServiceType = new FormProcessingFeeElectricity();

                        FeeElectServiceType.ShowDialog();
                        FeeElectServiceType.Dispose();
                        ServiceType = GlobalClass.Status ? "A" : "D";
                        GB_ItemCodeData.ServiceType = ServiceType;

                        if (ServiceType == "D")
                        {
                            ItemCodeData Data = new ItemCodeData();
                            Data.Selected = true;
                            Data.ServiceType = "D";
                            Data.ContractAccount = this.txtContractAccount.Text;
                            Data.AmountExVat = "40.00";
                            Data.Vat = ((VAT * 40) / 100).ToString("N2");
                            Data.AmountIncVat = (double.Parse(Data.Vat) + 40.0).ToString("N2");

                            Data.TotalAmountExVat = "40.00";
                            Data.TotalVat = Data.Vat;
                            Data.TotalAll = Data.AmountIncVat;

                            GB_LstStopElectricityModel.Add(Data);

                            GB_ItemCodeData.StopElectricityAmount = "40.00";
                        }
                        /*********************/
                        FormInputMoney frmInputMoney = new FormInputMoney();
                        frmInputMoney.ShowDialog();
                        frmInputMoney.Dispose();

                        if (!string.IsNullOrEmpty(frmInputMoney.InputMoney))
                        {
                            GB_ItemCodeData.AmountExVat = double.Parse(frmInputMoney.InputMoney).ToString("N2");
                        }

                        /*********************/
                        AddData(GB_ItemCodeData,"");
                     //   ResetContractAccountEvent();
                    }
                    else if (this.txtContractAccount.Text.Trim().Length > 55)
                    {

                        string strData = this.txtContractAccount.Text.Trim().Replace(" ","").Replace("|","");
                        ItemCodeData ItemCode = new ItemCodeData();
                        if (strData.Substring(13, 2) == "00")
                        {
                            CodeType = "BARCODE";
                            ItemCode = OFC.ReadStdBarCode(this.txtContractAccount.Text);
                        }
                        else
                        {
                            CodeType = "QRCODE";
                            ItemCode = OFC.ReadQrCode(this.txtContractAccount.Text);
                        }


                        /********************************************/
                       


                        //  MessageBox.Show(ItemCode.ContractAccount + " " + ItemCode.DueDate + " " + ItemCode.InvoiceNumber + " " + ItemCode.AmountExVat, "Time Elapsed");

                        AddData(ItemCode , CodeType);
                       // ResetContractAccountEvent();
                    }
                }
               

            }
            catch(Exception ex)
            {

            }
        }


        //private  void ResetContractAccountEvent()
        //{
        //    try
        //    {

        //        this.txtContractAccount.TextChanged -= new System.EventHandler(this.txtContractAccount_TextChanged);
        //        this.txtContractAccount.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.txtContractAccount_KeyDown);
        //        this.txtContractAccount.Text = "";

        //       this.txtContractAccount.TextChanged += new System.EventHandler(this.txtContractAccount_TextChanged);
        //        this.txtContractAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtContractAccount_KeyDown);
        //        this.txtContractAccount.Focus();
        //    }
        //    catch(Exception ex)
        //    {

        //    }
        //}

        private void txtContractAccount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
