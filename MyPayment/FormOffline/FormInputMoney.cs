﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using MyPayment.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace MyPayment.FormOffline
{
    public partial class FormInputMoney : Form
    {
        public FormInputMoney()
        {
            InitializeComponent();
        }

        public string InputMoney { get; set; }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                //GlobalClass.Data = this.txtMoney.Text.Trim();

                this.InputMoney = this.txtMoney.Text.Trim();
                this.Close();
            }
            catch(Exception ex)
            {

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtMoney.Text = "";
                this.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void txtMoney_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                   
                    this.SelectNextControl(this.txtMoney, true, true, true, true);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void txtMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
