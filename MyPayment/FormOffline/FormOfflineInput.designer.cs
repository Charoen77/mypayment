﻿namespace MyPayment.FormOffline
{
    partial class FormOfflineInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOfflineInput));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PN1 = new System.Windows.Forms.Panel();
            this.btnSendService = new System.Windows.Forms.Button();
            this.btnAdd = new Custom_Controls_in_CS.ButtonZ();
            this.txtContractAccount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PN2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GridInputData = new System.Windows.Forms.DataGridView();
            this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TaxId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContractAccount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DebtCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DueDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FineFlakedOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StopElectricityAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountExVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PN3 = new System.Windows.Forms.Panel();
            this.btnReceiveMoney = new Custom_Controls_in_CS.ButtonZ();
            this.txtTotalSumAmount = new System.Windows.Forms.Label();
            this.Label37 = new System.Windows.Forms.Label();
            this.Label39 = new System.Windows.Forms.Label();
            this.Label38 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtFine = new System.Windows.Forms.Label();
            this.Label36 = new System.Windows.Forms.Label();
            this.txtTotalAmountIncVat = new System.Windows.Forms.Label();
            this.Label35 = new System.Windows.Forms.Label();
            this.txtTotalVat = new System.Windows.Forms.Label();
            this.txtTotalAmountExVat = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label32 = new System.Windows.Forms.Label();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label29 = new System.Windows.Forms.Label();
            this.txtTotalItem = new System.Windows.Forms.Label();
            this.Label31 = new System.Windows.Forms.Label();
            this.txtItemSelect = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.txtPayAmount = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.PN1.SuspendLayout();
            this.PN2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridInputData)).BeginInit();
            this.PN3.SuspendLayout();
            this.Panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN1
            // 
            this.PN1.Controls.Add(this.btnSendService);
            this.PN1.Controls.Add(this.btnAdd);
            this.PN1.Controls.Add(this.txtContractAccount);
            this.PN1.Controls.Add(this.label1);
            this.PN1.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN1.Location = new System.Drawing.Point(0, 0);
            this.PN1.Name = "PN1";
            this.PN1.Size = new System.Drawing.Size(1008, 56);
            this.PN1.TabIndex = 0;
            // 
            // btnSendService
            // 
            this.btnSendService.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnSendService.Location = new System.Drawing.Point(785, 13);
            this.btnSendService.Name = "btnSendService";
            this.btnSendService.Size = new System.Drawing.Size(85, 29);
            this.btnSendService.TabIndex = 1000000042;
            this.btnSendService.Text = "Service";
            this.btnSendService.UseVisualStyleBackColor = true;
            this.btnSendService.Visible = false;
            this.btnSendService.Click += new System.EventHandler(this.btnSendService_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd.BorderColor = System.Drawing.Color.Transparent;
            this.btnAdd.BorderWidth = 1;
            this.btnAdd.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btnAdd.ButtonText = "เพิ่ม";
            this.btnAdd.CausesValidation = false;
            this.btnAdd.EndColor = System.Drawing.Color.DarkGray;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.GradientAngle = 90;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(696, 14);
            this.btnAdd.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btnAdd.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnAdd.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btnAdd.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ShowButtontext = true;
            this.btnAdd.Size = new System.Drawing.Size(83, 28);
            this.btnAdd.StartColor = System.Drawing.Color.LightGray;
            this.btnAdd.TabIndex = 1000000041;
            this.btnAdd.TextLocation_X = 32;
            this.btnAdd.TextLocation_Y = 1;
            this.btnAdd.Transparent1 = 80;
            this.btnAdd.Transparent2 = 120;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtContractAccount
            // 
            this.txtContractAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtContractAccount.Location = new System.Drawing.Point(130, 13);
            this.txtContractAccount.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtContractAccount.Name = "txtContractAccount";
            this.txtContractAccount.Size = new System.Drawing.Size(561, 29);
            this.txtContractAccount.TabIndex = 1000000026;
            this.txtContractAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtContractAccount_KeyDown);
            this.txtContractAccount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContractAccount_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 26);
            this.label1.TabIndex = 1000000025;
            this.label1.Text = "บัญชีแสดงสัญญา";
            // 
            // PN2
            // 
            this.PN2.Controls.Add(this.groupBox1);
            this.PN2.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN2.Location = new System.Drawing.Point(0, 56);
            this.PN2.Name = "PN2";
            this.PN2.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN2.Size = new System.Drawing.Size(1008, 481);
            this.PN2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GridInputData);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(10, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10, 3, 10, 4);
            this.groupBox1.Size = new System.Drawing.Size(988, 481);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รายละเอียดหนี้ค้างชำระ";
            // 
            // GridInputData
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridInputData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridInputData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridInputData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selected,
            this.TaxId,
            this.ContractAccount,
            this.PowerUnit,
            this.DebtCode,
            this.InvoiceNumber,
            this.DueDate,
            this.FineFlakedOut,
            this.StopElectricityAmount,
            this.AmountExVat});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridInputData.DefaultCellStyle = dataGridViewCellStyle5;
            this.GridInputData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridInputData.Location = new System.Drawing.Point(10, 28);
            this.GridInputData.Name = "GridInputData";
            this.GridInputData.Size = new System.Drawing.Size(968, 449);
            this.GridInputData.TabIndex = 0;
            this.GridInputData.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridInputData_CellClick);
            // 
            // Selected
            // 
            this.Selected.DataPropertyName = "Selected";
            this.Selected.HeaderText = "";
            this.Selected.Name = "Selected";
            this.Selected.Width = 30;
            // 
            // TaxId
            // 
            this.TaxId.DataPropertyName = "TaxId";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TaxId.DefaultCellStyle = dataGridViewCellStyle2;
            this.TaxId.HeaderText = "เลขประจำตัวผู้เสียภาษี";
            this.TaxId.Name = "TaxId";
            this.TaxId.Width = 180;
            // 
            // ContractAccount
            // 
            this.ContractAccount.DataPropertyName = "ContractAccount";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ContractAccount.DefaultCellStyle = dataGridViewCellStyle3;
            this.ContractAccount.HeaderText = "บัญชีแสดงสัญญา";
            this.ContractAccount.Name = "ContractAccount";
            this.ContractAccount.Width = 140;
            // 
            // PowerUnit
            // 
            this.PowerUnit.DataPropertyName = "PowerUnit";
            this.PowerUnit.HeaderText = "หน่วยการใช้ไฟฟ้า";
            this.PowerUnit.Name = "PowerUnit";
            this.PowerUnit.Width = 150;
            // 
            // DebtCode
            // 
            this.DebtCode.DataPropertyName = "DebtCode";
            this.DebtCode.HeaderText = "รหัสหนี้ค้างชำระ";
            this.DebtCode.Name = "DebtCode";
            this.DebtCode.Width = 150;
            // 
            // InvoiceNumber
            // 
            this.InvoiceNumber.DataPropertyName = "InvoiceNumber";
            this.InvoiceNumber.HeaderText = "เลขที่ใบแจ้งฯ";
            this.InvoiceNumber.Name = "InvoiceNumber";
            this.InvoiceNumber.Width = 120;
            // 
            // DueDate
            // 
            this.DueDate.DataPropertyName = "DueDate";
            this.DueDate.HeaderText = "วันครบกำหนดชำระ";
            this.DueDate.Name = "DueDate";
            this.DueDate.Width = 160;
            // 
            // FineFlakedOut
            // 
            this.FineFlakedOut.DataPropertyName = "FineFlakedOut";
            this.FineFlakedOut.HeaderText = "ค่าปรับ";
            this.FineFlakedOut.Name = "FineFlakedOut";
            // 
            // StopElectricityAmount
            // 
            this.StopElectricityAmount.DataPropertyName = "StopElectricityAmount";
            this.StopElectricityAmount.HeaderText = "ค่าดำเนินการงดจ่ายไฟ";
            this.StopElectricityAmount.Name = "StopElectricityAmount";
            this.StopElectricityAmount.Width = 180;
            // 
            // AmountExVat
            // 
            this.AmountExVat.DataPropertyName = "AmountExVat";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AmountExVat.DefaultCellStyle = dataGridViewCellStyle4;
            this.AmountExVat.HeaderText = "จำนวนเงิน";
            this.AmountExVat.Name = "AmountExVat";
            // 
            // PN3
            // 
            this.PN3.Controls.Add(this.btnReceiveMoney);
            this.PN3.Controls.Add(this.txtTotalSumAmount);
            this.PN3.Controls.Add(this.Label37);
            this.PN3.Controls.Add(this.Label39);
            this.PN3.Controls.Add(this.Label38);
            this.PN3.Controls.Add(this.Label22);
            this.PN3.Controls.Add(this.Label13);
            this.PN3.Controls.Add(this.txtFine);
            this.PN3.Controls.Add(this.Label36);
            this.PN3.Controls.Add(this.txtTotalAmountIncVat);
            this.PN3.Controls.Add(this.Label35);
            this.PN3.Controls.Add(this.txtTotalVat);
            this.PN3.Controls.Add(this.txtTotalAmountExVat);
            this.PN3.Controls.Add(this.Label34);
            this.PN3.Controls.Add(this.Label33);
            this.PN3.Controls.Add(this.Panel4);
            this.PN3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN3.Location = new System.Drawing.Point(0, 537);
            this.PN3.Name = "PN3";
            this.PN3.Size = new System.Drawing.Size(1008, 192);
            this.PN3.TabIndex = 2;
            // 
            // btnReceiveMoney
            // 
            this.btnReceiveMoney.BackColor = System.Drawing.Color.Transparent;
            this.btnReceiveMoney.BorderColor = System.Drawing.Color.Transparent;
            this.btnReceiveMoney.BorderWidth = 1;
            this.btnReceiveMoney.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btnReceiveMoney.ButtonText = "รับเงิน";
            this.btnReceiveMoney.CausesValidation = false;
            this.btnReceiveMoney.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReceiveMoney.EndColor = System.Drawing.Color.DarkGray;
            this.btnReceiveMoney.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReceiveMoney.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnReceiveMoney.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnReceiveMoney.GradientAngle = 90;
            this.btnReceiveMoney.Image = global::MyPayment.Properties.Resources.icons8_paper_money_26;
            this.btnReceiveMoney.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReceiveMoney.Location = new System.Drawing.Point(845, 145);
            this.btnReceiveMoney.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btnReceiveMoney.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnReceiveMoney.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btnReceiveMoney.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btnReceiveMoney.Name = "btnReceiveMoney";
            this.btnReceiveMoney.ShowButtontext = true;
            this.btnReceiveMoney.Size = new System.Drawing.Size(83, 35);
            this.btnReceiveMoney.StartColor = System.Drawing.Color.LightGray;
            this.btnReceiveMoney.TabIndex = 1000000041;
            this.btnReceiveMoney.TextLocation_X = 35;
            this.btnReceiveMoney.TextLocation_Y = 6;
            this.btnReceiveMoney.Transparent1 = 80;
            this.btnReceiveMoney.Transparent2 = 120;
            this.btnReceiveMoney.UseVisualStyleBackColor = true;
            this.btnReceiveMoney.Click += new System.EventHandler(this.btnReceiveMoney_Click);
            // 
            // txtTotalSumAmount
            // 
            this.txtTotalSumAmount.BackColor = System.Drawing.Color.White;
            this.txtTotalSumAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalSumAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTotalSumAmount.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalSumAmount.Location = new System.Drawing.Point(197, 91);
            this.txtTotalSumAmount.Name = "txtTotalSumAmount";
            this.txtTotalSumAmount.Size = new System.Drawing.Size(394, 39);
            this.txtTotalSumAmount.TabIndex = 142;
            this.txtTotalSumAmount.Text = "0.00";
            this.txtTotalSumAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.Font = new System.Drawing.Font("TH Sarabun New", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label37.ForeColor = System.Drawing.Color.Blue;
            this.Label37.Location = new System.Drawing.Point(24, 82);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(173, 57);
            this.Label37.TabIndex = 141;
            this.Label37.Text = "รวมรับชำระ";
            this.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label39
            // 
            this.Label39.AutoSize = true;
            this.Label39.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label39.Location = new System.Drawing.Point(930, 105);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(36, 26);
            this.Label39.TabIndex = 140;
            this.Label39.Text = "บาท";
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label38.Location = new System.Drawing.Point(930, 72);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(36, 26);
            this.Label38.TabIndex = 139;
            this.Label38.Text = "บาท";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label22.Location = new System.Drawing.Point(929, 39);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(36, 26);
            this.Label22.TabIndex = 138;
            this.Label22.Text = "บาท";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label13.Location = new System.Drawing.Point(929, 6);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(36, 26);
            this.Label13.TabIndex = 137;
            this.Label13.Text = "บาท";
            // 
            // txtFine
            // 
            this.txtFine.BackColor = System.Drawing.Color.White;
            this.txtFine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtFine.Location = new System.Drawing.Point(785, 105);
            this.txtFine.Name = "txtFine";
            this.txtFine.Size = new System.Drawing.Size(143, 25);
            this.txtFine.TabIndex = 136;
            this.txtFine.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label36
            // 
            this.Label36.AutoSize = true;
            this.Label36.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label36.Location = new System.Drawing.Point(627, 105);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(152, 26);
            this.Label36.TabIndex = 135;
            this.Label36.Text = "เบี้ยปรับและค่าเสียหายฯ";
            this.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalAmountIncVat
            // 
            this.txtTotalAmountIncVat.BackColor = System.Drawing.Color.White;
            this.txtTotalAmountIncVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalAmountIncVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalAmountIncVat.Location = new System.Drawing.Point(785, 72);
            this.txtTotalAmountIncVat.Name = "txtTotalAmountIncVat";
            this.txtTotalAmountIncVat.Size = new System.Drawing.Size(143, 25);
            this.txtTotalAmountIncVat.TabIndex = 134;
            this.txtTotalAmountIncVat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label35
            // 
            this.Label35.AutoSize = true;
            this.Label35.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label35.Location = new System.Drawing.Point(742, 72);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(37, 26);
            this.Label35.TabIndex = 133;
            this.Label35.Text = "รวม";
            this.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalVat
            // 
            this.txtTotalVat.BackColor = System.Drawing.Color.White;
            this.txtTotalVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalVat.Location = new System.Drawing.Point(785, 39);
            this.txtTotalVat.Name = "txtTotalVat";
            this.txtTotalVat.Size = new System.Drawing.Size(143, 25);
            this.txtTotalVat.TabIndex = 132;
            this.txtTotalVat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalAmountExVat
            // 
            this.txtTotalAmountExVat.BackColor = System.Drawing.Color.White;
            this.txtTotalAmountExVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalAmountExVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalAmountExVat.Location = new System.Drawing.Point(785, 6);
            this.txtTotalAmountExVat.Name = "txtTotalAmountExVat";
            this.txtTotalAmountExVat.Size = new System.Drawing.Size(143, 25);
            this.txtTotalAmountExVat.TabIndex = 131;
            this.txtTotalAmountExVat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label34
            // 
            this.Label34.AutoSize = true;
            this.Label34.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label34.Location = new System.Drawing.Point(721, 6);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(58, 26);
            this.Label34.TabIndex = 130;
            this.Label34.Text = "รวมเงิน";
            this.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label33
            // 
            this.Label33.AutoSize = true;
            this.Label33.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label33.Location = new System.Drawing.Point(685, 39);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(94, 26);
            this.Label33.TabIndex = 129;
            this.Label33.Text = "ภาษีมูลค่าเพิ่ม";
            this.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel4.Controls.Add(this.Label26);
            this.Panel4.Controls.Add(this.Label32);
            this.Panel4.Controls.Add(this.Label19);
            this.Panel4.Controls.Add(this.Label29);
            this.Panel4.Controls.Add(this.txtTotalItem);
            this.Panel4.Controls.Add(this.Label31);
            this.Panel4.Controls.Add(this.txtItemSelect);
            this.Panel4.Controls.Add(this.Label30);
            this.Panel4.Controls.Add(this.txtPayAmount);
            this.Panel4.Location = new System.Drawing.Point(20, 6);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(571, 65);
            this.Panel4.TabIndex = 128;
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label26.ForeColor = System.Drawing.Color.Blue;
            this.Label26.Location = new System.Drawing.Point(431, 34);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(56, 26);
            this.Label26.TabIndex = 93;
            this.Label26.Text = "รายการ";
            // 
            // Label32
            // 
            this.Label32.AutoSize = true;
            this.Label32.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label32.ForeColor = System.Drawing.Color.Blue;
            this.Label32.Location = new System.Drawing.Point(3, 4);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(44, 26);
            this.Label32.TabIndex = 96;
            this.Label32.Text = "ชำระ";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label19.ForeColor = System.Drawing.Color.Blue;
            this.Label19.Location = new System.Drawing.Point(3, 32);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(196, 26);
            this.Label19.TabIndex = 99;
            this.Label19.Text = "จำนวนบัญชีแสดงสัญญา/สัญญา";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label29.ForeColor = System.Drawing.Color.Blue;
            this.Label29.Location = new System.Drawing.Point(431, 4);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(36, 26);
            this.Label29.TabIndex = 99;
            this.Label29.Text = "บาท";
            // 
            // txtTotalItem
            // 
            this.txtTotalItem.BackColor = System.Drawing.Color.White;
            this.txtTotalItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalItem.Location = new System.Drawing.Point(205, 34);
            this.txtTotalItem.Name = "txtTotalItem";
            this.txtTotalItem.Size = new System.Drawing.Size(224, 25);
            this.txtTotalItem.TabIndex = 100;
            this.txtTotalItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label31
            // 
            this.Label31.AutoSize = true;
            this.Label31.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label31.ForeColor = System.Drawing.Color.Blue;
            this.Label31.Location = new System.Drawing.Point(166, 4);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(56, 26);
            this.Label31.TabIndex = 97;
            this.Label31.Text = "รายการ";
            // 
            // txtItemSelect
            // 
            this.txtItemSelect.BackColor = System.Drawing.Color.White;
            this.txtItemSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtItemSelect.Location = new System.Drawing.Point(53, 5);
            this.txtItemSelect.Name = "txtItemSelect";
            this.txtItemSelect.Size = new System.Drawing.Size(111, 25);
            this.txtItemSelect.TabIndex = 101;
            this.txtItemSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label30.ForeColor = System.Drawing.Color.Blue;
            this.Label30.Location = new System.Drawing.Point(224, 4);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(56, 26);
            this.Label30.TabIndex = 98;
            this.Label30.Text = "เป็นเงิน";
            // 
            // txtPayAmount
            // 
            this.txtPayAmount.BackColor = System.Drawing.Color.White;
            this.txtPayAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtPayAmount.Location = new System.Drawing.Point(286, 5);
            this.txtPayAmount.Name = "txtPayAmount";
            this.txtPayAmount.Size = new System.Drawing.Size(143, 25);
            this.txtPayAmount.TabIndex = 103;
            this.txtPayAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormOfflineInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.PN3);
            this.Controls.Add(this.PN2);
            this.Controls.Add(this.PN1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormOfflineInput";
            this.Text = "FormOfflineInput";
            this.Load += new System.EventHandler(this.FormOfflineInput_Load);
            this.PN1.ResumeLayout(false);
            this.PN1.PerformLayout();
            this.PN2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridInputData)).EndInit();
            this.PN3.ResumeLayout(false);
            this.PN3.PerformLayout();
            this.Panel4.ResumeLayout(false);
            this.Panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN1;
        private System.Windows.Forms.Panel PN2;
        private System.Windows.Forms.TextBox txtContractAccount;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel PN3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView GridInputData;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label32;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label txtTotalItem;
        internal System.Windows.Forms.Label Label31;
        internal System.Windows.Forms.Label txtItemSelect;
        internal System.Windows.Forms.Label Label30;
        internal System.Windows.Forms.Label txtPayAmount;
        internal System.Windows.Forms.Label Label39;
        internal System.Windows.Forms.Label Label38;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label txtFine;
        internal System.Windows.Forms.Label Label36;
        internal System.Windows.Forms.Label txtTotalAmountIncVat;
        internal System.Windows.Forms.Label Label35;
        internal System.Windows.Forms.Label txtTotalVat;
        internal System.Windows.Forms.Label txtTotalAmountExVat;
        internal System.Windows.Forms.Label Label34;
        internal System.Windows.Forms.Label Label33;
        internal System.Windows.Forms.Label txtTotalSumAmount;
        internal System.Windows.Forms.Label Label37;
        private Custom_Controls_in_CS.ButtonZ btnAdd;
        private Custom_Controls_in_CS.ButtonZ btnReceiveMoney;
        private System.Windows.Forms.Button btnSendService;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContractAccount;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn DebtCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn DueDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn FineFlakedOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn StopElectricityAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountExVat;
    }
}