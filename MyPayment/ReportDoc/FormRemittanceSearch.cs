﻿using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.ReportDoc
{
    public partial class FormRemittanceSearch : Form
    {
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ResultRemittance resul = new ResultRemittance();
        public string strTemp;
        public FormRemittanceSearch()
        {
            InitializeComponent();
        }
        private void btAdd_Click(object sender, EventArgs e)
        {
            FormRemittance receive = new FormRemittance();
            receive.ShowDialog();
        }
        private void FormRemittanceSearch_Load(object sender, EventArgs e)
        {
            LabelEmployeeCode.Text = GlobalClass.UserId.ToString();
            LabelStationNo.Text = Convert.ToDecimal(GlobalClass.No).ToString("00");
            SetControl();
        }
        public void SetControl()
        {
            GridViewDetail.AutoGenerateColumns = false;
            GridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ArrearsController arr = new ArrearsController();
                ClassUser clsuserNew = new ClassUser();
                clsuserNew.http = "login";
                clsuserNew.empId = LabelEmployeeCode.Text.Trim();
                clsuserNew.counterNo = LabelStationNo.Text.Trim();
                clsuserNew.Datetime = DateTimePicker.Text.Trim();
                resul = arr.SearchDataRemittance(clsuserNew);
                if (resul.Output != null)
                {
                    SetControl();
                    this.GridViewDetail.DataSource = resul.Output;
                    meaLog.WriteData("Success :: " + strTemp, method, LogLevel.Debug);
                    AddImage();
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormRemittanceSearch | Function :: ButtonSearch_Click()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
            }
        }
        public void AddImage()
        {
            int i = 0;
            var loopTo = GridViewDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                Image image = Properties.Resources.Recycle;
                GridViewDetail.Rows[i].Cells["ColDelete"].Value = image;
                Image Edit = Properties.Resources.icons8_edit_32;
                GridViewDetail.Rows[i].Cells["ColEdit"].Value = Edit;
            }
        }
        private void GridViewDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.ColumnIndex == 5)
                {
                    FormRemittance detail = new FormRemittance();
                    detail.labelEmployeeCode.Text = GridViewDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                    detail.LabelStationNo.Text = GridViewDetail.CurrentRow.Cells["ColNo"].Value.ToString();
                    detail.lblAmount.Text = GridViewDetail.CurrentRow.Cells["ColName"].Value.ToString();
                    detail.ShowDialog();
                    detail.Dispose();
                    ButtonSearch_Click(null, null);
                }
                else if (e.ColumnIndex == 6)
                {
                    if (MessageBox.Show("คุณต้องการยกเลิกรายการนี้หรือไม่ ", "ยกเลิกเช็ค", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    else
                    {
                        if (GridViewDetail.Rows.Count > 0)
                        {
                            DataGridViewRow dgvDelRow = GridViewDetail.Rows[e.RowIndex];
                            GridViewDetail.Rows.Remove(dgvDelRow);
                            GridViewDetail.Refresh();
                            //int rowIndex = GridViewDetail.CurrentCell.RowIndex;
                            //_cheque.RemoveAt(rowIndex);
                            //this.GridViewDetail.DataSource = null;

                            //if (_cheque.Count > 0)
                            //    this.GridViewDetail.DataSource = _cheque;

                            //this.GridViewDetail.Update();
                            //this.GridViewDetail.Refresh();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormRemittanceSearch | Function :: GridViewDetail_CellClick()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
            }
        }
    }
}
