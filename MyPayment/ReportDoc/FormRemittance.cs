﻿using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.ReportDoc
{
    public partial class FormRemittance : Form
    {
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ResultRemittanceSlip resul = new ResultRemittanceSlip();
        ArrearsController arrears = new ArrearsController();
        public CultureInfo usCulture = new CultureInfo("en-US");
        List<data> _data = new List<data>();
        public double _cashAmount;
        public FormRemittance()
        {
            InitializeComponent();
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FormRemittance_Load(object sender, EventArgs e)
        {
            LoadData();
            SetControl();
        }
        public void LoadData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ResultPayInBalance rs = new ResultPayInBalance();
                labelEmployeeCode.Text = GlobalClass.UserId.ToString();
                LabelStationNo.Text = Convert.ToDecimal(GlobalClass.No).ToString("00");
                DateTime dateNow = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                ClassPayInBalance classPay = new ClassPayInBalance();
                classPay.payDate = dateNow.ToString("dd/MM/yyyy");
                classPay.distId = GlobalClass.DistId;
                classPay.cashierId = GlobalClass.UserId;
                classPay.cashierNo = int.Parse(GlobalClass.No);
                rs = arrears.SelectDataPayin(classPay);
                double countAmount = 0;

                if (rs.OutputPayinSummary.Count > 0)
                {
                    foreach (var item in rs.OutputPayinSummary)
                    {
                        countAmount = item.cashAmount + item.chequeAmount + item.cardAmount;
                        _cashAmount = item.cashAmount;
                    }
                }
                lblAmount.Text = countAmount.ToString("#,###,###,##0.00");
                if (rs.OutputPayinCheque.Count > 0)
                    GridViewDetailCheque.DataSource = rs.OutputPayinCheque;
                if (rs.OutputPayinCredit.Count > 0)
                    GridViewCard.DataSource = rs.OutputPayinCredit;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void SetControl()
        {
            GridViewDetailCheque.AutoGenerateColumns = false;
            GridViewDetailCheque.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GridViewCard.AutoGenerateColumns = false;
            GridViewCard.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void DateTimePickerCheque_ValueChanged(object sender, EventArgs e)
        {

        }
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ClassPayIn payin = new ClassPayIn();
                _data = new List<data>();
                payin.processSet = "PayIN";
                payin.distId = GlobalClass.DistId;
                payin.cashierId = Convert.ToInt64(labelEmployeeCode.Text);
                payin.cashierNo = int.Parse(LabelStationNo.Text);
                DateTime dateNow = Convert.ToDateTime(DateTimePicker.Value.ToString("dd/MM/yyyy", usCulture));
                payin.clsDate = dateNow.ToString("dd/MM/yyyy");
                payin.note01 = (TextAmount25st.Text != "") ? int.Parse(TextAmount25st.Text) : 0;
                payin.note02 = (TextAmount50st.Text != "") ? int.Parse(TextAmount50st.Text) : 0;
                payin.note03 = (TextAmount1.Text != "") ? int.Parse(TextAmount1.Text) : 0;
                payin.note04 = (TextAmount2.Text != "") ? int.Parse(TextAmount2.Text) : 0;
                payin.note05 = (TextAmount5.Text != "") ? int.Parse(TextAmount5.Text) : 0;
                payin.note06 = (TextAmount10.Text != "") ? int.Parse(TextAmount10.Text) : 0;
                payin.note07 = (TextAmount20.Text != "") ? int.Parse(TextAmount20.Text) : 0;
                payin.note08 = (TextAmount50.Text != "") ? int.Parse(TextAmount50.Text) : 0;
                payin.note09 = (TextAmount100.Text != "") ? int.Parse(TextAmount100.Text) : 0;
                payin.note10 = (TextAmount500.Text != "") ? int.Parse(TextAmount500.Text) : 0;
                payin.note11 = (TextAmount1000.Text != "") ? int.Parse(TextAmount1000.Text) : 0;
                payin.note12 = 0;
                payin.chequeQty = CountCheqce();
                payin.chequeAmount = Convert.ToDouble(labelAmountCheq.Text);
                payin.ccardQty = CountCard();
                payin.ccardAmount = Convert.ToDouble(labelAmountCard.Text);
                AddDataPayIn();
                payin.data = _data;
                resul = arrears.InsertRemittance(payin);
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void AddDataPayIn()
        {
            var cashierId = Convert.ToInt64(labelEmployeeCode.Text);
            var dis = GlobalClass.DistId;
            if (labelAmountCheq.Text != "0")
            {
                int i = 0;
                var loopTo = GridViewDetailCheque.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewDetailCheque.Rows[i].Cells["ColCkCheq"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                    {
                        data data = new data();
                        data.method = 2;
                        data.distId = dis;
                        data.cashierId = cashierId;
                        data.refId = Convert.ToInt64(GridViewDetailCheque.Rows[i].Cells["ColtransactionId"].Value);
                        data.refNo = GridViewDetailCheque.Rows[i].Cells["ColChequeNo"].Value.ToString();
                        data.closingId = 0;
                        _data.Add(data);
                    }
                }
            }
            if (labelAmountCard.Text != "0")
            {
                int i = 0;
                var loopTo = GridViewCard.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewCard.Rows[i].Cells["ColCkCard"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                    {
                        data data = new data();
                        data.method = 3;
                        data.distId = dis;
                        data.cashierId = cashierId;
                        data.refId = Convert.ToInt64(GridViewCard.Rows[i].Cells["transactionId"].Value);
                        data.refNo = GridViewCard.Rows[i].Cells["ColCardNo"].Value.ToString();
                        data.closingId = 0;
                        _data.Add(data);
                    }
                }
            }
        }
        private void TextAmount1000_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount1000.Text == string.Empty)
                labelAmount1000.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount1000.Text) * 1000;
                if (amount <= _cashAmount)
                    labelAmount1000.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount500_TextChanged_1(object sender, EventArgs e)
        {
            if (TextAmount500.Text == string.Empty)
                TextAmount500.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount500.Text) * 500;
                labelAmount500.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount100_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount100.Text == string.Empty)
                TextAmount100.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount100.Text) * 100;
                labelAmount100.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount50_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount50.Text == string.Empty)
                TextAmount50.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount50.Text) * 50;
                labelAmount50.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount20_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount20.Text == string.Empty)
                TextAmount20.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount20.Text) * 20;
                labelAmount20.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount10_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount10.Text == string.Empty)
                TextAmount10.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount10.Text) * 10;
                labelAmount10.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount5_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount5.Text == string.Empty)
                TextAmount5.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount5.Text) * 5;
                labelAmount5.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount2_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount2.Text == string.Empty)
                TextAmount2.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount2.Text) * 2;
                labelAmount2.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount1_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount1.Text == string.Empty)
                TextAmount1.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount1.Text);
                labelAmount1.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount50st_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount50st.Text == string.Empty)
                TextAmount50st.Text = "0";
            else
            {
                double amount = (Convert.ToInt32(TextAmount50st.Text) * 50) / 100;
                labelAmount50st.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount25st_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount25st.Text == string.Empty)
                TextAmount25st.Text = "0";
            else
            {
                double amount = (Convert.ToInt32(TextAmount25st.Text) * 25) / 100;
                labelAmount25st.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        public void CountAmount()
        {
            double amount;
            double amount1000 = Convert.ToDouble(labelAmount1000.Text);
            double amount500 = Convert.ToDouble(labelAmount500.Text);
            double amount100 = Convert.ToDouble(labelAmount100.Text);
            double amount50 = Convert.ToDouble(labelAmount50.Text);
            double amount20 = Convert.ToDouble(labelAmount20.Text);
            double amount10 = Convert.ToDouble(labelAmount10.Text);
            double amount5 = Convert.ToDouble(labelAmount5.Text);
            double amount2 = Convert.ToDouble(labelAmount2.Text);
            double amount1 = Convert.ToDouble(labelAmount1.Text);
            double amount50st = Convert.ToDouble(labelAmount50st.Text);
            double amount25st = Convert.ToDouble(labelAmount25st.Text);
            amount = amount1000 + amount500 + amount100 + amount50 + amount20 + amount10 + amount5 + amount2 + amount1 + amount50st + amount25st;
            labelAmount.Text = amount.ToString("#,###,###,##0.00");
        }
        public void SumAmount()
        {
            double sumAmount;
            double amountCheq = Convert.ToDouble(labelAmountCheq.Text);
            double amountCard = Convert.ToDouble(labelAmountCard.Text);
            double amount = Convert.ToDouble(labelAmount.Text);
            sumAmount = amountCheq + amountCard + amount;
            labelTotalAmount.Text = sumAmount.ToString("#,###,###,##0.00");

            double sumAll;
            double TotalAmount = Convert.ToDouble(labelTotalAmount.Text);
            double AmountAll = Convert.ToDouble(lblAmount.Text);
            sumAll = AmountAll - TotalAmount;
            labelSettlement.Text = sumAll.ToString("#,###,###,##0.00");
        }
        public int CountCheqce()
        {
            int i = 0;
            int countCheck = 0;
            var loopTo = GridViewDetailCheque.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (GridViewDetailCheque.CurrentCell is DataGridViewCheckBoxCell)
                {
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewDetailCheque.Rows[i].Cells["ColCkCheq"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                        countCheck++;
                }
            }
            return countCheck;
        }
        public int CountCard()
        {
            int i = 0;
            int countCheck = 0;
            var loopTo = GridViewCard.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (GridViewCard.CurrentCell is DataGridViewCheckBoxCell)
                {
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewCard.Rows[i].Cells["ColCkCard"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                        countCheck++;
                }
            }
            return countCheck;
        }
        private void GridViewDetailCheque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    double amount = 0;
                    int i = 0;
                    int countCheck = 0;
                    var loopTo = GridViewDetailCheque.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (GridViewDetailCheque.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewDetailCheque.Rows[i].Cells["ColCkCheq"];
                            bool newBool = (bool)checkCell.EditedFormattedValue;
                            if (newBool)
                            {
                                if (GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value.ToString() != "")
                                    amount += Convert.ToDouble(GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value);
                                countCheck++;
                                if (countCheck == GridViewDetailCheque.Rows.Count)
                                    CkSelectCheqAll.Checked = true;
                            }
                            else
                                CkSelectCheqAll.Checked = false;
                        }
                    }
                    labelAmountCheq.Text = amount.ToString("#,###,###,##0.00");
                    SumAmount();
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void GridViewCard_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    double amount = 0;
                    int i = 0;
                    int countCheck = 0;
                    var loopTo = GridViewCard.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (GridViewCard.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewCard.Rows[i].Cells["ColCkCard"];
                            bool newBool = (bool)checkCell.EditedFormattedValue;
                            if (newBool)
                            {
                                if (GridViewCard.Rows[i].Cells["ColAmountCard"].Value.ToString() != "")
                                    amount += Convert.ToDouble(GridViewCard.Rows[i].Cells["ColAmountCard"].Value);
                                countCheck++;
                                if (countCheck == GridViewCard.Rows.Count)
                                    CkSelectCardAll.Checked = true;
                            }
                            else
                                CkSelectCardAll.Checked = false;
                        }
                    }
                    labelAmountCard.Text = amount.ToString("#,###,###,##0.00");
                    SumAmount();
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void CkSelectCheqAll_Click(object sender, EventArgs e)
        {
            SelectCheckboxCheqce();
        }
        public void SelectCheckboxCheqce()
        {
            double amount = 0;
            int i = 0;
            var loopTo = GridViewDetailCheque.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CkSelectCheqAll.Checked == true)
                {
                    GridViewDetailCheque.Rows[i].Cells["ColCkCheq"].Value = true;
                    if (GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value.ToString() != "")
                        amount += Convert.ToDouble(GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value);
                }
                else
                {
                    amount = 0;
                    GridViewDetailCheque.Rows[i].Cells["ColCkCheq"].Value = false;
                }
            }
            labelAmountCheq.Text = amount.ToString("#,###,###,##0.00");
            SumAmount();
        }
        public void SelectCheckboxCard()
        {
            double amount = 0;
            int i = 0;
            var loopTo = GridViewCard.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CkSelectCardAll.Checked == true)
                {
                    GridViewCard.Rows[i].Cells["ColCkCard"].Value = true;
                    if (GridViewCard.Rows[i].Cells["ColAmountCard"].Value.ToString() != "")
                        amount += Convert.ToDouble(GridViewCard.Rows[i].Cells["ColAmountCard"].Value);
                }
                else
                {
                    amount = 0;
                    GridViewCard.Rows[i].Cells["ColCkCard"].Value = false;
                }
            }
            labelAmountCard.Text = amount.ToString("#,###,###,##0.00");
            SumAmount();
        }
        private void CkSelectCardAll_Click(object sender, EventArgs e)
        {
            SelectCheckboxCard();
        }
        private void TextAmount1000_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount500_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount100_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount50_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount20_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount10_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount50st_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount25st_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
    }
}
