﻿namespace MyPayment.ReportDoc
{
    partial class FormRemittanceSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.LabelStationNo = new System.Windows.Forms.Label();
            this.LabelEmployeeCode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btAdd = new Custom_Controls_in_CS.ButtonZ();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GridViewDetail = new System.Windows.Forms.DataGridView();
            this.ColNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CoEdit = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColDelete = new System.Windows.Forms.DataGridViewImageColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ButtonSearch);
            this.panel1.Controls.Add(this.DateTimePicker);
            this.panel1.Controls.Add(this.LabelStationNo);
            this.panel1.Controls.Add(this.LabelEmployeeCode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 59);
            this.panel1.TabIndex = 0;
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "ค้นหา";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.Silver;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = global::MyPayment.Properties.Resources.zoom1;
            this.ButtonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSearch.Location = new System.Drawing.Point(881, 14);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(90, 28);
            this.ButtonSearch.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.TabIndex = 1000000043;
            this.ButtonSearch.TextLocation_X = 35;
            this.ButtonSearch.TextLocation_Y = 1;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker.Location = new System.Drawing.Point(54, 15);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(171, 26);
            this.DateTimePicker.TabIndex = 1000000029;
            // 
            // LabelStationNo
            // 
            this.LabelStationNo.BackColor = System.Drawing.Color.White;
            this.LabelStationNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelStationNo.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStationNo.Location = new System.Drawing.Point(734, 14);
            this.LabelStationNo.Name = "LabelStationNo";
            this.LabelStationNo.Size = new System.Drawing.Size(83, 28);
            this.LabelStationNo.TabIndex = 1000000028;
            this.LabelStationNo.Text = "09";
            this.LabelStationNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelEmployeeCode
            // 
            this.LabelEmployeeCode.BackColor = System.Drawing.Color.White;
            this.LabelEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelEmployeeCode.Font = new System.Drawing.Font("TH Sarabun New", 18F);
            this.LabelEmployeeCode.Location = new System.Drawing.Point(329, 14);
            this.LabelEmployeeCode.Name = "LabelEmployeeCode";
            this.LabelEmployeeCode.Size = new System.Drawing.Size(277, 28);
            this.LabelEmployeeCode.TabIndex = 1000000027;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(621, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 28);
            this.label2.TabIndex = 1000000026;
            this.label2.Text = "โต๊ะรับชำระเงิน";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(231, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 28);
            this.label1.TabIndex = 1000000025;
            this.label1.Text = "รหัสพนักงาน";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(12, 14);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(41, 28);
            this.Label3.TabIndex = 1000000024;
            this.Label3.Text = "วันที่";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 59);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1012, 646);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btAdd);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1012, 40);
            this.panel3.TabIndex = 1;
            // 
            // btAdd
            // 
            this.btAdd.BackColor = System.Drawing.Color.Transparent;
            this.btAdd.BorderColor = System.Drawing.Color.Transparent;
            this.btAdd.BorderWidth = 1;
            this.btAdd.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btAdd.ButtonText = "เพิ่ม";
            this.btAdd.CausesValidation = false;
            this.btAdd.EndColor = System.Drawing.Color.Silver;
            this.btAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAdd.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btAdd.ForeColor = System.Drawing.Color.Black;
            this.btAdd.GradientAngle = 90;
            this.btAdd.Image = global::MyPayment.Properties.Resources.add;
            this.btAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAdd.Location = new System.Drawing.Point(12, 5);
            this.btAdd.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btAdd.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btAdd.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btAdd.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btAdd.Name = "btAdd";
            this.btAdd.ShowButtontext = true;
            this.btAdd.Size = new System.Drawing.Size(95, 27);
            this.btAdd.StartColor = System.Drawing.Color.Gainsboro;
            this.btAdd.TabIndex = 1000000044;
            this.btAdd.TextLocation_X = 35;
            this.btAdd.TextLocation_Y = 1;
            this.btAdd.Transparent1 = 80;
            this.btAdd.Transparent2 = 120;
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GridViewDetail);
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(3, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1006, 600);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รายละเอียด";
            // 
            // GridViewDetail
            // 
            this.GridViewDetail.AllowUserToAddRows = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridViewDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.GridViewDetail.BackgroundColor = System.Drawing.Color.White;
            this.GridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColNo,
            this.Column3,
            this.Column1,
            this.Column2,
            this.ColAmount,
            this.CoEdit,
            this.ColDelete});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridViewDetail.DefaultCellStyle = dataGridViewCellStyle14;
            this.GridViewDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.GridViewDetail.Location = new System.Drawing.Point(3, 29);
            this.GridViewDetail.Name = "GridViewDetail";
            this.GridViewDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridViewDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.GridViewDetail.Size = new System.Drawing.Size(1000, 565);
            this.GridViewDetail.TabIndex = 3;
            this.GridViewDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridViewDetail_CellClick);
            // 
            // ColNo
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColNo.DefaultCellStyle = dataGridViewCellStyle9;
            this.ColNo.HeaderText = "ลำดับ";
            this.ColNo.Name = "ColNo";
            this.ColNo.Width = 65;
            // 
            // Column3
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column3.HeaderText = "ครั้ง";
            this.Column3.Name = "Column3";
            this.Column3.Width = 190;
            // 
            // Column1
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "#,###,###,##0.0000";
            this.Column1.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column1.HeaderText = "จำนวนเงินเช็ค";
            this.Column1.Name = "Column1";
            this.Column1.Width = 200;
            // 
            // Column2
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "#,###,###,##0.0000";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle12;
            this.Column2.HeaderText = "จำนวนเงินบัตรเครดิต";
            this.Column2.Name = "Column2";
            this.Column2.Width = 200;
            // 
            // ColAmount
            // 
            this.ColAmount.DataPropertyName = "ca";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "#,###,###,##0.0000";
            this.ColAmount.DefaultCellStyle = dataGridViewCellStyle13;
            this.ColAmount.HeaderText = "จำนวนเงิน";
            this.ColAmount.Name = "ColAmount";
            this.ColAmount.Width = 200;
            // 
            // CoEdit
            // 
            this.CoEdit.HeaderText = "";
            this.CoEdit.Name = "CoEdit";
            this.CoEdit.Width = 50;
            // 
            // ColDelete
            // 
            this.ColDelete.HeaderText = "";
            this.ColDelete.Name = "ColDelete";
            this.ColDelete.Width = 50;
            // 
            // FormRemittanceSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormRemittanceSearch";
            this.Text = "FormRemittanceSearch";
            this.Load += new System.EventHandler(this.FormRemittanceSearch_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView GridViewDetail;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        internal System.Windows.Forms.Label LabelStationNo;
        internal System.Windows.Forms.Label LabelEmployeeCode;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label Label3;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private Custom_Controls_in_CS.ButtonZ btAdd;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAmount;
        private System.Windows.Forms.DataGridViewImageColumn CoEdit;
        private System.Windows.Forms.DataGridViewImageColumn ColDelete;
    }
}