﻿using MyPayment.FormArrears;
using MyPayment.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;
using System.Threading;

namespace MyPayment.Controllers
{
    class ArrearsController
    {
        ClassLogsService ControlLog = new ClassLogsService("Controllers", "EventLog");
        ClassLogsService ControlEventLog = new ClassLogsService("Controllers", "EventLog");
        ClassLogsService ControlErrorLog = new ClassLogsService("Controllers", "EventLog");
        ClassLogsService LogInquiry = new ClassLogsService("Inquiry", "DataLog");
        ClassLogsService LogEventInquiry = new ClassLogsService("Inquiry", "EventLog");
        ClassLogsService LogErrorInquiry = new ClassLogsService("Inquiry", "ErrorLog");
        public CultureInfo thCulture = new CultureInfo("th-TH");
        public CultureInfo usCulture = new CultureInfo("en-US");
        public ResultInquiryDebt SelectProcessInquiry(ClassUser cls, ResultInquiryDebt _r, string barcode)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultInquiryDebt rs = new ResultInquiryDebt();
            try
            {
                string strJSON = null;
            string result = JsonConvert.SerializeObject(cls);
            strJSON = ClassReadAPI.GetDataAPI("processInquiry", result);
            ResultInquiryDebt clsData = ClassReadAPI.ReadToObjectInquiry(strJSON);
            rs.Output = new List<inquiryInfoBeanList>();
            if (_r.Output != null && _r.Output.Count != 0)
            {
                if(clsData.OutputInquiryDebt.inquiryBean != null)
                {
                    foreach (inquiryInfoBeanList item in clsData.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
                    {
                        List<inquiryInfoBeanList> resulDelete = _r.Output.Where(c => c.inquiryGroupDebtBeanList == null || c.inquiryGroupDebtBeanList.Count == 0).ToList();
                        foreach (var itemDelete in resulDelete)
                        {
                            _r.Output.Remove(itemDelete);
                        }
                        List<inquiryInfoBeanList> lstData = _r.Output.Where(c => c.ca == item.ca).ToList();
                        if (lstData.Count == 0)
                            rs = AddDatainquiryDebt(clsData, _r);
                        else
                        {
                            rs.ResultMessage = "มีข้อมูล " + item.ca + " อยู่แล้ว";
                            return rs;
                        }
                    }
                }
                if (clsData.ResultMessage != null)
                {
                    LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                    rs.ResultMessage = clsData.ResultMessage.ToString();
                }
            }
            else
            {
                if (clsData.OutputInquiryDebt != null && clsData.ResultMessage == null)
                    rs = AddDatainquiry(clsData);
                else
                {
                    if (clsData.ResultMessage != "SUCCESS" && clsData.OutputInquiryDebt.inquiryBean != null)
                    {
                        LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                        rs.ResultMessage = clsData.ResultMessage.ToString();
                    }
                    if (clsData.OutputInquiryDebt.inquiryBean != null)
                    {
                        foreach (inquiryInfoBeanList item in clsData.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
                        {
                            string name = "";
                            inquiryInfoBeanList models = new inquiryInfoBeanList();
                            if (item.ResultMessage != null)
                            {
                                models.ResultMessage = item.ResultMessage;
                                LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                                return rs;
                            }
                            else
                            {
                                name = item.Title + " " + item.FirstName + " " + item.LastName;
                                models.custId = item.custId;
                                models.ca = item.ca;
                                models.ui = (item.ui != null) ? item.ui : "";
                                models.FirstName = name;
                                models.coAddress = item.coAddress;
                                models.custType = (item.custType != null) ? item.custType : "";
                                models.payeeEleName = (item.payeeEleName != null) ? item.payeeEleName : "";
                                models.payeeEleAddress = (item.payeeEleAddress != null) ? item.payeeEleAddress : "";
                                models.payeeFlag = item.PayeeFlag;
                                models.collId = (item.collId != null) ? item.collId : "";
                                models.Tax20 = (item.Tax20 != null) ? item.Tax20 : "";
                                models.TaxBranch = (item.TaxBranch != null) ? item.TaxBranch : "";
                                models.Status = true;
                                models.SelectCheck = true;
                                models.billVatCode = item.billVatCode;
                                rs.Output.Add(models);
                            }
                            LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                            rs.ResultMessage = clsData.ResultMessage.ToString();
                        }
                    }
                }
            }
            LogInquiry.WriteData("Success ==> " + barcode + " || Data ::" + strJSON, method, LogLevel.Debug);
        }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
        public ResultInquiryDebt AddDatainquiry(ResultInquiryDebt result)
        {
            ResultInquiryDebt rs = new ResultInquiryDebt();
            rs.Output = new List<inquiryInfoBeanList>();
            List<inquiryInfoBeanList> _lstInfo = new List<inquiryInfoBeanList>();
            List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
            foreach (var item in result.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
            {
                string type = "";
                string name = "";
                decimal amountEle = 0;
                decimal TotalAmount = 0;
                decimal TotalVat = 0;
                decimal TotalDefaultpenalty = 0;
                inquiryInfoBeanList models = new inquiryInfoBeanList();
                name = item.Title + " " + item.FirstName + " " + item.LastName;
                models.custId = item.custId;
                models.ca = item.ca;
                models.ui = (item.ui != null) ? item.ui : "";
                models.FirstName = name;
                models.coAddress = item.coAddress;
                models.custType = (item.custType != null) ? item.custType : "";
                models.payeeEleName = (item.payeeEleName != null) ? item.payeeEleName : "";
                models.payeeEleAddress = (item.payeeEleAddress != null) ? item.payeeEleAddress : "";
                models.SelectCheck = true;
                models.payeeFlag = item.PayeeFlag;
                models.collId = (item.collId != null) ? item.collId : "";
                models.Tax20 = (item.Tax20 != null) ? item.Tax20 : "";
                models.TaxBranch = (item.TaxBranch != null) ? item.TaxBranch : "";
                models.Status = true;
                models.lockCheq = (item.lockCheq != null) ? item.lockCheq : "";
                models.billVatCode = item.billVatCode;
                if (item.inquiryGroupDebtBeanList != null)
                {
                    var selectData = item.inquiryGroupDebtBeanList.Where(c => c.debtType == "DIS").FirstOrDefault();
                    if (selectData != null)
                    {
                        if (selectData.debtType == "DIS")
                        {
                            FormProcessingFeeElectricity process = new FormProcessingFeeElectricity(selectData.debtIdSet);
                            process.ShowDialog();
                            process.Dispose();
                        }
                    }
                    foreach (var itemDetail in item.inquiryGroupDebtBeanList)
                    {
                        inquiryGroupDebtBeanList modelsDetail = new inquiryGroupDebtBeanList();
                        modelsDetail.debtIdSet = itemDetail.debtIdSet;
                        modelsDetail.debtId = itemDetail.debtIdSet[0].ToString();
                        modelsDetail.debtType = itemDetail.debtType;
                        modelsDetail.distId = itemDetail.distId;
                        modelsDetail.reqNo = itemDetail.reqNo;
                        modelsDetail.invNo = itemDetail.invNo;
                        modelsDetail.custId = itemDetail.custId;
                        modelsDetail.ca = itemDetail.ca;
                        modelsDetail.ui = itemDetail.ui;
                        modelsDetail.debtDate = itemDetail.debtDate;
                        modelsDetail.dueDate = itemDetail.dueDate;
                        modelsDetail.percentVat = itemDetail.percentVat;
                        modelsDetail.amount = itemDetail.amount;
                        modelsDetail.vat = itemDetail.vat;
                        modelsDetail.totalAmount = itemDetail.totalAmount;
                        modelsDetail.debtBalance = itemDetail.debtBalance;
                        modelsDetail.vatBalance = itemDetail.vatBalance;
                        modelsDetail.debtDesc = itemDetail.debtDesc;
                        modelsDetail.reqDesc = itemDetail.reqDesc;
                        modelsDetail.kwh = itemDetail.kwh;
                        modelsDetail.ftRate = itemDetail.ftRate;
                        modelsDetail.schdate = itemDetail.schdate;
                        modelsDetail.tarifId = itemDetail.tarifId;
                        modelsDetail.custType = itemDetail.custType;
                        modelsDetail.debtInsIntFlag = itemDetail.debtInsIntFlag;
                        modelsDetail.intLockFlag = itemDetail.intLockFlag;
                        modelsDetail.intLockDate = itemDetail.intLockDate;
                        modelsDetail.invNewDueDate = itemDetail.invNewDueDate;
                        modelsDetail.installmentsFlag = itemDetail.installmentsFlag;
                        modelsDetail.postponeFlag = itemDetail.postponeFlag;
                        modelsDetail.createdProcess = itemDetail.createdProcess;
                        modelsDetail.totalIntDay = itemDetail.totalIntDay;//จำนวนเบี้ยปรับ
                        modelsDetail.totalInt = itemDetail.totalInt;//เบี้ยปรับ
                        modelsDetail.directDebitFlag = itemDetail.directDebitFlag;
                        modelsDetail.directDebitDate = itemDetail.directDebitDate;
                        modelsDetail.totalPayment = itemDetail.totalPayment;
                        modelsDetail.status = true;
                        modelsDetail.StatusCheq = false;
                        modelsDetail.AmountBalance = "0";
                        modelsDetail.cusName = name;
                        modelsDetail.StatusDefaultpenalty = false;
                        modelsDetail.statusCancel = true;
                        modelsDetail.calTotalIntDay = itemDetail.calTotalIntDay;//จำนวนเบี้ยปรับ
                        modelsDetail.calTotalInt = itemDetail.calTotalInt;//เบี้ยปรับ
                        #region                   
                        string amountFine = "0";
                        if (itemDetail.debtType == "CHQ" && itemDetail.debtInsHdrId is null || Convert.ToDouble(itemDetail.totalAmount) > 10000 && itemDetail.debtType == "ELE" || itemDetail.debtType == "ELO" && itemDetail.debtInsHdrId is null)
                        {
                            DateTime dueDate;
                            DateTime dateNow;
                            double countFine = 0;
                            if (!(itemDetail.intLockDate is null))
                                dueDate = dueDate = DateTime.Parse(itemDetail.intLockDate);
                            else if (!(itemDetail.paymentDate is null))
                                dueDate = dueDate = DateTime.Parse(itemDetail.paymentDate);
                            else if (!(itemDetail.invNewDueDate is null))
                                dueDate = DateTime.Parse(itemDetail.invNewDueDate);
                            else
                                dueDate = DateTime.Parse(itemDetail.dueDate);
                            //dateNow = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                            dateNow = DateTime.Now;
                            string morRate = "0";
                            var dtCountDay = dateNow - dueDate;
                            if (itemDetail.debtType == "ELE")
                            {
                                if (item.custType != "GV" && dateNow > dueDate)
                                {
                                    if (itemDetail.interest != null)
                                        morRate = itemDetail.interest.ToString();
                                    countFine += Convert.ToDouble(itemDetail.amount) * Math.Abs(dtCountDay.Days) * (Convert.ToDouble(morRate) / 100) / 365;
                                    modelsDetail.countDay = Math.Abs(dtCountDay.Days);
                                }
                            }
                            else
                            {
                                if (item.custType != "GV" && DateTime.Now > Convert.ToDateTime(itemDetail.dueDate))
                                    countFine += Convert.ToDouble(itemDetail.amount) * Math.Abs(dtCountDay.Days) * (Convert.ToDouble(morRate) / 100) / 365;
                            }
                            amountFine = countFine.ToString("#,###,###,##0.00");
                            modelsDetail.Defaultpenalty = Convert.ToDecimal(amountFine);
                            modelsDetail.countDayNew = Math.Abs(dtCountDay.Days);
                            modelsDetail.DefaultpenaltyNew = Convert.ToDecimal(amountFine);
                        }
                        else
                        {
                            amountFine = "0";
                            modelsDetail.countDay = 0;
                            modelsDetail.Defaultpenalty = 0;
                        }
                        #endregion                        
                        modelsDetail.Defaultpenalty = Convert.ToDecimal(itemDetail.calTotalInt);
                        modelsDetail.countDay = (itemDetail.calTotalIntDay != null) ? itemDetail.calTotalIntDay.Value : 0;
                        modelsDetail.countDayNew = (itemDetail.calTotalIntDay != null) ? itemDetail.calTotalIntDay.Value : 0;
                        modelsDetail.DefaultpenaltyNew = Convert.ToDecimal(itemDetail.calTotalInt);
                        type = itemDetail.debtType;
                        _lstDebt.Add(modelsDetail);
                        TotalDefaultpenalty += Convert.ToDecimal(itemDetail.calTotalInt);
                        amountEle += Convert.ToDecimal(itemDetail.amount);
                        TotalAmount += Convert.ToDecimal(itemDetail.debtBalance);
                        TotalVat += Convert.ToDecimal(itemDetail.vat);
                    }
                }
                models.EleTotalAmount = amountEle;
                models.EleTotalVat = TotalVat;
                models.TotalAmount = TotalAmount;
                models.Defaultpenalty = TotalDefaultpenalty.ToString();
                models.payeeEleTax20 = type == "SER" ? item.payeeSerTax20 : item.payeeEleTax20;
                models.payeeEleTaxBranch = type == "SER" ? item.payeeSerTaxBranch : item.payeeEleTaxBranch;
                models.inquiryGroupDebtBeanList = _lstDebt;
                _lstInfo.Add(models);
                rs.Output.Add(models);
            }
            GlobalClass.LstInfo = _lstInfo;
            return rs;
        }
        public ResultInquiryDebt AddDatainquiryDebt(ResultInquiryDebt result, ResultInquiryDebt _r)
        {
            ResultInquiryDebt rs = new ResultInquiryDebt();
            rs.Output = new List<inquiryInfoBeanList>();
            List<inquiryInfoBeanList> _lstInfo = new List<inquiryInfoBeanList>();
            if (_r.Output != null)
            {
                foreach (var item in _r.Output)
                {
                    List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
                    inquiryInfoBeanList models = new inquiryInfoBeanList();
                    models.custId = item.custId;
                    models.ca = item.ca;
                    models.ui = item.ui;
                    models.FirstName = item.FirstName;
                    models.coAddress = item.coAddress;
                    models.custType = (item.custType != null) ? item.custType : "";
                    models.payeeEleName = (item.payeeEleName != null) ? item.payeeEleName : "";
                    models.payeeEleTax20 = (item.payeeEleTax20 != null) ? item.payeeEleTax20 : "";
                    models.payeeEleAddress = (item.payeeEleAddress != null) ? item.payeeEleAddress : "";
                    models.payeeEleTaxBranch = (item.payeeEleTaxBranch != null) ? item.payeeEleTaxBranch : "";
                    models.SelectCheck = item.Status;
                    models.EleInvCount = (item.EleInvCount != null) ? item.EleInvCount : "";
                    models.EleTotalAmount = (item.EleTotalAmount != 0) ? item.EleTotalAmount : 0;
                    models.OtherTotalAmount = (item.OtherTotalAmount != null) ? item.OtherTotalAmount : "";
                    models.EleTotalVat = (Convert.ToDecimal(item.EleTotalVat) != 0) ? Convert.ToDecimal(item.EleTotalVat) : 0;
                    models.OtherTotalVat = (item.OtherTotalVat != null) ? item.OtherTotalVat : "";
                    models.lockBill = (item.lockBill != null) ? item.lockBill : null;
                    models.debtType = Convert.ToBoolean((item.debtType == true) ? true : false);
                    models.Defaultpenalty = (item.Defaultpenalty != null) ? item.Defaultpenalty : "0";
                    models.TotalAmount = (item.TotalAmount != 0) ? Convert.ToDecimal(item.TotalAmount) : 0;
                    models.collId = (item.collId != null) ? item.collId : "";
                    models.Tax20 = (item.Tax20 != null) ? item.Tax20 : "";
                    models.TaxBranch = (item.TaxBranch != null) ? item.TaxBranch : "";
                    models.Status = item.Status;
                    models.lockCheq = (item.lockCheq != null) ? item.lockCheq : "";
                    if (item.inquiryGroupDebtBeanList != null)
                    {
                        foreach (var itemDetail in item.inquiryGroupDebtBeanList)
                        {
                            inquiryGroupDebtBeanList modelsDetail = new inquiryGroupDebtBeanList();
                            modelsDetail.debtIdSet = itemDetail.debtIdSet;
                            modelsDetail.debtId = itemDetail.debtId;
                            modelsDetail.debtType = itemDetail.debtType;
                            modelsDetail.distId = itemDetail.distId;
                            modelsDetail.reqNo = itemDetail.reqNo;
                            modelsDetail.invNo = itemDetail.invNo;
                            modelsDetail.custId = itemDetail.custId;
                            modelsDetail.ca = itemDetail.ca;
                            modelsDetail.ui = itemDetail.ui;
                            modelsDetail.debtDate = itemDetail.debtDate;
                            modelsDetail.dueDate = itemDetail.dueDate;
                            modelsDetail.percentVat = itemDetail.percentVat;
                            modelsDetail.amount = itemDetail.amount;
                            modelsDetail.vat = itemDetail.vat;
                            modelsDetail.totalAmount = itemDetail.totalAmount;
                            modelsDetail.debtBalance = itemDetail.debtBalance;
                            modelsDetail.vatBalance = itemDetail.vatBalance;
                            modelsDetail.debtDesc = itemDetail.debtDesc;
                            modelsDetail.reqDesc = itemDetail.reqDesc;
                            modelsDetail.kwh = itemDetail.kwh;
                            modelsDetail.ftRate = itemDetail.ftRate;
                            modelsDetail.schdate = itemDetail.schdate;
                            modelsDetail.tarifId = itemDetail.tarifId;
                            modelsDetail.custType = itemDetail.custType;
                            modelsDetail.debtInsIntFlag = itemDetail.debtInsIntFlag;
                            modelsDetail.intLockFlag = itemDetail.intLockFlag;
                            modelsDetail.intLockDate = itemDetail.intLockDate;
                            modelsDetail.invNewDueDate = itemDetail.invNewDueDate;
                            modelsDetail.installmentsFlag = itemDetail.installmentsFlag;
                            modelsDetail.postponeFlag = itemDetail.postponeFlag;
                            modelsDetail.createdProcess = itemDetail.createdProcess;
                            modelsDetail.totalIntDay = itemDetail.totalIntDay;//จำนวนเบี้ยปรับ
                            modelsDetail.totalInt = itemDetail.totalInt;//เบี้ยปรับ
                            modelsDetail.directDebitFlag = itemDetail.directDebitFlag;
                            modelsDetail.directDebitDate = itemDetail.directDebitDate;
                            modelsDetail.totalPayment = itemDetail.totalPayment;
                            modelsDetail.status = itemDetail.status;
                            modelsDetail.StatusCheq = itemDetail.StatusCheq;
                            modelsDetail.AmountBalance = itemDetail.AmountBalance;
                            modelsDetail.cusName = item.FirstName;
                            modelsDetail.StatusDefaultpenalty = itemDetail.StatusDefaultpenalty;
                            modelsDetail.statusCancel = itemDetail.statusCancel;
                            modelsDetail.calTotalIntDay = itemDetail.calTotalIntDay;//จำนวนเบี้ยปรับ
                            modelsDetail.calTotalInt = itemDetail.calTotalInt;//เบี้ยปรับ
                            modelsDetail.countDay = (itemDetail.countDay != null) ? itemDetail.countDay : 0;
                            modelsDetail.Defaultpenalty = (itemDetail.Defaultpenalty != 0) ? itemDetail.Defaultpenalty : 0;
                            _lstDebt.Add(modelsDetail);
                        }
                        models.inquiryGroupDebtBeanList = _lstDebt;
                    }
                    _lstInfo.Add(models);
                    rs.Output.Add(models);
                }
            }
            if (result.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList != null)
            {
                foreach (var item in result.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
                {
                    List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
                    string type = "";
                    string name = "";
                    decimal amountEle = 0;
                    decimal TotalAmount = 0;
                    decimal TotalVat = 0;
                    decimal TotalDefaultpenalty = 0;
                    inquiryInfoBeanList models = new inquiryInfoBeanList();
                    name = item.Title + " " + item.FirstName + " " + item.LastName;
                    models.custId = item.custId;
                    models.ca = item.ca;
                    models.ui = (item.ui != null) ? item.ui : "";
                    models.FirstName = name;
                    models.coAddress = item.coAddress;
                    models.custType = (item.custType != null) ? item.custType : "";
                    models.payeeEleName = (item.payeeEleName != null) ? item.payeeEleName : "";
                    models.payeeEleAddress = (item.payeeEleAddress != null) ? item.payeeEleAddress : "";
                    models.SelectCheck = true;
                    models.payeeFlag = item.PayeeFlag;
                    models.collId = (item.collId != null) ? item.collId : "";
                    models.Tax20 = (item.Tax20 != null) ? item.Tax20 : "";
                    models.TaxBranch = (item.TaxBranch != null) ? item.TaxBranch : "";
                    models.Status = true;
                    models.lockCheq = (item.lockCheq != null) ? item.lockCheq : "";
                    if (item.inquiryGroupDebtBeanList != null || item.inquiryGroupDebtBeanList.Count != 0)
                    {
                        var selectData = item.inquiryGroupDebtBeanList.Where(c => c.debtType == "DIS").FirstOrDefault();
                        if (selectData != null)
                        {
                            if (selectData.debtType == "DIS")
                            {
                                FormProcessingFeeElectricity process = new FormProcessingFeeElectricity(selectData.debtIdSet);
                                process.ShowDialog();
                                process.Dispose();
                            }
                        }
                        foreach (var itemDetail in item.inquiryGroupDebtBeanList)
                        {
                            inquiryGroupDebtBeanList modelsDetail = new inquiryGroupDebtBeanList();
                            modelsDetail.debtIdSet = itemDetail.debtIdSet;
                            modelsDetail.debtType = itemDetail.debtType;
                            modelsDetail.debtId = itemDetail.debtIdSet[0].ToString();
                            modelsDetail.distId = itemDetail.distId;
                            modelsDetail.reqNo = itemDetail.reqNo;
                            modelsDetail.invNo = itemDetail.invNo;
                            modelsDetail.custId = itemDetail.custId;
                            modelsDetail.ca = itemDetail.ca;
                            modelsDetail.ui = itemDetail.ui;
                            modelsDetail.debtDate = itemDetail.debtDate;
                            modelsDetail.dueDate = itemDetail.dueDate;
                            modelsDetail.percentVat = itemDetail.percentVat;
                            modelsDetail.amount = itemDetail.amount;
                            modelsDetail.vat = itemDetail.vat;
                            modelsDetail.totalAmount = itemDetail.totalAmount;
                            modelsDetail.debtBalance = itemDetail.debtBalance;
                            modelsDetail.vatBalance = itemDetail.vatBalance;
                            modelsDetail.debtDesc = itemDetail.debtDesc;
                            modelsDetail.reqDesc = itemDetail.reqDesc;
                            modelsDetail.kwh = itemDetail.kwh;
                            modelsDetail.ftRate = itemDetail.ftRate;
                            modelsDetail.schdate = itemDetail.schdate;
                            modelsDetail.tarifId = itemDetail.tarifId;
                            modelsDetail.custType = itemDetail.custType;
                            modelsDetail.debtInsIntFlag = itemDetail.debtInsIntFlag;
                            modelsDetail.intLockFlag = itemDetail.intLockFlag;
                            modelsDetail.intLockDate = itemDetail.intLockDate;
                            modelsDetail.invNewDueDate = itemDetail.invNewDueDate;
                            modelsDetail.installmentsFlag = itemDetail.installmentsFlag;
                            modelsDetail.postponeFlag = itemDetail.postponeFlag;
                            modelsDetail.createdProcess = itemDetail.createdProcess;
                            modelsDetail.totalIntDay = itemDetail.totalIntDay;//จำนวนเบี้ยปรับ
                            modelsDetail.totalInt = itemDetail.totalInt;//เบี้ยปรับ
                            modelsDetail.directDebitFlag = itemDetail.directDebitFlag;
                            modelsDetail.directDebitDate = itemDetail.directDebitDate;
                            modelsDetail.totalPayment = itemDetail.totalPayment;
                            modelsDetail.status = true;
                            modelsDetail.StatusCheq = false;
                            modelsDetail.AmountBalance = "0";
                            modelsDetail.cusName = name;
                            modelsDetail.StatusDefaultpenalty = false;
                            modelsDetail.statusCancel = true;
                            modelsDetail.calTotalIntDay = itemDetail.calTotalIntDay;//จำนวนเบี้ยปรับ
                            modelsDetail.calTotalInt = itemDetail.calTotalInt;//เบี้ยปรับ
                            #region                   
                            string amountFine = "0";
                            if (Convert.ToDouble(itemDetail.totalAmount) > 10000 && itemDetail.debtType == "ELE" || itemDetail.debtType == "CHQ" || itemDetail.debtType == "ELO" && itemDetail.debtInsHdrId is null)
                            {
                                DateTime dueDate;
                                DateTime dateNow;
                                double countFine = 0;
                                if (!(itemDetail.intLockDate is null))
                                    dueDate = Convert.ToDateTime(itemDetail.intLockDate);
                                else if (!(itemDetail.paymentDate is null))
                                    dueDate = Convert.ToDateTime(itemDetail.paymentDate);
                                else if (!(itemDetail.invNewDueDate is null))
                                    dueDate = Convert.ToDateTime(itemDetail.invNewDueDate);
                                else
                                    dueDate = Convert.ToDateTime(itemDetail.dueDate);
                                dateNow = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                                string morRate = "0";
                                var dtCountDay = dateNow - dueDate;
                                if (itemDetail.debtType == "ELE")
                                {
                                    if (item.custType != "GV" && dateNow > dueDate)
                                    {
                                        if (itemDetail.interest != null)
                                            morRate = itemDetail.interest.ToString();
                                        countFine += (Convert.ToDouble(itemDetail.debtBalance) - Convert.ToDouble(itemDetail.vatBalance)) * Math.Abs(dtCountDay.Days) * (Convert.ToDouble(morRate) / 100) / 365;
                                        modelsDetail.countDay = Math.Abs(dtCountDay.Days);
                                    }
                                }
                                else
                                {
                                    if (item.custType != "GV" && DateTime.Now > Convert.ToDateTime(itemDetail.dueDate))
                                        countFine += (Convert.ToDouble(itemDetail.debtBalance) - Convert.ToDouble(itemDetail.vatBalance)) * Math.Abs(dtCountDay.Days) * (Convert.ToDouble(morRate) / 100) / 365;
                                }
                                amountFine = countFine.ToString("#,###,###,##0.00");
                                modelsDetail.Defaultpenalty = Convert.ToDecimal(amountFine);
                                modelsDetail.countDayNew = Math.Abs(dtCountDay.Days);
                                modelsDetail.DefaultpenaltyNew = Convert.ToDecimal(amountFine);
                            }
                            else
                            {
                                amountFine = "0";
                                modelsDetail.countDay = 0;
                                modelsDetail.Defaultpenalty = 0;
                            }
                            #endregion
                            modelsDetail.Defaultpenalty = Convert.ToDecimal(itemDetail.calTotalInt);
                            modelsDetail.countDay = (itemDetail.calTotalIntDay != null) ? itemDetail.calTotalIntDay.Value : 0;
                            modelsDetail.countDayNew = (itemDetail.calTotalIntDay != null) ? itemDetail.calTotalIntDay.Value : 0;
                            modelsDetail.DefaultpenaltyNew = Convert.ToDecimal(itemDetail.calTotalInt);
                            type = itemDetail.debtType;
                            _lstDebt.Add(modelsDetail);
                            TotalDefaultpenalty += Convert.ToDecimal(itemDetail.calTotalInt);
                            amountEle += Convert.ToDecimal(itemDetail.debtBalance);
                            TotalAmount += Convert.ToDecimal(itemDetail.debtBalance);
                            TotalVat += Convert.ToDecimal(itemDetail.vatBalance);
                        }
                    }
                    models.EleTotalAmount = amountEle - TotalVat;
                    models.EleTotalVat = TotalVat;
                    models.TotalAmount = TotalAmount;
                    models.Defaultpenalty = TotalDefaultpenalty.ToString();
                    models.payeeEleTax20 = type == "SER" ? item.payeeSerTax20 : item.payeeEleTax20;
                    models.payeeEleTaxBranch = type == "SER" ? item.payeeSerTaxBranch : item.payeeEleTaxBranch;
                    models.inquiryGroupDebtBeanList = _lstDebt;
                    _lstInfo.Add(models);
                    rs.Output.Add(models);
                }
            }
            GlobalClass.LstInfo = _lstInfo;
            return rs;
        }
        public ResultCostTDebt SelectDataInquiryDebtDetail(ResultInquiryDebt result, string ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultCostTDebt rs = new ResultCostTDebt();
            try
            {
                #region
                List<inquiryGroupDebtBeanList> _list = new List<inquiryGroupDebtBeanList>();
                if (result.Output.Count > 0)
                {
                    inquiryInfoBeanList lstInfo = result.Output.Where(c => c.ca == ca).FirstOrDefault();
                    List<inquiryGroupDebtBeanList> lstData = lstInfo.inquiryGroupDebtBeanList.OrderBy(c => c.schdate).ToList();
                    if (lstData.Count > 0)
                    {
                        rs.Output = new List<inquiryGroupDebtBeanList>();
                        int Invcount = 0;
                        foreach (inquiryGroupDebtBeanList item in lstData)
                        {
                            decimal TotalAmount = 0;
                            decimal OtherTotalAmount = 0;
                            decimal VatTotalAmount = 0;
                            decimal VatOtherTotalAmount = 0;
                            inquiryGroupDebtBeanList models = new inquiryGroupDebtBeanList();
                            if (item.ResultMessage != null)
                            {
                                models.ResultMessage = item.ResultMessage;
                                return rs;
                            }
                            else
                            {
                                models.debtIdSet = item.debtIdSet;
                                models.SelectCheck = item.status;
                                models.debtId = item.debtId;
                                models.schdate = (item.schdate != null) ? Convert.ToDateTime(item.schdate).ToString("dd/MM/yyyy") : "";
                                models.totalkWh = item.kwh;
                                models.invNo = item.invNo;
                                models.debtDate = (item.debtDate != null) ? Convert.ToDateTime(item.debtDate).ToString("dd/MM/yyyy") : "";
                                models.dueDate = (item.dueDate != null) ? Convert.ToDateTime(item.dueDate).ToString("dd/MM/yyyy") : "";
                                models.amountNew = item.amount;
                                models.vatBalance = item.vat;
                                models.debtBalance = item.debtBalance;
                                if (item.debtType == "MWA")
                                    models.Payment = item.Payment;
                                else
                                    models.Payment = (item.Payment != 0) ? item.Payment : Convert.ToDecimal(item.debtBalance);
                                models.debtDesc = item.debtDesc;
                                models.custId = item.custId;
                                models.ca = item.ca;
                                models.invNewDueDate = (item.invNewDueDate != null) ? Convert.ToDateTime(item.invNewDueDate).ToString("dd/MM/yyyy") : "";
                                models.lockBill = item.lockBill;
                                models.ftRate = item.ftRate;
                                models.receiptDetailDesc = item.receiptDetailDesc;
                                models.debtType = item.debtType;
                                models.StatusDefaultpenalty = item.StatusDefaultpenalty;
                                models.countDay = item.countDay != 0 ? item.countDay : 0;
                                models.Defaultpenalty = item.Defaultpenalty != 0 ? item.Defaultpenalty : 0;
                                if (item.debtType == "ELE")
                                {
                                    Invcount++;
                                    TotalAmount += item.debtBalance;
                                    VatTotalAmount += Convert.ToDecimal(item.vatBalance);
                                }
                                else
                                {
                                    OtherTotalAmount += item.debtBalance;
                                    VatOtherTotalAmount += Convert.ToDecimal(item.vatBalance);
                                }
                                models.EleInvCount = Invcount;
                                models.EleTotalAmount = TotalAmount;
                                models.OtherTotalAmount = OtherTotalAmount;
                                models.VatEleTotalAmount = VatTotalAmount;
                                models.VatOtherTotalAmount = VatOtherTotalAmount;
                                rs.Output.Add(models);
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ControlErrorLog = new ClassLogsService("ErrorLog", "");
                ControlErrorLog.WriteData("Controller :: ArrearsController | Function :: SelectDataCustomerDetail()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
        public ResultCostTDebt LoadDataContinue()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultCostTDebt rs = new ResultCostTDebt();
            try
            {
                #region
                if (GlobalClass.LstCost.Count > 0)
                {
                    List<inquiryGroupDebtBeanList> clsData = GlobalClass.LstCost.Where(c => c.debtType == "").ToList();
                    if (clsData.Count > 0)
                    {
                        rs.Output = new List<inquiryGroupDebtBeanList>();
                        foreach (inquiryGroupDebtBeanList item in clsData)
                        {
                            inquiryGroupDebtBeanList models = new inquiryGroupDebtBeanList();
                            if (item.ResultMessage != null)
                            {
                                models.ResultMessage = item.ResultMessage;
                                return rs;
                            }
                            else
                            {
                                models.cusName = item.cusName;
                                models.debtIdSet = item.debtIdSet;
                                models.custId = item.custId;
                                models.ca = item.ca;
                                models.ui = item.ui != null ? item.ui : item.ui;
                                rs.Output.Add(models);
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
        public inquiryGroupDebtBeanList UpdateStatus(string Id, bool status, decimal amount, string ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            inquiryGroupDebtBeanList _costDebt = new inquiryGroupDebtBeanList();
            try
            {
                inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == ca).FirstOrDefault();
                inquiryGroupDebtBeanList costTDebt = info.inquiryGroupDebtBeanList.Find(item => item.debtId.ToString().Equals(Id, StringComparison.CurrentCultureIgnoreCase));
                if (costTDebt != null)
                {
                    costTDebt.status = status;
                    if (amount != 0)
                    {
                        string perVat;
                        if (Convert.ToInt16(costTDebt.percentVat) >= 10)
                            perVat = "1." + costTDebt.percentVat;
                        else
                            perVat = "1.0" + costTDebt.percentVat;
                        decimal vat = amount - (amount / Convert.ToDecimal(perVat));
                        costTDebt.Payment = amount;
                        costTDebt.amount = amount - vat;
                        costTDebt.vatBalance = vat.ToString("#,###,###,##0.00");
                        costTDebt.totalAmount = amount.ToString();
                        costTDebt.debtBalance = amount;
                        _costDebt.percentVat = perVat;
                        _costDebt.vatBalance = vat.ToString("#,###,###,##0.00");
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return _costDebt;
        }
        public bool UpdateStatusDefaultpenalty(bool _status, string _id, string _ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == _ca).FirstOrDefault();
                inquiryGroupDebtBeanList costTDebt = info.inquiryGroupDebtBeanList.Find(item => item.debtIdSet[0].ToString().Equals(_id, StringComparison.CurrentCultureIgnoreCase));
                if (costTDebt == null)
                {
                    string debtIdDetail = costTDebt.debtIdSet[0].ToString();
                    var Datadebt = costTDebt.debtIdSet.Where(c => c.ToString() == _id).FirstOrDefault();
                    if (Datadebt.ToString() != "")
                        costTDebt = info.inquiryGroupDebtBeanList.Find(item => item.debtIdSet[0].ToString().Equals(debtIdDetail, StringComparison.CurrentCultureIgnoreCase));
                }
                if (costTDebt != null)
                {
                    inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(item => item.ca.Equals(costTDebt.ca, StringComparison.CurrentCultureIgnoreCase));
                    if (userInfo != null)
                    {
                        if (_status)
                        {
                            info.Defaultpenalty = (Convert.ToDecimal(info.Defaultpenalty) - costTDebt.Defaultpenalty).ToString();
                            costTDebt.Defaultpenalty = 0;
                            costTDebt.countDay = 0;
                            _status = true;
                        }
                        else
                        {
                            info.Defaultpenalty = (Convert.ToDecimal(info.Defaultpenalty) + costTDebt.DefaultpenaltyNew).ToString();
                            costTDebt.Defaultpenalty = costTDebt.DefaultpenaltyNew;
                            costTDebt.countDay = costTDebt.countDayNew;
                            _status = false;
                        }
                    }
                    costTDebt.StatusDefaultpenalty = _status;
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                tf = false;
            }
            return tf;
        }
        public bool UpdateStatusUserInfo(string Ca, bool status, decimal amount, decimal vat, string defaultpenalty)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(item => item.ca.Equals(Ca, StringComparison.CurrentCultureIgnoreCase));
                if (userInfo != null)
                {
                    userInfo.Status = status;
                    userInfo.SelectCheck = status;
                    if (amount != 0)
                    {
                        userInfo.EleTotalAmount = amount;
                        userInfo.TotalAmount = amount;
                        userInfo.EleTotalVat = vat;
                        userInfo.Defaultpenalty = defaultpenalty;
                    }
                    else
                    {
                        if (!status)
                        {
                            userInfo.TotalAmount = 0;
                            userInfo.EleTotalVat = 0;
                            userInfo.Defaultpenalty = "0";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                tf = false;
            }
            return tf;
        }
        public string MyXor(string strConvert)
        {
            string strReturn = "";
            int intStr = 0;
            var loopTo = strConvert.Length - 1;
            for (int x = 0; x <= loopTo; x++)
                intStr = intStr ^ strConvert[x];
            strReturn = intStr.ToString();
            return strReturn;
        }
        public string GetIPAddress()
        {
            string ipAddresses = "";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    ipAddresses = ip.ToString();
            }
            return ipAddresses;
        }
        public string ThaiBaht(decimal number)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            string bahtTxt, n, bahtTH = "";
            try
            {
                decimal amount;
                try { amount = Convert.ToDecimal(number); }
                catch { amount = 0; }
                bahtTxt = amount.ToString("####.00");
                string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
                string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
                string[] temp = bahtTxt.Split('.');
                string intVal = temp[0];
                string decVal = temp[1];
                if (Convert.ToDouble(bahtTxt) == 0)
                    bahtTH = "ศูนย์บาทถ้วน";
                else
                {
                    for (int i = 0; i < intVal.Length; i++)
                    {
                        n = intVal.Substring(i, 1);
                        if (n != "0")
                        {
                            if ((i == (intVal.Length - 1)) && (n == "1"))
                                bahtTH += "เอ็ด";
                            else if ((i == (intVal.Length - 2)) && (n == "2"))
                                bahtTH += "ยี่";
                            else if ((i == (intVal.Length - 2)) && (n == "1"))
                                bahtTH += "";
                            else
                                bahtTH += num[Convert.ToInt32(n)];
                            bahtTH += rank[(intVal.Length - i) - 1];
                        }
                    }
                    bahtTH += "บาท";
                    if (decVal == "00")
                        bahtTH += "ถ้วน";
                    else
                    {
                        for (int i = 0; i < decVal.Length; i++)
                        {
                            n = decVal.Substring(i, 1);
                            if (n != "0")
                            {
                                if ((i == decVal.Length - 1) && (n == "1"))
                                    bahtTH += "เอ็ด";
                                else if ((i == (decVal.Length - 2)) && (n == "2"))
                                    bahtTH += "ยี่";
                                else if ((i == (decVal.Length - 2)) && (n == "1"))
                                    bahtTH += "";
                                else
                                    bahtTH += num[Convert.ToInt32(n)];
                                bahtTH += rank[(decVal.Length - i) - 1];
                            }
                        }
                        bahtTH += "สตางค์";
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return bahtTH;
        }
        public double Fraction(int number)
        {
            double Satang = 0;
            if (number <= 12)
                Satang = 0;
            else if (number <= 37)
                Satang = 0.25;
            else if (number <= 62)
                Satang = 0.50;
            else if (number <= 87)
                Satang = 0.75;
            else
                Satang = 1;
            return Satang;
        }
        public string GetBankDesc(string code)
        {
            string jsonData = ClassReadAPI.GetBank(code);
            dynamic data = JObject.Parse(jsonData);
            return data.bankDesc;
        }
        public ResultExpenseItem GetExpenseItem(int status)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultExpenseItem rs = new ResultExpenseItem();
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var jsonData = ClassReadAPI.GetItemCode("inquiryProfitCenterAndExpenseItemList");
                ClassItemCode expenseItem = new ClassItemCode();
                expenseItem = new JavaScriptSerializer().Deserialize<ClassItemCode>(jsonData);
                rs.Output = new List<listExpenseItem>();
                rs.OutputPro = new List<listProfitCenter>();
                rs.OutputUnit = new List<costUnit>();
                if (status == 0)
                {
                    foreach (var item in expenseItem.listProfitCenter)
                    {
                        listProfitCenter itemPro = new listProfitCenter();
                        itemPro.profitCenter = item.profitCenter;
                        itemPro.distShortDesc = item.distShortDesc;
                        rs.OutputPro.Add(itemPro);
                    }
                }
                else if (status == 1)
                {
                    foreach (var item in expenseItem.listExpenseItem)
                    {
                        listExpenseItem itemEx = new listExpenseItem();
                        itemEx.itemCode = item.itemCode;
                        itemEx.itemAccountCode = item.itemAccountCode;
                        itemEx.itemDescEn = item.itemDescEn;
                        itemEx.itemDescTh = item.itemCode + " " + item.itemDescTh;
                        itemEx.itemId = item.itemId;
                        rs.Output.Add(itemEx);
                    }
                }
                else
                {
                    foreach (var item in expenseItem.costUnit)
                    {
                        costUnit itemIUnit = new costUnit();
                        itemIUnit.unitId = item.unitId;
                        itemIUnit.unitCode = item.unitCode;
                        itemIUnit.unitDetail = item.unitDetail;
                        rs.OutputUnit.Add(itemIUnit);
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
        public ResultDisplayPaytment GetDisplayPaytment(ClassDisplayPaytment cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultDisplayPaytment rs = new ResultDisplayPaytment();
            try
            {
                string strJSON = null;
                string result = JsonConvert.SerializeObject(cls);
                strJSON = ClassReadAPI.GetDataAPI("displayPaymentReceipt", result);
                ResultDisplayPaytment clsData = ClassReadAPI.ReadToObjectDisplayPaytment(strJSON);
                rs.Output = new List<DisplayPaytmentReceiptList>();
                if (clsData.Output != null)
                {
                    foreach (DisplayPaytmentReceiptList item in clsData.Output)
                    {
                        DisplayPaytmentReceiptList models = new DisplayPaytmentReceiptList();
                        models.DistId = item.DistId;
                        models.DistName = item.DistName;
                        models.PaymentId = item.PaymentId;
                        models.ReceiptNo = item.ReceiptNo;
                        models.ReceiptDate = item.ReceiptDate;
                        models.DueDate = item.DueDate;
                        models.ReceiptPayer = item.ReceiptPayer;
                        models.ReceiptCustId = item.ReceiptCustId;
                        models.ReceiptCustName = item.ReceiptCustName;
                        models.ReceiptCustTaxid = item.ReceiptCustTaxid;
                        models.ReceiptCustBranch = item.ReceiptCustBranch;
                        models.ReceiptReqAddress = item.ReceiptReqAddress;
                        models.ReceiptCa = item.ReceiptCa;
                        models.ReceiptUi = item.ReceiptUi;
                        models.DebtId = item.DebtId;
                        models.DebtType = item.DebtType;
                        models.DebtName = item.DebtName;
                        models.ItemId = item.ItemId;
                        models.ItemCode = item.ItemCode;
                        models.ItemDescTh = item.ItemDescTh;
                        models.PaidExcAmount = item.PaidExcAmount;
                        models.PaidVatPercent = item.PaidVatPercent;
                        models.PaidVatAmount = item.PaidVatAmount;
                        models.PaymentIntAmount = item.PaymentIntAmount;
                        models.PaymentIntDay = item.PaymentIntDay;
                        models.TtlPaidIncAmount = item.TtlPaidIncAmount;
                        models.ReqId = item.ReqId;
                        models.RecStatus = item.RecStatus;
                        rs.Output.Add(models);
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
        public ResultRemittance SearchDataRemittance(ClassUser cls)
        {
            ResultRemittance result = new ResultRemittance();

            return result;
        }
        public ResultRemittanceSlip InsertRemittance(ClassPayIn mylist)
        {
            ResultRemittanceSlip rs = new ResultRemittanceSlip();
            string result = JsonConvert.SerializeObject(mylist);
            var strJSON = ClassReadAPI.GetDataAPITest("directPaymentPayIn", result);
            rs = new JavaScriptSerializer().Deserialize<ResultRemittanceSlip>(strJSON);
            return rs;
        }
        public void AddDataCostTDebt(ResultInquiryDebt userInfo)
        {

        }
        public ClassUser loadDataLogin(ClassLogin cls, string url)
        {
            string strJSON = null;
            string result = JsonConvert.SerializeObject(cls);
            strJSON = ClassReadAPI.GetDataAPI(url, result);
            ClassUser clsLogin = ClassReadAPI.ReadToObject(strJSON);
            return clsLogin;
        }
        public ClassUser ChangePassword(ClassUser cls, string url)
        {
            string strJSON = null;
            string result = JsonConvert.SerializeObject(cls);
            strJSON = ClassReadAPI.GetDataAPI(url, result);
            ClassUser clsLogin = ClassReadAPI.ReadToObject(strJSON);
            return clsLogin;
        }
        public bool UpdateCostTDebt(List<inquiryInfoBeanList> lstInfo, int type, string count)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                foreach (var item in lstInfo)
                {
                    bool status = false;
                    decimal vat = 0;
                    decimal payment = 0;
                    decimal fine = 0;
                    if (type == 1)
                    {
                        int i = 0;
                        inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == item.ca).FirstOrDefault();
                        foreach (var itemDetail in info.inquiryGroupDebtBeanList.OrderBy(c => c.dueDate).ToList())
                        {
                            if (i < Convert.ToInt32(count))
                            {
                                itemDetail.status = true;
                                fine += itemDetail.Defaultpenalty;
                                if (itemDetail.vatBalance != "")
                                    vat += Convert.ToDecimal(itemDetail.vatBalance);
                                payment += Convert.ToDecimal(itemDetail.debtBalance);
                                status = true;
                            }
                            else
                                itemDetail.status = false;
                            i++;
                        }
                        UpdateStatusUserInfo(info.ca, status, payment, vat, fine.ToString());
                    }
                    else
                    {
                        inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == item.ca).FirstOrDefault();
                        foreach (var itemDetail in info.inquiryGroupDebtBeanList.Where(c => c.debtType == "GUA").ToList())
                        {
                            itemDetail.status = false;
                            payment += itemDetail.Payment;
                            fine += itemDetail.Defaultpenalty;
                            if (itemDetail.vatBalance != "")
                                vat += Convert.ToDecimal(itemDetail.vatBalance);
                            UpdateStatusUserInfo(info.ca, false, payment, vat, fine.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                tf = false;
            }
            return tf;
        }
        public string PrintDate(int mouth)
        {
            string str = "";
            if (mouth == 1)
                str = "มกราคม";
            else if (mouth == 2)
                str = "กุมภาพันธ์";
            else if (mouth == 3)
                str = "มีนาคม";
            else if (mouth == 4)
                str = "เมษายน";
            else if (mouth == 5)
                str = "พฤษภาคม";
            else if (mouth == 6)
                str = "มิถุนายน";
            else if (mouth == 7)
                str = "กรกฏาคม";
            else if (mouth == 8)
                str = "สิงหาคม";
            else if (mouth == 9)
                str = "กันยายน";
            else if (mouth == 10)
                str = "ตุลาคม";
            else if (mouth == 11)
                str = "พฤษจิกายน";
            else
                str = "ธันวาคม";
            return str;
        }
        public ResultInquiryDebt addDataWater(ClassItemWater _water)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultInquiryDebt rs = new ResultInquiryDebt();
            try
            {
                List<inquiryInfoBeanList> _lstInfo = new List<inquiryInfoBeanList>();
                List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
                rs.Output = new List<inquiryInfoBeanList>();
                if (GlobalClass.LstInfo == null)
                    GlobalClass.LstInfo = new List<inquiryInfoBeanList>();

                inquiryInfoBeanList model = new inquiryInfoBeanList();
                model.ca = _water.customerName;
                model.EleTotalAmount = _water.totalAmount;
                model.EleTotalVat = _water.VatAmount;
                model.SelectCheck = true;
                model.lockCheq = "";
                model.Status = true;
                model.OtherTotalAmount = "0";
                model.OtherTotalVat = "0";
                model.Defaultpenalty = "0";
                #region Detail
                inquiryGroupDebtBeanList modelDetail = new inquiryGroupDebtBeanList();
                modelDetail.amount = Convert.ToDecimal(_water.Amount);
                modelDetail.percentVat = _water.vatType;
                modelDetail.vatBalance = _water.VatAmount.ToString("#,###,###,##0.00");
                modelDetail.totalAmount = _water.totalAmount.ToString("#,###,###,##0.00");
                modelDetail.debtBalance = _water.totalAmount;
                modelDetail.Payment = Math.Round(_water.totalAmount, 2);
                modelDetail.dueDate = Substring(Convert.ToString(_water.dueDate));
                modelDetail.unit = _water.meaUnit;
                modelDetail.debtType = "MWA";
                modelDetail.mwaTaxId = _water.mwaTaxId;
                modelDetail.customerCode = _water.customerCode;
                modelDetail.billDueDate = _water.billDueDate;
                modelDetail.billNumber = _water.billNumber;
                modelDetail.check = _water.check;
                modelDetail.status = true;
                modelDetail.debtIdSet = _water.debtIdSet;
                _lstDebt.Add(modelDetail);
                model.inquiryGroupDebtBeanList = _lstDebt;
                _lstInfo.Add(model);
                #endregion
                rs.Output.Add(model);
                GlobalClass.LstInfo.Add(model);
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
        public void AddReceiveinadvance(decimal amount, ResultInquiryDebt result, string ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultInquiryDebt rs = new ResultInquiryDebt();
            try
            {
                rs.Output = new List<inquiryInfoBeanList>();
                if (result.Output != null)
                {
                    inquiryInfoBeanList inquiry = result.Output.Where(c => c.ca == ca).FirstOrDefault();
                    inquiryGroupDebtBeanList inquiryDetail = new inquiryGroupDebtBeanList();
                    string perVat;
                    if (Convert.ToInt16(inquiry.billVatCode) >= 10)
                        perVat = "1." + inquiry.billVatCode;
                    else
                        perVat = "1.0" + inquiry.billVatCode;
                    decimal vat = amount - (amount / Convert.ToDecimal(perVat));
                    inquiryDetail.debtIdSet = new Int64[1] { 1 };
                    inquiryDetail.debtId = "1";
                    inquiryDetail.amount = Convert.ToDecimal(amount) - vat;
                    inquiryDetail.percentVat = inquiry.billVatCode;
                    inquiryDetail.vat = Math.Round(vat, 2).ToString();
                    inquiryDetail.totalAmount = amount.ToString();
                    inquiryDetail.debtBalance = amount;
                    inquiryDetail.Payment = Convert.ToDecimal(amount);
                    inquiryDetail.debtType = "ADV";
                    inquiryDetail.custId = inquiry.custId;
                    inquiryDetail.ca = inquiry.ca;
                    inquiryDetail.ui = inquiry.ui;
                    inquiryDetail.status = true;
                    inquiryDetail.debtDesc = "เงินล่วงหน้า";
                    if (inquiry.inquiryGroupDebtBeanList == null)
                    {
                        inquiry.inquiryGroupDebtBeanList = new List<inquiryGroupDebtBeanList>();
                        inquiry.EleTotalAmount = Convert.ToDecimal(amount) - vat;
                        inquiry.EleTotalVat = Math.Round(vat, 2);
                        inquiry.TotalAmount = Convert.ToDecimal(amount);
                    }
                    else
                    {
                        inquiry.EleTotalAmount += Convert.ToDecimal(amount) - vat;
                        inquiry.EleTotalVat += vat;
                        inquiry.TotalAmount += Convert.ToDecimal(amount);
                    }
                    inquiry.inquiryGroupDebtBeanList.Add(inquiryDetail);
                }
                GlobalClass.LstInfo = result.Output;
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public string Substring(string date)
        {
            string _date = "";
            string day = date.Substring(0, 2);
            string month = date.Substring(2, 2);
            string year = date.Substring(4, 2);
            _date = day + "/" + month + "/" + ConvertDate() + year;
            return _date;
        }
        public string ConvertDate()
        {
            string date = "";
            DateTime dt = DateTime.Now;
            DateTime _date;
            if (dt.Year < 2500)
                _date = dt.AddYears(543);
            else
                _date = dt;
            var strDate = _date.Year;
            date = strDate.ToString().Substring(0, 2);
            return date;
        }
        public ResultPayment SelectDataRemittance(inquiryInfoBeanList cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultPayment rs = new ResultPayment();
            try
            {
                #region
                string strJSON = null;
                string result = JsonConvert.SerializeObject(cls);
                strJSON = ClassReadAPI.GetDataAPI("inquiryPayment", result);
                ResultPayment clsData = ClassReadAPI.ReadToObjectDisplayRemittance(strJSON);
                rs.Output = new List<listPayment>();
                List<listPayment> _list = new List<listPayment>();
                if (clsData.Output != null)
                {
                    foreach (listPayment item in clsData.Output)
                    {
                        listPayment models = new listPayment();
                        models.paymentDtlId = item.paymentDtlId;
                        models.paymentNo = item.paymentNo;
                        models.receiptNo = item.receiptNo;
                        models.paymentChanelDescTh = item.paymentChanelDescTh;
                        models.taxBranch = item.taxBranch;
                        models.distId = item.distId;
                        models.distDesc = item.distDesc;
                        models.paymentTypeDescTh = item.paymentTypeDescTh;
                        models.dueDate = item.dueDate;
                        models.paymentDate = item.paymentDate;
                        models.debtName = item.debtName;
                        models.itemDescTh = item.itemDescTh;
                        models.paymentDtlAmount = item.paymentDtlAmount;
                        models.paymentDtlPercentVat = item.paymentDtlPercentVat;
                        models.paymentDtlTotalAmount = item.paymentDtlTotalAmount;
                        models.createdby = item.createdby;
                        models.channelDTl = item.channelDTl;
                        rs.Output.Add(models);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ControlErrorLog = new ClassLogsService("ErrorLog", "");
                ControlErrorLog.WriteData("Controller :: ArrearsController | Function :: SelectDataRemittance()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
        public ResultPayInBalance SelectDataPayin(ClassPayInBalance cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultPayInBalance rs = new ResultPayInBalance();
            try
            {
                string strJSON = null;
                string result = JsonConvert.SerializeObject(cls);
                strJSON = ClassReadAPI.GetDataAPITest("displayPayInBalance", result);
                ResultPayInBalance clsData = ClassReadAPI.ReadToObjectPayInBalance(strJSON);
                rs.OutputPayinCheque = new List<listPayinCheque>();
                rs.OutputPayinCredit = new List<listPayinCreditCard>();
                rs.OutputPayinSummary = new List<payinSummary>();
                if (clsData.ResultMessage == "SUCCESS")
                {
                    if (clsData.OutputPayinSummary != null)
                    {
                        foreach (payinSummary item in clsData.OutputPayinSummary)
                        {
                            payinSummary models = new payinSummary();
                            models.createDt = item.createDt;
                            models.distId = item.distId;
                            models.cashierId = item.cashierId;
                            models.cashAmount = item.cashAmount;
                            models.chequeAmount = item.chequeAmount;
                            models.cardAmount = item.cardAmount;
                            models.transferCash = item.transferCash;
                            models.transferCheque = item.transferCheque;
                            models.transferCard = item.transferCard;
                            models.outStandingCash = item.outStandingCash;
                            models.outStandingCheque = item.outStandingCheque;
                            models.outStandingCard = item.outStandingCard;
                            rs.OutputPayinSummary.Add(models);
                        }
                    }
                    if (clsData.OutputPayinCheque.Count != 0)
                    {
                        foreach (var item in clsData.OutputPayinCheque)
                        {
                            listPayinCheque models = new listPayinCheque();
                            models.chequeNo = item.chequeNo;
                            models.chequeOwner = item.chequeOwner;
                            models.transactionId = item.transactionId;
                            models.chequeAmount = item.chequeAmount;
                            models.bankName = item.bankName;
                            rs.OutputPayinCheque.Add(models);
                        }
                    }
                    if (clsData.OutputPayinCredit.Count != 0)
                    {
                        foreach (var item in clsData.OutputPayinCredit)
                        {
                            listPayinCreditCard models = new listPayinCreditCard();
                            models.cardNo = item.cardNo;
                            models.cardOwner = item.cardOwner;
                            models.transactionId = item.transactionId;
                            models.cardAmount = item.cardAmount;
                            rs.OutputPayinCredit.Add(models);
                        }
                    }
                }
                else
                {
                    LogEventInquiry.WriteDataEvent("Fail ==> Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                    rs.ResultMessage = clsData.ResultMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
    }
}
