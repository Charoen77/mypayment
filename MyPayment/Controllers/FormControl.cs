﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPayment.Controllers
{
   public class FormControl
    {
        List<Form> forms = new List<Form>();

        public void Add(Form form)
        {
            Form existingForm = Find(form.Name);
            if (existingForm == null)
                forms.Add(form);
        }

        public void Remove(Form form)
        {
            Form existingForm = Find(form.Name);
            if (existingForm != null)
                forms.Remove(existingForm);
        }

        public Form Find(string name)
        {
            return (from c in forms
                    where c.Name == name
                    select c).FirstOrDefault();
        }
    }
}
