﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassCustomerDetail
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        //public string MeasurementId { get; set; }
        //public string Location { get; set; }
        //public string Account { get; set; }
        //public string AmountCount { get; set; }
        //public string NotificationCount { get; set; }
        //public string Detail { get; set; }
        public string CA { get; set; }
        public string UI { get; set; }
        public string Address { get; set; }
        public string message { get; set; }
    }
}
