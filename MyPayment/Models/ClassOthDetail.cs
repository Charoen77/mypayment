﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassOthDetail
    {
        public string InvNo { get; set; }
        public string UI { get; set; }
        public string CustName { get; set; }
        public string Address { get; set; }
        public string BusinessType { get; set; }
        public string Department { get; set; }
        public string profit { get; set; }
    }
}
