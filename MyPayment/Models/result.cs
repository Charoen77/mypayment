﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class result
    {
        public string payeeFlag { get; set; }
        public string responseMessage { get; set; }
        public string responseCode { get; set; }
        public string mwaTransRef { get; set; }
        public string taxId { get; set; }
        public string invoiceType { get; set; }
        public string mwaThAddress1 { get; set; }
        public string paidAddress { get; set; }
        public string mwaThAddress2 { get; set; }
        public string customerName { get; set; }
        public int totalInvoice { get; set; }
        public string customerCode { get; set; }
        public string taxBranch { get; set; }
        public string branchCode { get; set; }
        public string mwaTaxId { get; set; }
        public string agentTransRef { get; set; }
        public double totalNetAmount { get; set; }
        public string vatType { get; set; }
        public string billDate { get; set; }
        public string billDueDate { get; set; }
        public string consumption { get; set; }
        public string billNumber { get; set; }
        public string mwaThName { get; set; }
        public string mwaTaxBranch { get; set; }
        public string customerAddress { get; set; }
        public string paidName { get; set; }
        public string receiveType { get; set; }
        public double totalGrossAmount { get; set; }
        public double totalVatAmount { get; set; }
        public string barcode { get; set; }
    }
}
