﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class listExpenseItem
    {
        public Int64 itemId { get; set; }
        public string itemCode { get; set; }
        public string itemDescTh { get; set; }
        public string itemDescEn { get; set; }
        public Int64 itemAccountCode { get; set; }
        public string vatCode { get; set; }
        public double percentVat { get; set; }
    }
}
