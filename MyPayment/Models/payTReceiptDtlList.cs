﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class responseReceiptDtlBeanList
    {
        public string receiptDtlNo { get; set; }
        public string receiptDtlName { get; set; }
        public decimal receiptDtlAmount { get; set; }
        public decimal? receiptDtlVat { get; set; }
        public decimal receiptDtlTotalAmount { get; set; }
        public string lastBillingDate { get; set; }
        public int? receiptDtlUnit { get; set; }
        public decimal intAmount { get; set; }
        public int? intDay { get; set; }
        public string ft { get; set; }
    }
}
