﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class listSummaryCostTDebt : ResultList
    {
        public string ca { get; set; }
        public string eleInvCount { get; set; }
        public string eleTotalAmount { get; set; }
        public string otherTotalAmount { get; set; }
        public string eleTotalVat { get; set; }
        public string otherTotalVat { get; set; }
    }
}
