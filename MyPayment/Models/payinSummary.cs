﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class payinSummary
    {
        public string createDt { get; set; }
        public int distId { get; set; }
        public Int64 cashierId { get; set; }
        public double cashAmount { get; set; }
        public double chequeAmount { get; set; }
        public double cardAmount { get; set; }
        public string transferCash { get; set; }
        public string transferCheque { get; set; }
        public string transferCard { get; set; }
        public double outStandingCash { get; set; }
        public double outStandingCheque { get; set; }
        public double outStandingCard { get; set; }       
    }
}
