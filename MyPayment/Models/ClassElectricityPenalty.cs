﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassElectricityPenalty
    {
        public string debtId { get; set; }
        public int userId { get; set; }
        public int approveEmpId { get; set; }
        public string ca { get; set; }
        public string authorizedCancelBy { get; set; }
        public bool StatusDefaultpenalty { get; set; }
    }
}
