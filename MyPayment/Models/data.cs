﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class data
    {
        public int? method { get; set; }
        public int? distId { get; set; }
        public Int64? cashierId { get; set; }
        public Int64? refId { get; set; }
        public string refNo { get; set; }
        public int? closingId { get; set; }
    }
}
