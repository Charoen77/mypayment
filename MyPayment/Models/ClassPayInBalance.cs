﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassPayInBalance
    {
        public string payDate { get; set; }
        public int distId { get; set; }
        public int cashierId { get; set; }
        public int cashierNo { get; set; }
    }
}
