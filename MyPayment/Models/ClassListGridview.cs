﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassListGridview
    {
        public string ChNo { get; set; }
        public string ChAmount { get; set; }
        public double Balance { get; set; }
    }
}
