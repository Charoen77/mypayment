﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClassItemCode
    {
        public List<listExpenseItem> listExpenseItem { get; set; }
        public List<listProfitCenter> listProfitCenter { get; set; }
        public List<costUnit> costUnit { get; set; }
    }
}
