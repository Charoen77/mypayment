﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class listPayment
    {
        public string paymentDtlId { get; set; }
        public string paymentNo { get; set; }
        public string receiptNo { get; set; }
        public string paymentChanelDescTh { get; set; }
        public string taxBranch { get; set; } 
        public string distId { get; set; }
        public string distDesc { get; set; }
        public string paymentTypeDescTh { get; set; }
        public string dueDate { get; set; }
        public string paymentDate { get; set; }
        public string debtName { get; set; }
        public string itemDescTh { get; set; }
        public string paymentDtlAmount { get; set; }
        public string paymentDtlPercentVat { get; set; }
        public string paymentDtlTotalAmount { get; set; }
        public string createdby { get; set; }
        public string channelDTl { get; set; }
    }
}
