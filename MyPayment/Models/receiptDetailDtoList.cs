﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class receiptDetailDtoList
    {
        public string receiptDtlNo { get; set; }
        public string receiptDtlName { get; set; }
        public double receiptDtlAmount { get; set; }
        public double receiptDtlVat { get; set; }
        public decimal receiptDtlTotalAmount { get; set; }
        public string lastBillingDate { get; set; }
        public string receiptDtlUnit { get; set; }
        public string intAmount { get; set; }
        public int? intDay { get; set; }
        public string ft { get; set; }
        public Int64[][] debtIdSetList { get; set; }
    }
}
