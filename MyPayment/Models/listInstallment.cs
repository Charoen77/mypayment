﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class listInstallment
    {
        public string debtInsHdrId { get; set; }
        public List<installmentDtlList> installmentDtlList { get; set; }
    }
}
