﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassOTH
    {
        public string processSet { get; set; }
        public string distId { get; set; }
        public string paymentDate { get; set; }
        public string invNo { get; set; }
        public string custName { get; set; }
        public string custAddBill { get; set; }
        public int? paMethodIdA { get; set; }
        public double? paMethodAmA { get; set; }
        public int? paMethodIdB { get; set; }
        public double? paMethodAmB { get; set; }
        public int? paMethodIdC { get; set; }
        public double? paMethodAmC { get; set; }
        public string amountIncVat { get; set; }
        public string bankBranch { get; set; }
        public string chequeNo { get; set; }
        public string chequeOwner { get; set; }
        public string chequeDate { get; set; }
        public string cardNo { get; set; }
        public string cardOwner { get; set; }
        public string cardStartDate { get; set; }
        public string cardEndDate { get; set; }
        public string cardApproveCode { get; set; }
        public string cashierId { get; set; }
        public string cashierNo { get; set; }
        public List<dataList> dataList { get; set; }
        public List<chequeList> chequeList { get; set; }
    }
}
