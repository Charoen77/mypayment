﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class chequeList
    {
        public string bankBranch { get; set; }
        public string chequeNo { get; set; }
        public string chequeOwner { get; set; }
        public string chequeDate { get; set; }
        public double? chequeAmount { get; set; }
    }
}
