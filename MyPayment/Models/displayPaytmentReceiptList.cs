﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class DisplayPaytmentReceiptList
    {
        public int? DistId { get; set; } 
        public string DistName { get; set; }
        public Int64? PaymentId { get; set; }
        public string ReceiptNo { get; set; }
        public string ReceiptDate { get; set; }
        public string DueDate { get; set; }
        public string ReceiptPayer { get; set; }
        public Int64? ReceiptCustId { get; set; }
        public string ReceiptCustName { get; set; }
        public int ReceiptCustTaxid { get; set; }
        public Int64? ReceiptCustBranch { get; set; }
        public string ReceiptReqAddress { get; set; }
        public Int64? ReceiptCa { get; set; }
        public Int64? ReceiptUi { get; set; }
        public Int64? DebtId { get; set; }
        public string DebtType { get; set; }
        public string DebtName { get; set; }
        public int? ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescTh { get; set; }
        public double? PaidExcAmount { get; set; }
        public int? PaidVatPercent { get; set; }
        public double? PaidVatAmount { get; set; }
        public double? PaymentIntAmount { get; set; }
        public int? PaymentIntDay { get; set; }
        public double? TtlPaidIncAmount { get; set; }
        public Int64? ReqId { get; set; }
        public string RecStatus { get; set; }
    }
}
