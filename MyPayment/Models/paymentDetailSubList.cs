﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class paymentDetailSubList
    {
        public int paymentMethodId { get; set; }
        public decimal totalAmount { get; set; }
        public string chequeNo { get; set; }
        public string cardNo { get; set; }
    }
}
