﻿using MyPayment.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;

namespace MyPayment.Models
{
    class ClassReadAPI
    {
        public enum StatusLoadData
        {
            Loging = 1,
            LoginAddmin = 2,
            changePassword = 3,
            LoadData = 4,
            Logout = 5
        }
        public static ClassUser ReadToObject(string json)
        {
            ClassUser deserializedMember = new ClassUser();
            deserializedMember = new JavaScriptSerializer().Deserialize<ClassUser>(json);

            //deserializedMember = JsonConvert.DeserializeObject<ClassUser>(json);
            //MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            //DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedMember.GetType());
            //deserializedMember = ser.ReadObject(ms) as ClassUser;
            //ms.Close();
            return deserializedMember;
        }
        public static DataTable GetDataTableFromJsonString(string json)
        {
            var jsonLinq = JObject.Parse(json);
            var srcArray = jsonLinq.Descendants().Where(d => d is JArray).First();
            var trgArray = new JArray();
            foreach (JObject row in srcArray.Children<JObject>())
            {
                var cleanRow = new JObject();
                foreach (JProperty column in row.Properties())
                {
                    if (column.Value is JValue)
                    {
                        cleanRow.Add(column.Name, column.Value);
                    }
                }
                trgArray.Add(cleanRow);
            }

            return JsonConvert.DeserializeObject<DataTable>(trgArray.ToString());
        }
        public static IList<inquiryGroupDebtBeanList> ReadToObjectlistCostTDebt(string json)
        {
            IList<inquiryGroupDebtBeanList> searchResults = new List<inquiryGroupDebtBeanList>();
            ClassUser deserializedMember = new ClassUser();
            deserializedMember = new JavaScriptSerializer().Deserialize<ClassUser>(json);
            if (deserializedMember.result_code == "SUCCESS")
            {
                JObject objSearch = JObject.Parse(json);
                IList<JToken> results = objSearch["result"]["listCostTDebt"].Children().ToList();
                foreach (JToken result in results)
                {
                    inquiryGroupDebtBeanList searchResult = JsonConvert.DeserializeObject<inquiryGroupDebtBeanList>(result.ToString());
                    searchResults.Add(searchResult);
                }
                return searchResults;
            }
            else
            {
                inquiryGroupDebtBeanList results = new inquiryGroupDebtBeanList();
                results.ResultMessage = deserializedMember.result_message;
                searchResults.Add(results);
                return searchResults;
            }
        }
        public static ResultInquiryDebt ReadToObjectInquiry(string json)
        {
            ResultInquiryDebt searchResults = new ResultInquiryDebt();
            ClassInquiryDebt Inquiry = JsonConvert.DeserializeObject<ClassInquiryDebt>(json);
            if (Inquiry.result_code == "SUCCESS")
            {
                if (Inquiry.inquiryBean.inquiryInfoBeanList != null)
                    searchResults.OutputInquiryDebt = Inquiry;
            }
            else
            {
                if (Inquiry.result_code != "EC005")
                    searchResults.OutputInquiryDebt = Inquiry;
                searchResults.ResultMessage = Inquiry.result_message;
            }
            return searchResults;
        }
        public static ResultPayInBalance ReadToObjectPayInBalance(string json)
        {
            ResultPayInBalance searchResults = new ResultPayInBalance();
            ClassUser deserializedMember = new ClassUser();
            deserializedMember = new JavaScriptSerializer().Deserialize<ClassUser>(json);
            if (deserializedMember.result_code == "SUCCESS")
            {
                searchResults.OutputPayinSummary = new List<payinSummary>();
                JObject objSearch = JObject.Parse(json);
                IList<JToken> results = objSearch["result"]["payinSummary"].Children().ToList();
                foreach (JToken result in results)
                {
                    payinSummary searchResult = JsonConvert.DeserializeObject<payinSummary>(result.ToString());
                    searchResults.OutputPayinSummary.Add(searchResult);
                }
                searchResults.OutputPayinCheque = new List<listPayinCheque>();
                IList<JToken> resultsCheque = objSearch["result"]["listPayinCheque"].Children().ToList();
                foreach (JToken result in resultsCheque)
                {
                    listPayinCheque searchResult = JsonConvert.DeserializeObject<listPayinCheque>(result.ToString());
                    searchResults.OutputPayinCheque.Add(searchResult);
                }
                searchResults.OutputPayinCredit = new List<listPayinCreditCard>();
                IList<JToken> resultsCredit = objSearch["result"]["listPayinCreditCard"].Children().ToList();
                foreach (JToken result in resultsCredit)
                {
                    listPayinCreditCard searchResult = JsonConvert.DeserializeObject<listPayinCreditCard>(result.ToString());
                    searchResults.OutputPayinCredit.Add(searchResult);
                }
                searchResults.ResultMessage = deserializedMember.result_code;
            }
            else
                searchResults.ResultMessage = deserializedMember.result_code;
            return searchResults;
        }
        public static string GetDataAPI(string url, string result)
        {
            string json = "";
            string http = GlobalClass.Url + url;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(http);
            request.ContentType = "application/json";
            request.Method = "POST";
            request.ContentLength = 0;
            request.AllowAutoRedirect = false;
            request.KeepAlive = false;
            request.Timeout = 3000000;
            request.ReadWriteTimeout = 3000000;
            request.Accept = "application/json";
            request.ProtocolVersion = HttpVersion.Version11;
            request.Headers.Add("Accept-Language", "de_DE");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            byte[] byteArray = Encoding.UTF8.GetBytes(result);
            request.ContentLength = byteArray.Length;
            using (var writer = request.GetRequestStream())
            {
                writer.Write(byteArray, 0, byteArray.Length);
                writer.Flush();
                writer.Close();
            }
            using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse())
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    json = streamReader.ReadToEnd();
                    streamReader.Close();
                    httpResponse.Close();
                    return json;
                }
            }            
        }
        public static string RunWebRequest(string url, string json)
        {
            string http = GlobalClass.Url + url;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(http);
            request.ContentType = "application/json";
            request.Method = "POST";
            request.AllowAutoRedirect = false;
            request.KeepAlive = false;
            request.Timeout = 300000;
            request.ReadWriteTimeout = 300000;
            request.Accept = "application/json";
            request.ProtocolVersion = HttpVersion.Version11;
            request.Headers.Add("Accept-Language", "de_DE");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            byte[] bytes = Encoding.UTF8.GetBytes(json);
            request.ContentLength = bytes.Length;
            using (var writer = request.GetRequestStream())
            {
                writer.Write(bytes, 0, bytes.Length);
                writer.Flush();
                writer.Close();
            }
            var httpResponse = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var jsonReturn = streamReader.ReadToEnd();
                return jsonReturn;
            }
        }
        public static string GetDataAPITestIn(string url, string result)
        {
            string http = "http://scssit.mea.or.th/MEA-SCSWebService/" + url;
            WebRequest request = WebRequest.Create(http);
            request.Method = "POST";
            request.ContentLength = 0;
            byte[] byteArray = Encoding.UTF8.GetBytes(result);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            //request.Timeout = 50000000;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string json = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            return json;
        }
        public static string GetDataAPITest(string url, string result)
        {
            string http = "http://10.76.52.111:9080/MEA-SCSWebService/" + url;
            WebRequest request = WebRequest.Create(http);
            request.Method = "POST";
            request.ContentLength = 0;
            byte[] byteArray = Encoding.UTF8.GetBytes(result);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string json = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            return json;
        }
        public static string GetDataWater(string cusId)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            //https://10.211.70.35/mwaquery.php?agent_trans_ref=54321&agent_code=A004&customer_code=55361273&collect_type=17&collect_type_form=00
            string url = ConfigurationManager.AppSettings["URLWater"].ToString() + "&agent_code=A004&customer_code=" + cusId + "&collect_type=17&collect_type_form=00";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentLength = 0;
            request.Method = "GET";
            request.ContentType = "text/plain";
            WebResponse webResponse = (HttpWebResponse)request.GetResponse();
            Stream webStream = webResponse.GetResponseStream();
            StreamReader responseReader = new StreamReader(webStream);
            string response = responseReader.ReadToEnd();
            responseReader.Close();
            return response;
        }
        public static string GetBank(string code)
        {
            string url = GlobalClass.Url + "getBankBranch?bankBranch=" + code;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/plain";
            WebResponse webResponse = (HttpWebResponse)request.GetResponse();
            Stream webStream = webResponse.GetResponseStream();
            StreamReader responseReader = new StreamReader(webStream);
            string response = responseReader.ReadToEnd();
            responseReader.Close();
            return response;
        }
        public static string GetItemCode(string url)
        {
            string http = GlobalClass.Url + url;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(http);
            request.Method = "GET";
            request.ContentType = "text/plain";
            WebResponse webResponse = (HttpWebResponse)request.GetResponse();
            Stream webStream = webResponse.GetResponseStream();
            StreamReader responseReader = new StreamReader(webStream);
            string response = responseReader.ReadToEnd();
            responseReader.Close();
            return response;
        }
        public static ResultDisplayPaytment ReadToObjectDisplayPaytment(string json)
        {
            ResultDisplayPaytment searchResults = new ResultDisplayPaytment();
            ResultDisplayPaytment Display = new ResultDisplayPaytment();
            Display = new JavaScriptSerializer().Deserialize<ResultDisplayPaytment>(json);
            searchResults.Output = new List<DisplayPaytmentReceiptList>();
            JObject objSearch = JObject.Parse(json);
            IList<JToken> results = objSearch["displayPaytmentReceiptList"].Children().ToList();
            foreach (JToken result in results)
            {
                DisplayPaytmentReceiptList searchResult = JsonConvert.DeserializeObject<DisplayPaytmentReceiptList>(result.ToString());
                searchResults.Output.Add(searchResult);
            }
            return searchResults;
        }
        public static ResultPayment ReadToObjectDisplayRemittance(string json)
        {
            ResultPayment searchResults = new ResultPayment();
            ResultPayment Display = new ResultPayment();
            Display = new JavaScriptSerializer().Deserialize<ResultPayment>(json);
            if (Display.result_code == "SUCCESS")
            {
                searchResults.Output = new List<listPayment>();
                JObject objSearch = JObject.Parse(json);
                IList<JToken> results = objSearch["listPayment"].Children().ToList();
                foreach (JToken result in results)
                {
                    listPayment searchResult = JsonConvert.DeserializeObject<listPayment>(result.ToString());
                    searchResults.Output.Add(searchResult);
                }
            }
            return searchResults;
        }
    }
}
