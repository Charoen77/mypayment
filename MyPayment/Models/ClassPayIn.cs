﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassPayIn
    {
        public string processSet { get; set; }
        public int? distId { get; set; }
        public Int64? cashierId { get; set; }
        public int? cashierNo { get; set; }
        public string clsDate { get; set; }
        public int note01 { get; set; }
        public int note02 { get; set; }
        public int note03 { get; set; }
        public int note04 { get; set; }
        public int note05 { get; set; }
        public int note06 { get; set; }
        public int note07 { get; set; }
        public int note08 { get; set; }
        public int note09 { get; set; }
        public int note10 { get; set; }
        public int note11 { get; set; }
        public double note12 { get; set; }
        public int? chequeQty { get; set; }
        public double chequeAmount { get; set; }
        public int? ccardQty { get; set; }
        public double ccardAmount { get; set; }
        public List<data> data { get; set; }
    }
}
