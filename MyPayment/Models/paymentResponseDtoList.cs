﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class paymentResponseDtoList
    {
        public string paymentId { get; set; }
        public List<payTReceiptList> payTReceiptList { get; set; }
    }
}
