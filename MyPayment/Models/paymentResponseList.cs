﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class paymentResponseList
    {
        public string paymentId { get; set; }
        public string debtType { get; set; }
        public Int64 receiptId { get; set; }
        public string receiptNo { get; set; }
        public int[] debtIds { get; set; }
        public string[] guaNo { get; set; }
        public string empIdStr { get; set; }
        public string receiptCa { get; set; }
        public string paymentNo { get; set; }
    }
}
