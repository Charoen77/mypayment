﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassRemittance: ResultList
    {
        public string No { get; set; }
        public double AmountCheq { get; set; }
        public double AmountCard { get; set; }
        public double Amount { get; set; }
    }
}
