﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class listPayinCheque
    {
        public string chequeNo { get; set; }
        public string chequeOwner { get; set; }
        public Int64 transactionId { get; set; }
        public double chequeAmount { get; set; }     
        public string bankName { get; set; }
    }
}
