﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class listPayinCreditCard
    {
        public string cardNo { get; set; }
        public string cardOwner { get; set; }
        public Int64 transactionId { get; set; }
        public double cardAmount { get; set; }         
    }
}
