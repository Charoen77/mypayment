﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ItemCodeData
    {
        public bool Selected { get; set; }
        public string TaxId { get; set; }

        public string BillNumber { get; set; }

        public string LastDayMemo { get; set; }
        public string TaxInvoice { get; set; }
        public string ContractAccount { get; set; }
        public string InvoiceNumber { get; set; }
        public string DueDate { get; set; }
        public string PowerUnit { get; set; }
        public string DebtCode { get; set; } // รหัสหนี้ค้างชำระ
        public string DayFine { get; set; }
        public string FineFlakedOut { get; set; }
        public string AmountExVat { get; set; }
        public string Vat { get; set; }
        public string AmountIncVat { get; set; }
        public string StopElectricityAmount { get; set; }
        public string ServiceType { get; set; }

        public string TotalAmountExVat { get; set; }
        public string TotalVat { get; set; }
        public string TotalAmountIncVat { get; set; }
        public string TotalFine { get; set; }
        public string TotalAll { get; set; }

        public string TEST { get; set; }

    }

    //public class ItemShortTaxInvoice
    //{
    //    public string ContractAccount { get; set; }
    //    public string LastDayMemo { get; set; }
    //    public string TaxInvoice { get; set; }
    //    public string PowerUnit { get; set; }
    //    public string AmountExVat { get; set; }
    //    public string Vat { get; set; }
    //    public string AmountIncVat { get; set; }
    //    public string DayFine { get; set; }
    //    public string FineFlakedOut { get; set; }
    //    public string FT { get; set; }
    //}

    public class ItemOfflineSumBanknote
    {
        public string Cash { get; set; }
        public string Cheque { get; set; }
    }


    public class ItemOfflineModel
    {
        public string RowNo { get; set; }
        public string PaymentDate { get; set; }
        public string Time { get; set; }
        public string Branch { get; set; }
        public string CashierNo { get; set; }
        public string UserId { get; set; }
        public string ContractAccount { get; set; }
        public string InvoiceNumber { get; set; }
        public string TaxInvoice { get; set; }
        public string Amount { get; set; }

        public string ServiceType { get; set; }

        public string Cash { get; set; }
        public string CashChange { get; set; }


        public string GainLoss { get; set; } 
        public string ContactNumber { get; set; }

        /******************************************************/

      

        public string BillNo { get; set; }

        public string AmountIncVat { get; set; }

        public string Interest { get; set; }

        public string DayOfInterest { get; set; }

        public string DB_OPS { get; set; }

        public string Transact { get; set; }


    }

    public class ItemOfflineLogToServiceModel
    {
        public string PaymentDate { get; set; }
        public string ServiceType { get; set; }


        public string ContractAccount { get; set; }

        public string InvoiceNumber { get; set; }

        public string BillNo { get; set; }

        public string AmountIncVat { get; set; }

        public string Interest { get; set; }

        public string DayOfInterest { get; set; }

        public string DB_OPS { get; set; }

        public string Transact { get; set; }

    }

    public class ItemSummaryPaymentModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
    public class ItemtRevertTaxInvoiceModel
    {
        public string UserId { get; set; }
        public string ContractAccount { get; set; }
        public string InvoiceNumber { get; set; }
        public string TaxInvoice { get; set; }
        public string Amount { get; set; }
        public string Remark { get; set; }
    }

 

    public class ItemtSummaryChequeModel
    {
        public string UserId { get; set; }
       
        public string ChequeNumber { get; set; }

        public string BankId { get; set; }

        public string BankName { get; set; }
        public string ChequeDate { get; set; }

        public string Cheque { get; set; }

        public string ChequeChange { get; set; }

        public string ChequeName { get; set; }
        public string CashierCheque { get; set; }

        //public string InvoiceNumber { get; set; }
        //public string ContractAccount { get; set; }
        //public string TaxInvoice { get; set; }


    }

    public class ItemSummaryPaymentModel_Display
    {
        public string UserId { get; set; }
        public string PaymentDate { get; set; }
        public string Branch { get; set; }
        public string CashierNo { get; set; }

        
        public string SumCheque { get; set; }

        public string SumChequeChange { get; set; }
        public string SumCash { get; set; }

        public string SumGainLoss { get; set; }
        public string SumCredit { get; set; }
        public string SumTotal { get; set; }


    }

    public class SummaryPaymentObj
    {
        public List< ItemSummaryPaymentModel_Display> Display { get; set; }

        public List<ItemSummaryPaymentModel> Report { get; set; }

    }

}
