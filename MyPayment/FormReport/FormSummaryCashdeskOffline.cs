﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Master;
using MyPayment.Models;
using System.Globalization;
using System.Threading;
using Newtonsoft.Json.Linq;


namespace MyPayment.FormReport
{
    public partial class FormSummaryCashdeskOffline : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        public FormSummaryCashdeskOffline()
        {
            InitializeComponent();
        }

        private void btnGetAllCashdeskOffline_Click(object sender, EventArgs e)
        {
            try
            {

                //  var client = new RestClient("http://scs4uat.mea.or.th/MEA-SCSWebService/");

                //  //  var client = new RestClient("http://localhost:65437/api");
                //  var request = new RestRequest("/paymentInquiryDebt/", Method.POST);
                //  request.Timeout = 120000;
                // // request.AddParameter("ca", 10442731);
                ////  var ca = { "ca" : 10442731};
                // JObject jObj = new JObject();
                //  jObj.Add("ca", 10442731);
                //  request.AddJsonBody(jObj);
                //  //request.AddParameter("EMP_CODE", EmpCode);
                //  //request.AddParameter("TO_EMP_CODE", EmpCode);
                //  //request.AddParameter("TRANS_NO", RequestNo);
                //  //request.AddParameter("CUSTOMER_CODE", CustomerCode);
                //  //request.AddParameter("TYPE", "DOCREQUEST");
                //  //request.AddParameter("LOCATION_NAME", Location);





                //  IRestResponse response = client.Execute(request);
                //  var content = response.Content;



                //  return;
                //string strDate =  dpSelect.Value.ToString("yyyyMMdd");
                //var obj = OFC.ReadOffLineFileFromCSVByIP(strDate);
                //GridDetail.DataSource = obj;
                //GlobalClass.LstOfflineModel = obj;


                ///*****************************************************************/
                //Random random = new Random();
                //string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                //string random_decimal = "0123456789";
                //string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

                //string InvoiceNumber = new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray());
                ///*****************************************************************/

                //ItemOfflineModel itemModel = new ItemOfflineModel();
                //itemModel.UserId = @"00001";
                //itemModel.InvoiceNumber = new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray());
                //itemModel.TaxInvoice = new string(Enumerable.Repeat(random_decimal, 10).Select(s => s[random.Next(s.Length)]).ToArray());
                //itemModel.PaymentDate = DateTime.Now.ToString("dd/MM/yyyy");
                //itemModel.Time = DateTime.Now.ToString("HH:mm");
                //itemModel.Branch = "013";
                //itemModel.CashierNo = "1";
                //itemModel.ServiceType = "A";
                //itemModel.ContractAccount = new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray());
                //itemModel.Amount = string.Format("{0:n}", decimal.Parse(new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray())));
                //itemModel.Cash = string.Format("{0:n}", decimal.Parse(new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray())));
                //itemModel.CashChange = string.Format("{0:n}", decimal.Parse(new string(Enumerable.Repeat(random_decimal, 3).Select(s => s[random.Next(s.Length)]).ToArray())));
                //itemModel.Cheque = string.Format("{0:n}", decimal.Parse(new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray())));
                //itemModel.ChequeChange = string.Format("{0:n}", decimal.Parse(new string(Enumerable.Repeat(random_decimal, 3).Select(s => s[random.Next(s.Length)]).ToArray())));
                //itemModel.ChequeNumber = new string(Enumerable.Repeat(random_decimal, 10).Select(s => s[random.Next(s.Length)]).ToArray());
                //itemModel.ChequeDate = DateTime.Now.ToString("dd/MM/yyyy");
                //itemModel.ChequeName = new string(Enumerable.Repeat(random_chars_only, 20).Select(s => s[random.Next(s.Length)]).ToArray());
                //itemModel.Credit = string.Format("{0:n}", decimal.Parse(new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray())));
                //itemModel.GainLoss = new string(Enumerable.Repeat(random_decimal, 1).Select(s => s[random.Next(s.Length)]).ToArray());
                //itemModel.ContactNumber = new string(Enumerable.Repeat(random_decimal, 5).Select(s => s[random.Next(s.Length)]).ToArray());


                //  OFC.WriteOffLineToCSV(itemModel);


                /*****************************************************************/


                int Year = dpSelect.Value.Year;
                if (Year > 2500)
                {
                    Year = Year - 543;
                }
                string strDate = Year.ToString() + dpSelect.Value.ToString("MMdd");
                List<ItemOfflineModel> obj = OFC.GetSummaryCashdeskOfflineReport(strDate);

                GridDetail.AutoGenerateColumns = false;
                GridDetail.DataSource = obj;
                GlobalClass.LstOfflineModel = obj;


                decimal TotalAmount = obj.Sum(m => decimal.Parse(m.Amount));
                decimal TotalContractAccount = obj.GroupBy(m => m.ContractAccount).Count();
                decimal TotalTaxInvoice = obj.GroupBy(m => m.TaxInvoice).Count();
               

                lblTotalAmount.Text = string.Format("{0:n}", TotalAmount);
                lblTotalContractAccount.Text = string.Format("{0:n0}", TotalContractAccount);
                lblTotalTaxInvoice.Text = string.Format("{0:n0}", TotalTaxInvoice);

                GlobalClass.Branch = "013";
                GlobalClass.CashierNo = "1";
                GlobalClass.TotalAmount = string.Format("{0:n}", TotalAmount);
                GlobalClass.TotalContractAccount = string.Format("{0:n0}", TotalContractAccount);
                GlobalClass.TotalTaxInvoice = string.Format("{0:n0}", TotalTaxInvoice);
            }
            catch (Exception ex)
            {

            }
         

        }

        private void btnPopUpSummaryCashdeskOffline_Click(object sender, EventArgs e)
        {

            GlobalClass.ReportName = "rptSummaryCashdeskOffline.rpt";
            GlobalClass.ReportOfflineType = "ALLOFFLINE";

            //GlobalClass.ReportName = "rptTaxInvoiceOffline_Test.rpt";
            //GlobalClass.ReportOfflineType = "ALLOFFLINE";

            FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();
            FVR.ShowDialog();
        }

        private void FormSummaryCashdeskOffline_Load(object sender, EventArgs e)
        {
            //CultureInfo ThaiCultureInfo = CultureInfo.CreateSpecificCulture("th");
            //dpSelect.Format = DateTimePickerFormat.Custom;
            //string[] formats = dpSelect.Value.GetDateTimeFormats(ThaiCultureInfo);
            //dpSelect.CustomFormat = formats[8];

            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("th");



            label2.Visible = false;
            label3.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            /*************************************/

            GridDetail.AutoGenerateColumns = false;

            /*************************************************/


            CultureInfo ci = new CultureInfo("th-TH");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;


            GridDetail.ColumnHeadersDefaultCellStyle.Font = new Font("TH Sarabun New", 14.25F, FontStyle.Bold);

            lblTotalAmount.Text = "";
            lblTotalContractAccount.Text = "";
            lblTotalTaxInvoice.Text = "";
        }
    }
}
