﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Master;
using MyPayment.Models;

namespace MyPayment.FormReport
{
    public partial class FormRevertReport : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        public FormRevertReport()
        {
            InitializeComponent();
        }

        private void btnCallDataReport_Click(object sender, EventArgs e)
        {
            /*****************************************************************/
            Random random = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string random_decimal = "0123456789";
            string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            ItemtRevertTaxInvoiceModel itemModel = new ItemtRevertTaxInvoiceModel();

            string ContractAccount = new string(Enumerable.Repeat(random_decimal, 10).Select(s => s[random.Next(s.Length)]).ToArray());
            string InvoiceNumber = new string(Enumerable.Repeat(random_decimal, 10).Select(s => s[random.Next(s.Length)]).ToArray());
            string TaxInvoice = new string(Enumerable.Repeat(random_decimal, 10).Select(s => s[random.Next(s.Length)]).ToArray());
            string Amount = new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray());
            string Remark = new string(Enumerable.Repeat(random_chars_only, 40).Select(s => s[random.Next(s.Length)]).ToArray());


            itemModel.UserId = "00001";
            itemModel.ContractAccount = ContractAccount;
            itemModel.InvoiceNumber = InvoiceNumber;
            itemModel.TaxInvoice = TaxInvoice;
            itemModel.Amount = string.Format("{0:n}", decimal.Parse(Amount)); 
            itemModel.Remark = Remark;

            OFC.WriteCancelOffLineToCSV(itemModel);

            int Year = dpSelect.Value.Year;
            if (Year > 2500)
            {
                Year = Year - 543;
            }
            string strDate = Year.ToString() + dpSelect.Value.ToString("MMdd");
            var obj = OFC.GetRevertReport(strDate);

            GridDetail.DataSource = obj;
            GlobalClass.LstRevertTaxInvoiceModel = obj;
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {

            GlobalClass.ReportName = "rptRevertReport.rpt";
            GlobalClass.ReportOfflineType = "CANCEL";


            FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();
            //formm receive = new FormReceiveMoney();
            FVR.ShowDialog();
        }

        private void FormRevertReport_Load(object sender, EventArgs e)
        {
            GridDetail.ColumnHeadersDefaultCellStyle.Font = new Font("TH Sarabun New", 14.25F, FontStyle.Bold);
        }
    }
}
