﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Master;
using MyPayment.Models;


namespace MyPayment.FormReport
{
    public partial class FormChequeReport : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        public FormChequeReport()
        {
            InitializeComponent();
        }

        private void btnCallDataReport_Click(object sender, EventArgs e)
        {




            /*****************************************************************/
            Random random = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string random_decimal = "0123456789";
            string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";


            string TaxInvoice = new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray());
            string ChequeNumber = new string(Enumerable.Repeat(random_decimal, 10).Select(s => s[random.Next(s.Length)]).ToArray());
            string BankId = new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray());
            string BankName = new string(Enumerable.Repeat(random_chars_only, 20).Select(s => s[random.Next(s.Length)]).ToArray());
            string ChequeDate = DateTime.Now.ToString("dd/MM/yyyy");
            string Cheque = new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray());
            string ChequeName = new string(Enumerable.Repeat(random_chars_only, 20).Select(s => s[random.Next(s.Length)]).ToArray());
            string CashierCheque = new string(Enumerable.Repeat(random_chars_only, 10).Select(s => s[random.Next(s.Length)]).ToArray());
            /*****************************************************************/

            ItemtSummaryChequeModel itemModel = new ItemtSummaryChequeModel();
          
     
            itemModel.UserId = @"00001";
          //  itemModel.TaxInvoice = TaxInvoice;

            itemModel.ChequeNumber = ChequeNumber;
            itemModel.BankId = BankId;
            itemModel.BankName = BankName;
            itemModel.ChequeDate = ChequeDate;
            itemModel.Cheque = string.Format("{0:n}", decimal.Parse( Cheque));
            itemModel.ChequeName = ChequeName;
            itemModel.CashierCheque = CashierCheque;


            // OFC.WriteChequeOffLineToCSV(itemModel);

            int Year = dpSelect.Value.Year;
            if (Year > 2500)
            {
                Year = Year - 543;
            }
            string strDate = Year.ToString() + dpSelect.Value.ToString("MMdd");
            var obj = OFC.GetSummaryChequeReport(strDate);

            GridDetail.DataSource = obj;
            GlobalClass.LstSummaryChequeModel = obj;
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            GlobalClass.ReportName = "rptSummaryChequeReport.rpt";
            GlobalClass.ReportOfflineType = "CHEQUE";


            FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();
            //formm receive = new FormReceiveMoney();
            FVR.ShowDialog();
        }

        private void FormChequeReport_Load(object sender, EventArgs e)
        {

            label2.Visible = false;
            label3.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            /*************************************/

            GridDetail.AutoGenerateColumns = false;

            GridDetail.ColumnHeadersDefaultCellStyle.Font = new Font("TH Sarabun New", 14.25F, FontStyle.Bold);
        }
    }
}
