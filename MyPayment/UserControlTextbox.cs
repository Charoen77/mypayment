﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPayment
{
    public partial class UserControlTextbox : UserControl
    {
        public UserControlTextbox()
        {
            InitializeComponent();
        }
       public string TextNew
        {
            get { return LabelHolder.Text; }
            set { LabelHolder.Text = value; }
        }
        public string TextKey
        {
            get { return Textbox.Text; }
            set { Textbox.Text = value; }
        }
        public string Password {
            get { return Textbox.PasswordChar.ToString(); }
            set { Textbox.Text = value; }
        }
        public string TEst
        {
            get { return Textbox.Text; }
            set { Textbox.Text = value; }
        }
        private void Textbox_TextChanged(object sender, EventArgs e)
        {
            if (Textbox.Text == "")
                LabelHolder.Visible = true;
            else
                LabelHolder.Visible = false;
        }

        private void LabelHolder_Click(object sender, EventArgs e)
        {
           this.Textbox.Focus();
        }
    }
}
