﻿using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.FormArrears
{
    public partial class FormSearchData : Form
    {
        ResultPayment resul = new ResultPayment();
        GeneralService genService = new GeneralService(true);
        ClassLogsService meaLog = new ClassLogsService("Cancel", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("Cancel", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        public FormSearchData()
        {
            InitializeComponent();
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }       
        private void FormCancelReceipt_Load(object sender, EventArgs e)
        {
            SetControl();
        }
        public void SetControl()
        {
            DataGridViewDetail.AutoGenerateColumns = false;
            DataGridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ArrearsController arr = new ArrearsController();
                inquiryInfoBeanList cls = new inquiryInfoBeanList();
                cls.ca = TextboxCA.Text;
                cls.ui = TextBoxUI.Text;
                resul = arr.SelectDataRemittance(cls);
                if (resul.Output.Count > 0)
                    DataGridViewDetail.DataSource = resul.Output;
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            ClearData();
        }       
        public void ClearData()
        {
            TextboxCA.Text = string.Empty;
            TextBoxUI.Text = string.Empty;
        }
        private void TextboxPaymentNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
            {
                ButtonSearch_Click(null, null);
            }
        }
        private void TextBoxReceiptNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
            {
                ButtonSearch_Click(null, null);
            }
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
    }
}
