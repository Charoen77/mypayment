﻿namespace MyPayment.FormArrears
{
    partial class FormCheckUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCheckUser));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TextBoxRemark = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonLogin = new Custom_Controls_in_CS.ButtonZ();
            this.label2 = new System.Windows.Forms.Label();
            this.TextboxPass = new System.Windows.Forms.TextBox();
            this.TextboxIdUser = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.TextBoxRemark);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ButtonCancel);
            this.groupBox1.Controls.Add(this.ButtonLogin);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TextboxPass);
            this.groupBox1.Controls.Add(this.TextboxIdUser);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(460, 265);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // TextBoxRemark
            // 
            this.TextBoxRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.TextBoxRemark.Location = new System.Drawing.Point(102, 105);
            this.TextBoxRemark.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxRemark.Multiline = true;
            this.TextBoxRemark.Name = "TextBoxRemark";
            this.TextBoxRemark.Size = new System.Drawing.Size(345, 91);
            this.TextBoxRemark.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 105);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 28);
            this.label3.TabIndex = 6;
            this.label3.Text = "หมายเหตุ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 28);
            this.label1.TabIndex = 4;
            this.label1.Text = "รหัสผู้ใช้งาน";
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close1;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(271, 213);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(127, 40);
            this.ButtonCancel.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCancel.TabIndex = 4;
            this.ButtonCancel.TextLocation_X = 45;
            this.ButtonCancel.TextLocation_Y = 5;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // ButtonLogin
            // 
            this.ButtonLogin.BackColor = System.Drawing.Color.Transparent;
            this.ButtonLogin.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonLogin.BorderWidth = 1;
            this.ButtonLogin.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonLogin.ButtonText = "ตกลง";
            this.ButtonLogin.CausesValidation = false;
            this.ButtonLogin.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLogin.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonLogin.ForeColor = System.Drawing.Color.Black;
            this.ButtonLogin.GradientAngle = 90;
            this.ButtonLogin.Image = global::MyPayment.Properties.Resources.ok;
            this.ButtonLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonLogin.Location = new System.Drawing.Point(128, 213);
            this.ButtonLogin.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonLogin.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonLogin.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonLogin.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonLogin.Name = "ButtonLogin";
            this.ButtonLogin.ShowButtontext = true;
            this.ButtonLogin.Size = new System.Drawing.Size(127, 40);
            this.ButtonLogin.StartColor = System.Drawing.Color.LightGray;
            this.ButtonLogin.TabIndex = 3;
            this.ButtonLogin.TextLocation_X = 45;
            this.ButtonLogin.TextLocation_Y = 5;
            this.ButtonLogin.Transparent1 = 80;
            this.ButtonLogin.Transparent2 = 120;
            this.ButtonLogin.UseVisualStyleBackColor = true;
            this.ButtonLogin.Click += new System.EventHandler(this.ButtonLogin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 28);
            this.label2.TabIndex = 5;
            this.label2.Text = "รหัสผ่าน";
            // 
            // TextboxPass
            // 
            this.TextboxPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.TextboxPass.Location = new System.Drawing.Point(102, 62);
            this.TextboxPass.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxPass.Name = "TextboxPass";
            this.TextboxPass.PasswordChar = '*';
            this.TextboxPass.Size = new System.Drawing.Size(345, 31);
            this.TextboxPass.TabIndex = 1;
            this.TextboxPass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxPass_KeyPress);
            // 
            // TextboxIdUser
            // 
            this.TextboxIdUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.TextboxIdUser.Location = new System.Drawing.Point(102, 20);
            this.TextboxIdUser.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxIdUser.Name = "TextboxIdUser";
            this.TextboxIdUser.Size = new System.Drawing.Size(345, 31);
            this.TextboxIdUser.TabIndex = 0;
            this.TextboxIdUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxIdUser_KeyPress);
            // 
            // FormCheckUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(475, 275);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCheckUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ยืนยัน";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private Custom_Controls_in_CS.ButtonZ ButtonLogin;
        private System.Windows.Forms.TextBox TextboxIdUser;
        private System.Windows.Forms.TextBox TextboxPass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxRemark;
    }
}