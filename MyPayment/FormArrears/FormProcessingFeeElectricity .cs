﻿using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormProcessingFeeElectricity : Form
    {
        ClassLogsService meaLog = new ClassLogsService("Cancel", "DataLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        public Int64[] _debtId;
        public FormProcessingFeeElectricity(Int64[] debtId)
        {
            _debtId = debtId;
            InitializeComponent();
        }
        private void ButtonNo_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ResultCancel result = new ResultCancel();
                result.Cancel = new List<ClassCancelContinue>(); 
                if (GlobalClass.Cancel != null)
                {
                    foreach (var item in GlobalClass.Cancel)
                    {
                        ClassCancelContinue Cancel = new ClassCancelContinue();
                        Cancel.debtId = item.debtId;
                        Cancel.userId = item.userId;
                        Cancel.authorizedCancelBy = item.authorizedCancelBy;
                        result.Cancel.Add(Cancel);
                    }
                    ClassCancelContinue CancelDetail = new ClassCancelContinue();
                    CancelDetail.debtId = _debtId.ToString();
                    CancelDetail.userId = GlobalClass.UserId;
                    CancelDetail.authorizedCancelBy = GlobalClass.UserName;
                    result.Cancel.Add(CancelDetail);
                }
                else
                {
                    ClassCancelContinue Cancel = new ClassCancelContinue();
                    Cancel.debtId = _debtId.ToString();
                    Cancel.userId = GlobalClass.UserId;
                    Cancel.authorizedCancelBy = GlobalClass.UserName;
                    result.Cancel.Add(Cancel);
                }
                GlobalClass.Cancel = result.Cancel;          
                this.Close();
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }            
        }
        private void ButtonYes_Click(object sender, EventArgs e)
        {
            this.Close();         
        }
    }
}
 