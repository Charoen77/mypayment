﻿using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormCancelContinue : Form
    {
        ResultCostTDebt resul = new ResultCostTDebt();
        ArrearsController arr = new ArrearsController();
        GeneralService genService = new GeneralService(true);
        ClassLogsService meaLog = new ClassLogsService("Cancel", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("Cancel", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        public FormCancelContinue()
        {
            InitializeComponent();
        }
        private void ButtonOk_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (DataGridViewDetail.Rows.Count > 0)
                {
                    List<ClassCancelContinue> lstReceip = new List<ClassCancelContinue>();
                    bool status = false;
                    int i = 0;
                    var loopTo = DataGridViewDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        ClassCancelContinue Continue = new ClassCancelContinue();
                        DataGridViewRow rownew = DataGridViewDetail.Rows[i];
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        string debtId = rownew.Cells["ColDebtId"].EditedFormattedValue.ToString();
                        string ca = rownew.Cells["ColCa"].EditedFormattedValue.ToString();
                        bool isChecked = (bool)checkCell.EditedFormattedValue;
                        if (isChecked)
                        {
                            status = true;
                            Continue.ca = ca;
                            Continue.debtId = debtId;
                            lstReceip.Add(Continue);
                        }
                    }
                    if (status)
                    {
                        if (MessageBox.Show("คุณต้องการยกเลิกค่างดจ่ายไฟ", "ยกเลิก", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;
                        else
                        {
                            FormCheckUser checkUser = new FormCheckUser();
                            checkUser.ShowDialog();
                            checkUser.Dispose();
                            if (GlobalClass.AuthorizeLevel == 2)
                            {
                                if (lstReceip.Count > 0)
                                {
                                    foreach (var item in lstReceip)
                                    {
                                        string strJSON = null;
                                        ClassCancelContinue Cancel = new ClassCancelContinue();
                                        Cancel.debtId = item.debtId;
                                        Cancel.userId = GlobalClass.CanceluserId;
                                        Cancel.approveEmpId = GlobalClass.UserIdCancel;
                                        string result = JsonConvert.SerializeObject(Cancel);
                                        strJSON = ClassReadAPI.GetDataAPI("cancelUnwireFee", result);
                                        ClassUser user = ClassReadAPI.ReadToObject(strJSON);
                                        if (user.result_code == "SUCCESS")
                                        {
                                            meaLog.WriteData("Success :: Debt ID : " + item.debtId.ToString() + " |  User ID : " + GlobalClass.UserIdCancel, method, LogLevel.Debug);
                                            List<inquiryGroupDebtBeanList> lst = GlobalClass.LstCost.Where(c => c.debtType == "").ToList();
                                            inquiryGroupDebtBeanList lstCost = lst.Find(itemDetail => itemDetail.ca.Equals(item.ca, StringComparison.CurrentCultureIgnoreCase));
                                            if (lstCost != null)
                                                lstCost.statusType = false;

                                            inquiryInfoBeanList lstInfo = GlobalClass.LstInfo.Find(itemDetail => itemDetail.ca.Equals(item.ca, StringComparison.CurrentCultureIgnoreCase));
                                            if (lstInfo != null)
                                                lstInfo.debtType = false;
                                        }
                                        else
                                        {
                                            meaLog.WriteData("Fail :: Debt ID : " + item.debtId.ToString() + " |  User ID : " + GlobalClass.UserIdCancel, method, LogLevel.Debug);
                                            MessageBox.Show(user.result_message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        }
                                    }
                                }
                                this.Close();
                            }
                            else
                                MessageBox.Show("User ที่สามารถยกเลิกค่างดจ่ายไฟได้ต้องเป็น Level 2 เท่านั้น", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
                else
                    MessageBox.Show("กรุณาเลือกข้อมูลที่ต้องการยกเลิกค่างดจ่ายไฟ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }           
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void LoadData()
        {
            resul = arr.LoadDataContinue();
            DataGridViewDetail.DataSource = resul;
        }
        private void CheckBoxSelectAll_Click(object sender, EventArgs e)
        {
            int i = 0;
            var loopTo = DataGridViewDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CheckBoxSelectAll.Checked == true)
                    DataGridViewDetail.Rows[i].Cells["ColCheckBox"].Value = true;
                else
                    DataGridViewDetail.Rows[i].Cells["ColCheckBox"].Value = false;
            }
        }
        private void DataGridViewDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                int i = 0;
                int countCheck = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (DataGridViewDetail.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        if (newBool)
                        {
                            DataGridViewDetail.Rows[i].Cells[0].Value = true;
                            countCheck++;
                            if (countCheck == DataGridViewDetail.Rows.Count)
                                CheckBoxSelectAll.Checked = true;
                        }
                        else
                        {
                            DataGridViewDetail.Rows[i].Cells[0].Value = false;
                            CheckBoxSelectAll.Checked = false;
                            if (countCheck == 0)
                                countCheck = 0;
                        }
                    }
                }
            }
        }
    }
}
