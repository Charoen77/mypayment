﻿using MyPayment.Controllers;
using MyPayment.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormArrears : Form
    {
        System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
        #region Global Variable
        GeneralService genService = new GeneralService(true);
        public bool q_count = false;
        ClassLogsService LogQueue = new ClassLogsService("Queue", "DataLog");
        ClassLogsService LogQueueError = new ClassLogsService("Queue", "ErrorLog");
        public string strUserCode = "1506013";
        public string oldSendMessage;
        public string strIPAddress = GlobalClass.IpConfig;//ConfigurationManager.AppSettings["IPAddress"].ToString();
        public bool isRefreshData;
        public string retMessage;
        public ArrayList arrData;
        public string oldData;
        public int printErrorAmount;
        public int SleepTime;
        public int TotalPrint;
        public string QNo = "000";
        public bool _status = false;
        public bool StatusPanel;
        public int BarcodeCount = 0;
        public bool _statusScan = false;
        public bool status = false;
        private int _rowIndex = 0;
        private string _custtype;
        ArrearsController arrears = new ArrearsController();
        ResultInquiryDebt resul = new ResultInquiryDebt();
        List<result> _resulQRCode = new List<result>();
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ClassLogsService LogPayment = new ClassLogsService("Payment", "DataLog");
        ClassLogsService LogEventPayment = new ClassLogsService("Payment", "EventLog");
        ClassLogsService LogErrorPayment = new ClassLogsService("Payment", "ErrorLog");
        Control cnt;
        //ProgressBar _probar = new ProgressBar();
        public CultureInfo usCulture = new CultureInfo("en-US");
        public CultureInfo _usCultureTh = new CultureInfo("th-TH");
        #endregion
        public FormArrears()
        {
            InitializeComponent();
        }
        public void SetControl()
        {
            DataGridViewDetail.AutoGenerateColumns = false;
            DataGridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void InitialData()
        {
            TextboxTotalList.Text = "0";
            TextboxPayList.Text = "0";
            txtAmount.Text = "0.00";
            txtVat.Text = "0.00";
            txtFine.Text = "0.00";
            txtTotal.Text = "0.00";
            txtTotalSum.Text = "0.00";
            txtTotalSum.Tag = "0.00";
        }
        private void FormArrearsNew_Load(object sender, EventArgs e)
        {
            LabelFkName.Text = GlobalClass.Dist;
            LabelEmployeeCode.Text = GlobalClass.UserId.ToString();
            LabelEmployeeName.Text = GlobalClass.UserName;
            LabelStationNo.Text = Convert.ToDecimal(GlobalClass.No).ToString("00");
            ComboBoxDocType.SelectedIndex = 0;
            comboBoxDetail.SelectedIndex = 0;
            SetPanel(true);
            GlobalClass.LstInfo = null;
            GlobalClass.LstCost = null;
            GlobalClass.List = null;
            SetControl();
            InitialData();
        }
        public void SetPanel(bool status)
        {
            if (status)
            {
                StatusPanel = true;
                PanelNoneQ.Visible = true;
                PanelHaveQ.Visible = false;
                TextBoxBarcode.Text = string.Empty;
                ComboBoxDocType.SelectedIndex = 0;
            }
            else
            {
                StatusPanel = false;
                PanelHaveQ.Visible = true;
                PanelNoneQ.Visible = false;
                ButtonResetQ.Enabled = true;
            }
        }
        private void ButtonRec_Click(object sender, EventArgs e)
        {
            if (TextboxPayList.Text.Trim() != "0")
            {
                string cusName = "";
                int i = 0;
                List<ClassModelsReport> _list = new List<ClassModelsReport>();
                foreach (DataGridViewRow row in DataGridViewDetail.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["ColCheckBox"].EditedFormattedValue))
                    {
                        if (i == 0)
                            cusName = (row.Cells["ColCusName"].Value != null) ? row.Cells["ColCusName"].Value.ToString() : "";

                        ClassModelsReport report = new ClassModelsReport();
                        report.DistName = GlobalClass.DistName;
                        report.ReceiptBranch = GlobalClass.ReceiptBranch;
                        report.CustomerName = (row.Cells["ColCusName"].Value != null) ? row.Cells["ColCusName"].Value.ToString() : "";
                        report.CustomerTaxId = (row.Cells["ColCusTaxId"].Value != null) ? row.Cells["ColCusTaxId"].Value.ToString() : "";
                        report.CustomerTaxBranch = (row.Cells["ColCusTaxBranch"].Value != null) ? row.Cells["ColCusTaxBranch"].Value.ToString() : "";
                        report.CustomerAddress = (row.Cells["ColCusAddress"].Value != null) ? row.Cells["ColCusAddress"].Value.ToString() : "";
                        report.Name = (row.Cells["ColCustomerName"].Value != null) ? row.Cells["ColCustomerName"].Value.ToString() : "";
                        report.CoAddress = (row.Cells["ColAddress"].Value != null) ? row.Cells["ColAddress"].Value.ToString() : "";
                        report.UI = (row.Cells["ColUI"].Value != null) ? row.Cells["ColUI"].Value.ToString() : null;
                        report.CA = row.Cells["ColCA"].Value.ToString();
                        report.EmpId = LabelEmployeeCode.Text;
                        report.EmpName = LabelEmployeeName.Text;
                        report.CusType = (row.Cells["ColType"].Value != null) ? row.Cells["ColType"].Value.ToString() : "";
                        _list.Add(report);
                        i++;
                    }
                }
                GlobalClass.List = _list;
                FormReceiveMoney receive = new FormReceiveMoney();
                receive.Countamount = (txtTotalSum.Text.Trim() != "") ? Convert.ToDouble(txtTotalSum.Text.Trim()) : 0;
                receive.CustomerName = cusName;
                receive.ShowDialog();
                receive.Dispose();
                if (GlobalClass.StatusFormMoney == 1)
                {
                    GlobalClass.PayMent = null;
                    GlobalClass.LstInfo = null;
                    ClearData();
                    RefreshGridView();
                }
                GlobalClass.StatusFormMoney = null;
            }
        }
        public void ClearData()
        {
            GlobalClass.LstCheque = null;
            GlobalClass.LstSub = null;
            resul.Output = null;
            resul.OutputTDebt = null;
            resul.OutputSumlist = null;
            TextboxPayList.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtVat.Text = string.Empty;
            txtTotal.Text = string.Empty;
            txtFine.Text = string.Empty;
            txtTotalSum.Text = string.Empty;
            TextboxTotalList.Text = string.Empty;
            GlobalClass.LstInfo = null;
            GlobalClass.LstCost = null;
            GridDetail.DataSource = null;
            DataGridViewDetail.DataSource = null;
            labelFk.Text = string.Empty;
            labelCa.Text = string.Empty;
            labelAddress.Text = string.Empty;
            labelUi.Text = string.Empty;
            labelBranch.Text = string.Empty;
            labelTaxId.Text = string.Empty;
            labelBill.Text = string.Empty;
            lblReceiptName.Text = string.Empty;
            CheckBoxSelectAll.Checked = false;
            CheckBoxSelect.Checked = false;
            comboBoxDetail.SelectedIndex = 0;
            txtCount.Text = string.Empty;
            lblCaDetail.Text = string.Empty;
            lblAmountDetail.Text = string.Empty;
            lblStatusC.Visible = false;
            lblStatusD.Visible = false;
            LabelTmp_Time.Visible = false;
            GlobalClass.StatusForm = null;
            GlobalClass.StatusFormMoney = null;
            GlobalClass.AuthorizeLevel = 0;
            GlobalClass.CancelElectricity = null;
            GlobalClass.Cancel = null;
        }
        private void ButtonAdd_Click(object sender, EventArgs e)
        {

        }
        private void ButtonCallQ_Click(object sender, EventArgs e)
        {
            //GetDataToGridview("100033122", 0);
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ButtonCallQ.Enabled = false;
                SetProgressBar(true);
                //if (q_count == false)
                //{
                if (!clientSocket1.IsConnected())
                {
                    LogQueue.WriteData("Connect Queue", method, LogLevel.Debug);
                    ConnectQ();
                    LogQueue.WriteData("Connect Success", method, LogLevel.Debug);
                }
                //    q_count = true;
                //}

                //GlobalClass.UserId = Convert.ToInt32(226173);
                //GlobalClass.No = "09";
                string strSum = "";
                strSum = arrears.MyXor((char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01");
                oldSendMessage = (char)1 + strIPAddress + (char)2 + (char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01" + (char)(3) + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)4;
                LogQueue.WriteData("Send Message :: " + oldSendMessage, method, LogLevel.Debug);
                clientSocket1.Send(oldSendMessage);
            }
            catch (Exception ex)
            {
                LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message, "IP Server : " + "QServer" + " Port : " + "QPort", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void SetProgressBar(bool status)
        {
            if (status == true)
            {
                ProgressBar.Invoke(new MethodInvoker(delegate { ProgressBar.Location = new Point(377, 84); }));
                ProgressBar.Invoke(new MethodInvoker(delegate { ProgressBar.Size = new Size(278, 271); }));
            }
            else
            {
                ProgressBar.Invoke(new MethodInvoker(delegate { ProgressBar.Location = new Point(212, 0); }));
                ProgressBar.Invoke(new MethodInvoker(delegate { ProgressBar.Size = new Size(0, 0); }));
            }
            ProgressBar.Invoke(new MethodInvoker(delegate { ProgressBar.Visible = status; }));
        }
        public void ConnectQ()
        {
            string host = ConfigurationManager.AppSettings["HostQueue"].ToString();
            int port = int.Parse(ConfigurationManager.AppSettings["Port"].ToString());
            clientSocket1.Connect(host, port);
        }
        private ArrayList getData(string data)
        {
            ArrayList retArr = new ArrayList();
            if (data == ((char)1).ToString())
                data = data.Substring(1);
            int intSTX = data.IndexOf("2");
            if (intSTX > -1)
                data = data.Substring((intSTX + 1));

            string[] tmpData = data.Split(' ');
            tmpData = tmpData[0].Split('~');
            string[] lines = Regex.Split(data, @"\W+");
            LabelNoQ.Invoke(new MethodInvoker(delegate { LabelNoQ.Text = tmpData[4].ToString(); }));
            LabelNoQ.Invoke(new MethodInvoker(delegate { LabelNoQ.Refresh(); }));
            LabelNoQ.Invoke(new MethodInvoker(delegate { this.Refresh(); }));

            if (tmpData[4] == "0000" || tmpData[4] == "000")
            {
                timer2.Enabled = true;
                SetPanelCancelWait(true);
                ButtonCancelQ.Invoke(new MethodInvoker(delegate { ButtonCancelQ.Enabled = false; }));
                ButtonRefreshQ.Invoke(new MethodInvoker(delegate { ButtonRefreshQ.Enabled = false; }));
                ButtonResetQ.Invoke(new MethodInvoker(delegate { ButtonResetQ.Enabled = false; }));
                PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Enabled = true; }));
                return retArr;
            }
            else
            {
                // Me.Enabled = True
                // waitqueue.Close()
                SetPanelCancelWait(false);
            }

            //Colorized by: CarlosAg.CodeColorizer
            int i = 0;
            for (i = 0; (i <= (int.Parse(lines[9]) - 1)); i++)
            {
                String[] realData = new String[2];
                realData[0] = lines[10 + (i * 2)];
                realData[1] = lines[11 + (i * 2)];
                retArr.Add(realData);
            }
            return retArr;
        }
        public void SetPanelCancelWait(bool status)
        {
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Visible = status; }));
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Location = new Point(178, 7); }));
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Size = new Size(609, 386); }));
        }
        private void ButtonRefreshQ_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (!clientSocket1.IsConnected())
                    ConnectQ();
                if (ButtonResetQ.Enabled == true)
                {
                    string strSum = arrears.MyXor((char)82 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
                    // strSum = Convert.ToChar(CInt(strSum))
                    string sendData = (char)1 + strIPAddress + (char)2 + (char)82 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim() + (char)3 + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)4;
                    oldSendMessage = sendData;
                    //LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                    clientSocket1.Send(sendData);
                }
                else
                {
                    // ClientSocket1.Send("04|" + txtEmpCode.Text.Trim + "|" + txtStationNo.Text.Trim + "|" + txtListNum1.Text)
                }
            }
            catch (Exception ex)
            {
                LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                //LogQueue.WriteData("Sleep " + (SleepTime * (1000 * TotalPrint)).ToString(), method, LogLevel.Debug);
                Thread.Sleep(SleepTime * (1000 * TotalPrint));
                //LogQueue.WriteData("Wake up", method, LogLevel.Debug);
            }
            catch (Exception)
            {
            }
            string strSum = arrears.MyXor((char)(85) + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
            if (printErrorAmount <= 0)
            {
                string sendData = ((char)(1) + strIPAddress + (char)(2) + (char)(85) + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~"
                            + LabelNoQ.Text.Trim() + (char)(3) + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)(4));
                oldSendMessage = sendData;
                //LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                clientSocket1.Send(sendData);
                timer1.Enabled = false;
            }
            timer1.Enabled = false;
        }
        public bool GetDataToGridview(string id, int selectIndex)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            string strBarcade = "";
            try
            {
                ClassUser clsUser = new ClassUser();
                ArrearsController arr = new ArrearsController();
                if (StatusPanel)
                {
                    if (selectIndex == 0)
                    {
                        clsUser.ca = TextBoxBarcode.Text.Trim();
                        strBarcade = " CA :: " + TextBoxBarcode.Text.Trim();
                    }
                    else if (selectIndex == 1)
                    {
                        clsUser.reqNo = TextBoxBarcode.Text.Trim();
                        strBarcade = " ReqNo :: " + TextBoxBarcode.Text.Trim();
                    }
                    else if (selectIndex == 2)
                    {
                        clsUser.custId = TextBoxBarcode.Text.Trim();
                        strBarcade = " Cust Id :: " + TextBoxBarcode.Text.Trim();
                    }
                    else if (selectIndex == 3)
                    {
                        clsUser.invNo = TextBoxBarcode.Text.Trim();
                        strBarcade = " Inv No :: " + TextBoxBarcode.Text.Trim();
                    }
                    else if (selectIndex == 4)
                    {
                        clsUser.ui = TextBoxBarcode.Text.Trim();
                        strBarcade = " UI :: " + TextBoxBarcode.Text.Trim();
                    }
                    else if (selectIndex == 6)
                    {
                        clsUser.QNo = TextBoxBarcode.Text.Trim();
                        strBarcade = " Q :: " + TextBoxBarcode.Text.Trim();
                    }
                    else if (selectIndex == 7)
                    {
                        clsUser.collId = TextBoxBarcode.Text.Trim();
                        strBarcade = " CollId :: " + TextBoxBarcode.Text.Trim();
                    }
                    else
                    {
                        clsUser.collInvNo = TextBoxBarcode.Text.Trim();
                        strBarcade = " CollInvNo :: " + TextBoxBarcode.Text.Trim();
                    }
                }
                else
                {
                    clsUser.ui = id;
                    strBarcade = " UI :: " + id;
                }
                resul = arr.SelectProcessInquiry(clsUser, resul, strBarcade);
                #region
                if (GlobalClass.Cancel != null)
                {
                    foreach (var item in GlobalClass.Cancel)
                    {
                        inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == item.ca).FirstOrDefault();
                        inquiryGroupDebtBeanList costTDebt = info.inquiryGroupDebtBeanList.Find(c => c.debtIdSet.ToString().Equals(item.debtId, StringComparison.CurrentCultureIgnoreCase));
                        if (costTDebt != null)
                            costTDebt.statusCancel = false;
                    }
                }
                #endregion
                if (resul.Output != null && resul.Output.Count != 0)
                    tf = true;
                else
                    tf = false;

                if (resul.ResultMessage != null)
                {
                    foreach (var item in resul.Output)
                    {
                        labelFk.Text = item.FirstName;
                        labelCa.Text = item.ca;
                        labelAddress.Text = item.coAddress;
                        labelUi.Text = item.ui;
                        labelBranch.Text = item.TaxBranch;
                        labelTaxId.Text = item.Tax20;
                        labelBill.Text = item.collId;
                        lblReceiptName.Text = item.payeeEleName;
                    }
                    //inquiryInfoBeanList resulDelete = resul.Output.Where(c => c.ca == labelCa.Text).FirstOrDefault();
                    //if (resulDelete != null)
                    //    resul.Output.Remove(resulDelete);
                    SetProgressBar(false);
                    MessageBox.Show(resul.ResultMessage.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    tf = false;
                }
                else
                {
                    TextboxTotalList.Text = resul.Output.Count.ToString();
                    SetControl();
                    this.DataGridViewDetail.DataSource = resul.Output;

                    double Total = 0;
                    double Amount = 0;
                    double TotalVat = 0;
                    double Fine = 0;
                    int i = 0;
                    int countCheck = 0;
                    var loopTo = DataGridViewDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (Convert.ToBoolean(DataGridViewDetail.Rows[i].Cells["ColckCancel"].Value) == true)
                            DataGridViewDetail.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 154, 154);
                        if (DataGridViewDetail.Rows[i].Cells["colStatus"].Value.ToString() != "")
                        {
                            DataGridViewDetail.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(205, 120, 91);
                            lblStatusC.Visible = true;
                        }
                        else
                            lblStatusC.Visible = false;

                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        if (newBool)
                        {
                            Amount += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColTotalAmount"].Value);
                            TotalVat += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColVat"].Value);
                            Fine += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColPenaltyAll"].Value);
                            Total += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColSumTotalAmount"].Value);
                            DataGridViewDetail.Rows[i].Cells[0].Value = true;
                            countCheck++;
                        }
                        else
                            DataGridViewDetail.Rows[i].Cells[0].Value = false;
                    }
                    if (countCheck == DataGridViewDetail.Rows.Count)
                        CheckBoxSelectAll.Checked = true;
                    else
                        CheckBoxSelectAll.Checked = false;

                    TextboxPayList.Text = countCheck.ToString();
                    txtAmount.Text = Amount.ToString("#,###,###,##0.00");
                    txtVat.Text = TotalVat.ToString("#,###,##0.00");
                    txtTotal.Text = (Amount + TotalVat).ToString("#,###,###,##0.00");
                    txtFine.Text = Fine.ToString("#,###,###,##0.00");
                    txtTotalSum.Text = (Total + Fine).ToString("#,###,###,##0.00");
                }
            }
            catch (Exception ex)
            {
                SetProgressBar(false);
                LogErrorPayment.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return tf;
        }
        public void SelectCheckbox()
        {
            double Total = 0;
            double Amount = 0;
            double TotalVat = 0;
            double Fine = 0;
            int i = 0;
            int countCheck = 0;
            var loopTo = DataGridViewDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CheckBoxSelectAll.Checked == true)
                {
                    Amount += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColTotalAmount"].Value);
                    TotalVat += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColVat"].Value);
                    Fine += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColPenaltyAll"].Value);
                    Total += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColSumTotalAmount"].Value);
                    DataGridViewDetail.Rows[i].Cells[0].Value = true;
                    countCheck++;
                }
                else
                {
                    DataGridViewDetail.Rows[i].Cells[0].Value = false;
                    countCheck = 0;
                }
            }
            TextboxPayList.Text = countCheck.ToString();
            txtAmount.Text = Amount.ToString("#,###,###,##0.00");
            txtVat.Text = TotalVat.ToString("#,###,##0.00");
            txtTotal.Text = Total.ToString("#,###,###,##0.00");
            txtFine.Text = Fine.ToString("#,###,###,##0.00");
            txtTotalSum.Text = (Total + Fine).ToString("#,###,###,##0.00");
            //lblAmountDetail.Text = Total.ToString("#,###,###,##0.00");
        }
        private void ButtonCancelQ_Click(object sender, EventArgs e)
        {
            if ((sender as Button).Text.ToString() != "ยกเลิกการรอคิว")
            {
                if (MessageBox.Show("ต้องการยกเลิกคิวนี้หรือไม่ ", "ยกเลิกคิว", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                else
                    q_count = false;
            }
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            if (ButtonResetQ.Enabled)
            {
                string strSum = arrears.MyXor((char)67 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
                string sendData = (char)1 + strIPAddress + (char)2 + (char)67 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim() + (char)3 + "" + (char)(Convert.ToInt32(strSum)) + "" + Convert.ToChar(4);
                oldSendMessage = sendData;
                //LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                clientSocket1.Send(sendData);
                //lblCallQ.Enabled = false;
            }
            else
            {
                string strSum = arrears.MyXor((char)67 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
                string sendData = (char)1 + strIPAddress + (char)2 + (char)67 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim() + (char)3 + "" + (char)(Convert.ToInt32(strSum)) + "" + Convert.ToChar(4);
                oldSendMessage = sendData;
                //LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                clientSocket1.Send(sendData);
                //lblCallQ.Enabled = true;
                //RadioButtonnNoneQ.Checked = true;
                //lblDeleteQ.Enabled = false;
                //lblRefreshQ.Enabled = false;
            }

            //SetPanelCancelWait();
        }
        private void ButtonResetQ_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ButtonResetQ.Enabled = false;
            string strSum = arrears.MyXor((char)80 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
            string sendData = (char)1 + strIPAddress + (char)2 + (char)80 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim() + (char)3 + "" + (char)(Convert.ToInt32(strSum)) + "" + Convert.ToChar(4);
            oldSendMessage = sendData;
            //LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
            clientSocket1.Send(sendData);

            ButtonRefreshQ.Enabled = false;
            ButtonCancelQ.Enabled = false;
            ButtonCallQ.Enabled = true;
            SetPanel(true);
        }
        private void ButtonCancelWait_Click(object sender, EventArgs e)
        {
            this.ButtonResetQ_Click(sender, e);
            this.ButtonCancelQ_Click(sender, e);
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Visible = false; }));
        }
        private void ButtonCallQNone_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            //SetPanelLoadProgressBar(true);
            try
            {
                ButtonCallQ.Enabled = false;
                if (!clientSocket1.IsConnected())
                {
                    LogQueue.WriteData("Connect Queue", method, LogLevel.Debug);
                    ConnectQ();
                    LogQueue.WriteData("Connect Success", method, LogLevel.Debug);
                    SetPanel(false);
                }
                //if (q_count == false)
                //{
                //    clientSocket1.Connect("10.76.52.114", 15068);
                //    q_count = true;
                //}

                //GlobalClass.UserId = Convert.ToInt32(2261732);
                //GlobalClass.No = "09";
                string strSum = "";
                strSum = arrears.MyXor((char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01");
                oldSendMessage = (char)1 + strIPAddress + (char)2 + (char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01" + (char)(3) + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)4;
                //LogQueue.WriteData("Send Message :: " + oldSendMessage, method, LogLevel.Debug);
                clientSocket1.Send(oldSendMessage);
            }
            catch (Exception ex)
            {
                ButtonCallQ.Enabled = true;
                SetPanel(true);
                //SetPanelLoadProgressBar(false);
                LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message, "IP Server : " + "QServer" + " Port : " + "QPort", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void clientSocket_Receive(string receiveData)
        {
            //SetPanelCancelWait(true);
            if (receiveData.ToString().Substring(0, 1) != "")
            {
                isRefreshData = true;
                return;
            }
            retMessage = receiveData;
            oldData = retMessage;
            arrData = getData(retMessage);
            if (StatusPanel)
                ComboBoxDocType.Invoke(new MethodInvoker(delegate { ComboBoxDocType.SelectedIndex = 1; }));

            //foreach (var item in arrData)
            //{
            //    GetDataToGridview(((string[])item)[1].ToString(),0);
            //}

            //int i = 0;
            //for (i = 0; (i <= (arrData.Count - 1)); i++)
            //{
            //    ComboBoxDocType.SelectedIndex = 1;
            //    if (i != (arrData.Count - 1))
            //    {
            //        //TextBoxBarcode.Text = arrData[1].ToString();
            //        //if (arrData[0] == "INV")
            //        //    ComboBoxDocType.SelectedIndex = 4;
            //        //else if (arrData[0] == "REF")
            //        //    ComboBoxDocType.SelectedIndex = 5;
            //        //else if (arrData[0] == "MET")
            //        //    ComboBoxDocType.SelectedIndex = 6;

            //    }
            //    else
            //    {
            //        //TextBoxBarcode.Text = arrData[1].ToString();
            //        //if (arrData[0].ToString() == "INV")
            //        //    ComboBoxDocType.SelectedIndex = 4;
            //        //else if (arrData[0].ToString() == "REF")
            //        //    ComboBoxDocType.SelectedIndex = 5;
            //        //else if (arrData[0].ToString() == "MET")
            //        //    ComboBoxDocType.SelectedIndex = 6;

            //    }
            //    try
            //    {
            //        string[] barcode_array;
            //        barcode_array = this.TextBoxBarcode.Text.Split('\r');
            //        TextBoxBarcode.Text = "";
            //        for (int f = 0; f <= (barcode_array.Length - 1); f++)
            //        {
            //            TextBoxBarcode.Text = (TextBoxBarcode.Text + barcode_array[f]);
            //            //searchCa();
            //        }

            //        // -----------------------------------------------------
            //    }
            //    catch (Exception ex)
            //    {
            //    }

            //}


            //if (int.Parse(LabelNumberQ_Have.Text.Trim()) > 0)
            //{

            //LabelNumberQ_Have.Text = "1";

            //XMLSelect(CurrentList);
            //CallUpdateXMLSelectALL(false);

            //}

            if (this.LabelNoQ.Text == "000" || LabelNoQ.Text == "0000")
            {
                try
                {
                    ButtonRefreshQ.Invoke(new MethodInvoker(delegate { ButtonRefreshQ.Enabled = false; }));
                    ButtonCancelQ.Invoke(new MethodInvoker(delegate { ButtonCancelQ.Enabled = false; }));
                    ButtonResetQ.Invoke(new MethodInvoker(delegate { ButtonResetQ.Enabled = false; }));
                    SetPanelCancelWait(true);
                    SetProgressBar(false);
                    clientSocket1.Close();
                    QNo = "0000";
                }
                catch (Exception)
                {

                }

            }
            else
            {
                ButtonRefreshQ.Invoke(new MethodInvoker(delegate { ButtonRefreshQ.Enabled = true; }));
                ButtonResetQ.Invoke(new MethodInvoker(delegate { ButtonResetQ.Enabled = true; }));
                ButtonResetQ.Invoke(new MethodInvoker(delegate { SetPanel(false); }));
                ButtonCancelQ.Invoke(new MethodInvoker(delegate { ButtonCancelQ.Enabled = true; }));
                QNo = LabelNoQ.Text;
                ButtonCallQ.Invoke(new MethodInvoker(delegate { ButtonCallQ.Enabled = true; }));
                SetPanelCancelWait(false);
                SetProgressBar(false);

            }
            isRefreshData = true;
        }
        private void clientSocket2_Receive(string receiveData)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            //LogQueue.WriteData("Receive :: " + receiveData, method, LogLevel.Debug);
            string retMessage = receiveData;
            oldData = retMessage;
            arrData = GetDataCallQ(retMessage);
        }
        public void CallQ()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                string host = ConfigurationManager.AppSettings["HostQueue"].ToString();
                int port = int.Parse(ConfigurationManager.AppSettings["Port"].ToString());

                if (!clientSocket2.IsConnected())
                {
                    LogQueue.WriteData("Connect Queue", method, LogLevel.Debug);
                    clientSocket2.Connect(host, port);
                    LogQueue.WriteData("Connect Success", method, LogLevel.Debug);
                }

                //GlobalClass.UserId = Convert.ToInt32(2261732);
                //GlobalClass.No = "09";
                string strSum = "";
                strSum = arrears.MyXor((char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01");
                oldSendMessage = (char)1 + strIPAddress + (char)2 + (char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01" + (char)(3) + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)4;
                //LogQueue.WriteData("Send Message :: " + oldSendMessage, method, LogLevel.Debug);
                clientSocket2.Send(oldSendMessage);
            }
            catch (Exception ex)
            {
                LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message, "IP Server : " + "QServer" + " Port : " + "QPort", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public ArrayList GetDataCallQ(string data)
        {
            ArrayList array = new ArrayList();
            if (data == ((char)1).ToString())
                data = data.Substring(1);
            int intSTX = data.IndexOf("2");
            if (intSTX > -1)
                data = data.Substring((intSTX + 1));

            string[] tmpData = data.Split(' ');
            tmpData = tmpData[0].Split('~');
            if (tmpData[4].ToString() != "0000")
            {
                LabelNoQ.Invoke(new MethodInvoker(delegate { LabelNoQ.Text = tmpData[4].ToString(); }));
                LabelNoQ.Invoke(new MethodInvoker(delegate { LabelNoQ.Refresh(); }));
                LabelNoQ.Invoke(new MethodInvoker(delegate { this.Refresh(); }));
                SetPanelCancelWait(false);
                timer2.Enabled = false;
                clientSocket2.Close();
                EnabledButton(true);
                ButtonCallQ.Invoke(new MethodInvoker(delegate { ButtonCallQ.Enabled = false; }));
            }
            return array;
        }
        public void EnabledButton(bool status)
        {
            ButtonRefreshQ.Invoke(new MethodInvoker(delegate { ButtonRefreshQ.Enabled = status; }));
            ButtonCancelQ.Invoke(new MethodInvoker(delegate { ButtonCancelQ.Enabled = status; }));
            ButtonResetQ.Invoke(new MethodInvoker(delegate { ButtonResetQ.Enabled = status; }));
            ButtonCallQ.Invoke(new MethodInvoker(delegate { ButtonCallQ.Enabled = status; }));
        }
        private void timer2_Tick(object sender, EventArgs e)
        {
            CallQ();
        }
        public void searchCa(string userId)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            //LogQueue = new ClassLogsService("Search");
        }
        private void TextBoxBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                //int cInt = Convert.ToInt32(e.KeyChar);
                ////if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8|| cInt == 13)
                ////{
                //if (TextBoxBarcode.Text.Trim() != string.Empty)
                //{
                //    if (ComboBoxDocType.SelectedIndex == 6)
                //        TextBoxBarcode.MaxLength = 4;
                //    else
                //    {
                //        if (cInt == 13)
                //        {
                //            BarcodeCount++;
                //            if (BarcodeCount == 2)
                //            {
                //                string ca = SplitData();
                //                TextBoxBarcode.Text = ca;
                //                //GetDataToGridview(ca, ComboBoxDocType.SelectedIndex);
                //            }
                //            else
                //            {
                //                //BarcodeCount = 0;
                //                //if ()
                //                //    GetDataToGridview(TextBoxBarcode.Text.Trim(), ComboBoxDocType.SelectedIndex);
                //            }
                //            //TextBoxBarcode.Text = string.Empty;
                //            //TextBoxBarcode.Focus();
                //        }
                //    }
                //}
                ////    e.Handled = false;
                ////}
                ////else
                ////{
                ////    MessageBox.Show("กรอกตัวเลขเท่านั้น", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ////    e.Handled = true;
                ////}
            }
            catch (Exception)
            {
                TextBoxBarcode.Text = string.Empty;
                TextBoxBarcode.Focus();
            }
        }
        private void DataGridViewDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    double Total = 0;
                    double Amount = 0;
                    double TotalVat = 0;
                    double Fine = 0;
                    int i = 0;
                    int countCheck = 0;
                    var loopTo = DataGridViewDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (DataGridViewDetail.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            DataGridViewCell cell = DataGridViewDetail.CurrentCell;
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                            bool newBool = (bool)checkCell.EditedFormattedValue;
                            if (newBool)
                            {
                                Amount += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColTotalAmount"].Value);
                                TotalVat += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColVat"].Value);
                                Fine += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColPenaltyAll"].Value);
                                Total += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColSumTotalAmount"].Value);
                                DataGridViewDetail.Rows[i].Cells[0].Value = true;
                                countCheck++;
                                if (countCheck == DataGridViewDetail.Rows.Count)
                                    CheckBoxSelectAll.Checked = true;
                            }
                            else
                            {
                                DataGridViewDetail.Rows[i].Cells[0].Value = false;
                                CheckBoxSelectAll.Checked = false;
                                if (countCheck == 0)
                                    countCheck = 0;
                            }
                        }
                    }
                    TextboxPayList.Text = countCheck.ToString();
                    txtAmount.Text = Amount.ToString("#,###,###,##0.00");
                    txtVat.Text = TotalVat.ToString("#,###,##0.00");
                    txtTotal.Text = Total.ToString("#,###,###,##0.00");
                    txtFine.Text = Fine.ToString("#,###,###,##0.00");
                    txtTotalSum.Text = (Total + Fine).ToString("#,###,###,##0.00");
                    lblAmountDetail.Text = Total.ToString("#,###,###,##0.00");
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormArrearsNew | Function :: DataGridViewDetail_CellContentDoubleClick()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void CheckBoxSelectAll_Click(object sender, EventArgs e)
        {
            SelectCheckbox();
        }
        private void CheckBoxSelectAll_MouseHover(object sender, EventArgs e)
        {
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(this.CheckBoxSelectAll, "เลือกรายการทั้งหมด");
        }
        private void ComboBoxDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxBarcode.Text = string.Empty;
            TextBoxBarcode.Focus();
        }
        public void RefreshGridView()
        {
            List<inquiryInfoBeanList> _list = new List<inquiryInfoBeanList>();
            if (GlobalClass.LstInfo != null)
            {
                foreach (var Item in GlobalClass.LstInfo)
                {
                    inquiryInfoBeanList models = new inquiryInfoBeanList();
                    models.custId = Item.custId;
                    models.ca = Item.ca;
                    models.ui = Item.ui;
                    models.FirstName = Item.FirstName;
                    models.coAddress = Item.coAddress;
                    models.custType = Item.custType;
                    models.payeeEleName = Item.payeeEleName;
                    models.payeeEleTax20 = Item.payeeEleTax20;
                    models.payeeEleAddress = Item.payeeEleAddress;
                    models.payeeEleTaxBranch = Item.payeeEleTaxBranch;
                    models.lockCheq = Item.lockCheq;
                    if (labelCa.Text == Item.ca)
                    {
                        models.EleTotalAmount = Item.EleTotalAmount;
                        models.EleTotalVat = Item.EleTotalVat;
                        models.Defaultpenalty = Item.Defaultpenalty;
                        models.TotalAmount = Item.TotalAmount;
                        if (!Item.Status)
                            models.SelectCheck = false;
                        else
                            models.SelectCheck = true;
                    }
                    else
                    {
                        models.EleTotalAmount = Item.EleTotalAmount;
                        models.EleTotalVat = Item.EleTotalVat;
                        models.Defaultpenalty = Item.Defaultpenalty;
                        models.EleInvCount = Item.EleInvCount;
                        models.TotalAmount = Item.TotalAmount;
                        if (!Item.Status)
                            models.SelectCheck = false;
                        else
                            models.SelectCheck = true;
                    }
                    models.lockBill = Item.lockBill;
                    models.debtType = Item.debtType;
                    _list.Add(models);
                }
                this.DataGridViewDetail.DataSource = _list;
                CountAmount();
                foreach (DataGridViewRow row in DataGridViewDetail.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["ColckCancel"].Value) == true)
                        row.DefaultCellStyle.BackColor = Color.FromArgb(255, 154, 154);
                }
            }
            else
            {
                this.DataGridViewDetail.DataSource = null;
                this.DataGridViewDetail.Refresh();
                this.GridDetail.DataSource = null;
                this.GridDetail.Refresh();
            }
        }
        public void CountAmount()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                decimal Total = 0;
                decimal Amount = 0;
                decimal TotalVat = 0;
                decimal Fine = 0;
                int countCheck = 0;
                if (GlobalClass.LstInfo != null)
                {
                    foreach (var Item in GlobalClass.LstInfo)
                    {
                        if (Item.SelectCheck)
                        {
                            Amount += Item.EleTotalAmount;
                            TotalVat += Convert.ToDecimal(Item.EleTotalVat);
                            Fine += Convert.ToDecimal(Item.Defaultpenalty);
                            Total += Item.TotalAmount;
                            countCheck++;
                            if (countCheck == DataGridViewDetail.Rows.Count)
                                CheckBoxSelectAll.Checked = true;
                        }
                        else
                        {
                            CheckBoxSelectAll.Checked = false;
                            if (countCheck == 0)
                                countCheck = 0;
                        }
                        if (Item.ca == lblCaDetail.Text)
                            lblAmountDetail.Text = (Item.TotalAmount + Convert.ToDecimal(Item.Defaultpenalty)).ToString("#,###,###,##0.00");
                    }
                }
                TextboxPayList.Text = countCheck.ToString();
                txtAmount.Text = Amount.ToString("#,###,###,##0.00");
                txtVat.Text = TotalVat.ToString("#,###,##0.00");
                txtTotal.Text = (Total).ToString("#,###,###,##0.00");
                txtFine.Text = Fine.ToString("#,###,###,##0.00");
                txtTotalSum.Text = (Total + Fine).ToString("#,###,###,##0.00");
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public string SplitData()
        {
            string strData = "";
            if (_resulQRCode != null && _resulQRCode.Count != 0)
            {
                string data = _resulQRCode[1].payeeFlag.ToString();
                if (data.Length >= 9)
                    strData = data.Substring(0, 9);
                _statusScan = true;
                _resulQRCode = new List<result>();
            }
            return strData;
        }
        public string SplitDataWater(string barcode)
        {
            string strData = "";
            if (barcode.Length <= 59)
                strData = barcode.Substring(19, 8);
            else
                strData = barcode.Substring(20, 8);
            return strData;
        }
        private void TextBoxBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                System.Reflection.MethodBase method;
                method = System.Reflection.MethodBase.GetCurrentMethod();
                try
                {
                    if (ComboBoxDocType.Text == "บัญชีแสดงสัญญา")
                        ComboBoxDocType.SelectedIndex = 0;
                    int select = ComboBoxDocType.SelectedIndex;
                    if (select == 5)
                        GetDataWaterbill();
                    else
                    {
                        if (_statusScan)
                            BarcodeCount = 0;
                        if (BarcodeCount <= 2)
                        {
                            if (_resulQRCode.Count == 2)
                                _resulQRCode = new List<result>();
                            BarcodeCount++;
                            result QrCode = new result();
                            QrCode.payeeFlag = TextBoxBarcode.Text;
                            _resulQRCode.Add(QrCode);
                        }
                        if (BarcodeCount == 2 && _statusScan == false)
                        {
                            string ca = SplitData();
                            TextBoxBarcode.Text = ca;
                            SetProgressBar(true);
                            bool tf = GetDataToGridview(ca, ComboBoxDocType.SelectedIndex);
                            if (!tf)
                            {
                                TextBoxBarcode.Text = string.Empty;
                                return;
                            }
                            ButtonRec.Focus();
                        }
                        else
                        {
                            int length = _resulQRCode[0].payeeFlag.Length;
                            if (ComboBoxDocType.SelectedIndex == 0)
                            {
                                if (_resulQRCode != null && _resulQRCode[0].payeeFlag.Length <= 9)
                                {
                                    string data = _resulQRCode[0].payeeFlag.ToString();
                                    SetProgressBar(true);
                                    bool tf = GetDataToGridview(data, ComboBoxDocType.SelectedIndex);
                                    BarcodeCount = 0;
                                    _statusScan = false;
                                    _resulQRCode = new List<result>();
                                    if (!tf)
                                    {
                                        TextBoxBarcode.Text = string.Empty;
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                if (_resulQRCode != null)
                                {
                                    SetProgressBar(true);
                                    string data = _resulQRCode[0].payeeFlag.ToString();
                                    bool tf = GetDataToGridview(data, ComboBoxDocType.SelectedIndex);
                                    if (!tf)
                                    {
                                        TextBoxBarcode.Text = string.Empty;
                                        return;
                                    }
                                    _resulQRCode = new List<result>();
                                    BarcodeCount = 0;
                                    _statusScan = false;
                                }
                            }
                            TextBoxBarcode.Focus();
                        }
                        TextBoxBarcode.Text = string.Empty;
                        int row = DataGridViewDetail.Rows.Count;
                        if (row > 0)
                        {
                            DataGridViewDetail.Rows[0].Selected = false;
                            DataGridViewDetail.Rows[row - 1].Selected = true;
                            LoadDataDetail(0, 1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    SetProgressBar(false);
                    meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                    MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
        private void ButtonCancelRev_Click(object sender, EventArgs e)
        {

        }
        private void ButtonAdvance_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double payment = 0;
                if (labelCa.Text == "")
                {
                    MessageBox.Show("กรุณาเลือก CA ที่ต้องการรับเงินล่วงหน้า", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (lblAmountDetail.Text == "")
                    payment = 0;// Convert.ToDouble(lblAmountDetail.Text);
                else
                    payment = Convert.ToDouble(lblAmountDetail.Text);
                FormReceiveinadvance advance = new FormReceiveinadvance();
                advance.LabelPayment.Text = payment.ToString("#,###,###,##0.00");
                advance.ca = labelCa.Text;
                advance.resul = resul;
                advance.ShowDialog();
                advance.Dispose();
                resul.Output = GlobalClass.LstInfo;
                LoadData(labelCa.Text);
                RefreshGridView();
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonClear_MouseHover(object sender, EventArgs e)
        {
            ToolTip1.SetToolTip(this.ButtonClear, "เคลียร์ข้อมูล");
        }
        private void ButtonDelete_MouseHover(object sender, EventArgs e)
        {
            ToolTip1.SetToolTip(this.ButtonDelete, "ลบข้อมูล");
        }
        private void ButtonSearch_MouseHover(object sender, EventArgs e)
        {
            ToolTip1.SetToolTip(this.ButtonSearch, "ค้นหาข้อมูล");
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                int i = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (DataGridViewDetail.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        if (newBool)
                        {
                            string ca = DataGridViewDetail.Rows[i].Cells["ColCA"].Value.ToString();
                            var itemToRemove = GlobalClass.LstInfo.SingleOrDefault(r => r.ca == ca);
                            if (itemToRemove != null)
                            {
                                GlobalClass.LstInfo.Remove(itemToRemove);
                                meaLog.WriteData("CancelInterest :: Success :: CA : " + ca + " |  User ID : " + GlobalClass.UserId, method, LogLevel.Debug);
                            }
                        }
                    }
                }
                if (GlobalClass.LstInfo.Count <= 0)
                    ClearData();
                else
                    this.DataGridViewDetail.DataSource = GlobalClass.LstInfo;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                List<inquiryInfoBeanList> lst = new List<inquiryInfoBeanList>();
                if (TextBoxSearch.Text != "")
                    lst = GlobalClass.LstInfo.Where(c => c.ca == TextBoxSearch.Text).ToList();

                if (lst.Count > 0)
                {
                    DataGridViewDetail.DataSource = resul.Output;
                    //DataGridViewDetail.Rows[20].Selected = true;
                }
                else
                    MessageBox.Show("ไม่มีข้อมูลในระบบ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                TextBoxSearch.Text = string.Empty;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void LoadData(string ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                bool status = false;
                ResultCostTDebt resulDebt = new ResultCostTDebt();
                resulDebt = arrears.SelectDataInquiryDebtDetail(resul, ca);
                int countCheck = 0;
                foreach (var item in resulDebt.Output)
                {
                    if (item.directDebitDate != null && item.directDebitDate != "")
                        status = true;
                    DateTime dateTime = DateTime.Now; //Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                    if (item.dueDate != null && item.dueDate != "")
                    {
                        DateTime DateTime = DateTime.Parse(item.dueDate);//, "dd/MM/yyyy", _usCultureTh);
                        if (DateTime <= dateTime)
                            GridDetail.Columns["colPayment"].ReadOnly = false;
                        else
                            GridDetail.Columns["colPayment"].ReadOnly = true;
                    }
                    else
                        GridDetail.Columns["colPayment"].ReadOnly = true;

                    if (item.SelectCheck)
                        countCheck++;
                }
                if (countCheck == resulDebt.Output.Count)
                    CheckBoxSelect.Checked = true;
                else
                    CheckBoxSelect.Checked = false;

                lblStatusD.Visible = status;
                GridDetail.DataSource = resulDebt.Output;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void CheckStatus()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double Amount = 0;
                double TotalVat = 0;
                double TotalAmount = 0;
                double Fine = 0;
                int i = 0;
                int countCheck = 0;
                var loopTo = GridDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        DataGridViewRow rownew = GridDetail.Rows[i];
                        rownew.Cells["ColCheckBoxDetail"].Value = !Convert.ToBoolean(rownew.Cells["ColCheckBoxDetail"].EditedFormattedValue);
                        bool newBool = (bool)rownew.Cells[0].Value;
                        bool originalBool = !newBool;
                        if (originalBool)
                        {
                            Fine += Convert.ToDouble(GridDetail.Rows[i].Cells["colDefaultpenalty"].Value);
                            Amount += Convert.ToDouble(GridDetail.Rows[i].Cells["colAmountDetail"].Value);
                            TotalVat += Convert.ToDouble(GridDetail.Rows[i].Cells["colVatDetail"].Value);
                            TotalAmount += Convert.ToDouble(GridDetail.Rows[i].Cells["colPayment"].Value);
                            GridDetail.Rows[i].Cells[0].Value = true;
                            countCheck++;
                            if (countCheck == GridDetail.Rows.Count)
                                CheckBoxSelectAll.Checked = true;
                        }
                        else
                        {
                            GridDetail.Rows[i].Cells[0].Value = false;
                            CheckBoxSelectAll.Checked = false;
                            if (countCheck == 0)
                                countCheck = 0;
                        }
                    }
                }
                TextboxPayList.Text = countCheck.ToString();
                txtAmount.Text = (TotalAmount - TotalVat).ToString("#,###,###,##0.00");
                txtVat.Text = TotalVat.ToString("#,###,##0.00");
                txtTotal.Text = TotalAmount.ToString("#,###,###,##0.00");
                txtFine.Text = Fine.ToString("#,###,###,##0.00");
                txtTotalSum.Text = (TotalAmount + Fine).ToString("#,###,###,##0.00");
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonClear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        public void SelectCheckboxDetail()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                decimal vat = 0;
                decimal payment = 0;
                double fine = 0;
                int i = 0;
                decimal balance = 0;
                string IdDebt = "";
                var loopTo = GridDetail.Rows.Count - 1;
                bool status = false;
                for (i = 0; i <= loopTo; i++)
                {
                    IdDebt = GridDetail.Rows[i].Cells["ColDebtId"].Value.ToString();
                    if (CheckBoxSelect.Checked == true)
                    {
                        GridDetail.Rows[i].Cells[0].Value = true;
                        if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            if (GridDetail.Rows[i].Cells["colDefaultpenalty"].Value.ToString() != "")
                                fine += Convert.ToDouble(GridDetail.Rows[i].Cells["colDefaultpenalty"].Value);
                            if (GridDetail.Rows[i].Cells["colVatDetail"].Value.ToString() != "")
                                vat += Convert.ToDecimal(GridDetail.Rows[i].Cells["colVatDetail"].Value);
                            if (GridDetail.Rows[i].Cells["colPayment"].Value.ToString() != "")
                            {
                                payment += Convert.ToDecimal(GridDetail.Rows[i].Cells["colPayment"].Value);
                                balance = Convert.ToDecimal(GridDetail.Rows[i].Cells["colPayment"].Value.ToString());
                            }
                        }
                        arrears.UpdateStatus(IdDebt, true, balance, labelCa.Text);
                        status = true;
                    }
                    else
                    {
                        GridDetail.Rows[i].Cells[0].Value = false;
                        arrears.UpdateStatus(IdDebt, false, balance, labelCa.Text);
                        status = false;
                    }
                }
                arrears.UpdateStatusUserInfo(labelCa.Text, status, payment, vat, fine.ToString());
                RefreshGridView();
                lblAmountDetail.Text = payment.ToString("#,###,###,##0.00");
                DataGridViewDetail.Rows[0].Selected = false;
                DataGridViewDetail.Rows[_rowIndex].Selected = true;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void GridDetail_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.TextChanged += new EventHandler(tb_TextChanged);
            cnt = e.Control;
            cnt.TextChanged += tb_TextChanged;
        }
        void tb_TextChanged(object sender, EventArgs e)
        {
            if (cnt.Text != string.Empty)
            {
                var isNumeric = double.TryParse(cnt.Text, out double n);
                if (isNumeric)
                {
                    DataGridViewCell cell = GridDetail.CurrentCell;
                    decimal vat = 0;
                    decimal payment = 0;
                    decimal fine = 0;
                    inquiryGroupDebtBeanList _costTDebt = new inquiryGroupDebtBeanList();
                    DataGridViewCheckBoxCell check = (DataGridViewCheckBoxCell)GridDetail.Rows[cell.RowIndex].Cells["ColCheckBoxDetail"];
                    bool isCheck = (bool)check.EditedFormattedValue;
                    if (isCheck)
                    {
                        string IdDebt = "";
                        IdDebt = GridDetail.Rows[cell.RowIndex].Cells["ColDebtId"].Value.ToString();
                        _costTDebt = arrears.UpdateStatus(IdDebt, true, Convert.ToDecimal(cnt.Text), labelCa.Text);
                        GridDetail.Rows[cell.RowIndex].Cells["coldebtBalance"].Value = cnt.Text;
                        GridDetail.Rows[cell.RowIndex].Cells["colVatDetail"].Value = _costTDebt.vatBalance;
                    }
                    int i = 0;
                    var loopTo = GridDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewRow rownew = GridDetail.Rows[i];
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBoxDetail"];
                        bool isChecked = (bool)checkCell.EditedFormattedValue;
                        if (isChecked)
                        {
                            if (GridDetail.Rows[i].Cells["colDefaultpenalty"].Value.ToString() != "")
                                fine += Convert.ToDecimal(GridDetail.Rows[i].Cells["colDefaultpenalty"].Value);
                            if (GridDetail.Rows[i].Cells["colVatDetail"].Value.ToString() != "")
                                vat += Convert.ToDecimal(GridDetail.Rows[i].Cells["colVatDetail"].Value);
                            if (GridDetail.Rows[i].Cells["colPayment"].Value.ToString() != "")
                                payment += Convert.ToDecimal(GridDetail.Rows[i].Cells["coldebtBalance"].Value);
                        }
                    }
                    arrears.UpdateStatusUserInfo(labelCa.Text, true, payment, vat, fine.ToString());
                    RefreshGridView();
                    txtAmount.Text = payment.ToString("#,###,###,##0.00");
                    lblAmountDetail.Text = (payment + fine).ToString("#,###,###,##0.00");
                    DataGridViewDetail.Rows[0].Selected = false;
                    DataGridViewDetail.Rows[_rowIndex].Selected = true;
                }
                else
                    return;
            }
        }
        public void SumAmount(int status, int rows)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double Amount = 0;
                double Fine = 0;
                if (status == 0)
                {
                    lblCaDetail.Text = DataGridViewDetail.Rows[rows].Cells["ColCA"].Value.ToString();
                    Amount += Convert.ToDouble(DataGridViewDetail.Rows[rows].Cells["ColSumTotalAmount"].Value);
                    Fine += Convert.ToDouble(DataGridViewDetail.Rows[rows].Cells["ColPenaltyAll"].Value);
                }
                else
                {
                    lblCaDetail.Text = DataGridViewDetail.CurrentRow.Cells["ColCA"].Value.ToString();
                    Amount += Convert.ToDouble(DataGridViewDetail.CurrentRow.Cells["ColSumTotalAmount"].Value);
                    Fine += Convert.ToDouble(DataGridViewDetail.CurrentRow.Cells["ColPenaltyAll"].Value);
                }
                lblAmountDetail.Text = (Amount + Fine).ToString("#,###,###,##0.00");
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void SumAmountDetail()
        {
            double payment = 0;
            double fine = 0;
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                DataGridViewRow rownew = GridDetail.Rows[i];
                DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBoxDetail"];
                bool isChecked = (bool)checkCell.EditedFormattedValue;
                if (isChecked)
                {
                    if (GridDetail.Rows[i].Cells["colDefaultpenalty"].Value.ToString() != "")
                        fine += Convert.ToDouble(GridDetail.Rows[i].Cells["colDefaultpenalty"].Value);
                    if (GridDetail.Rows[i].Cells["colPayment"].Value.ToString() != "")
                        payment += Convert.ToDouble(GridDetail.Rows[i].Cells["coldebtBalance"].Value);
                }
            }
            lblAmountDetail.Text = (payment + fine).ToString("#,###,###,##0.00");
        }
        private void GridDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    decimal vat = 0;
                    decimal payment = 0;
                    decimal fine = 0;
                    int i = 0;
                    int countCheck = 0;
                    decimal balance = 0;
                    string IdDebt = "";
                    var loopTo = GridDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            IdDebt = GridDetail.Rows[i].Cells["ColDebtId"].Value.ToString();
                            DataGridViewCell cell = GridDetail.CurrentCell;
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBoxDetail"];
                            bool newBool = (bool)checkCell.EditedFormattedValue;
                            if (newBool)
                            {
                                if (GridDetail.Rows[i].Cells["colDefaultpenalty"].Value.ToString() != "")
                                    fine += Convert.ToDecimal(GridDetail.Rows[i].Cells["colDefaultpenalty"].Value);
                                if (GridDetail.Rows[i].Cells["colVatDetail"].Value.ToString() != "")
                                    vat += Convert.ToDecimal(GridDetail.Rows[i].Cells["colVatDetail"].Value);
                                if (GridDetail.Rows[i].Cells["colPayment"].Value.ToString() != "")
                                {
                                    payment += Convert.ToDecimal(GridDetail.Rows[i].Cells["colPayment"].Value);
                                    balance = Convert.ToDecimal(GridDetail.Rows[i].Cells["colPayment"].Value.ToString());
                                }

                                GridDetail.Rows[i].Cells[0].Value = true;
                                countCheck++;
                                if (countCheck == GridDetail.Rows.Count)
                                    CheckBoxSelect.Checked = true;
                                arrears.UpdateStatus(IdDebt, true, balance, labelCa.Text);
                            }
                            else
                            {
                                GridDetail.Rows[i].Cells[0].Value = false;
                                CheckBoxSelect.Checked = false;
                                arrears.UpdateStatus(IdDebt, false, 0, labelCa.Text);
                            }
                        }
                    }
                    if (countCheck <= 0)
                        arrears.UpdateStatusUserInfo(labelCa.Text, false, 0, vat, fine.ToString());
                    else
                        arrears.UpdateStatusUserInfo(labelCa.Text, true, payment, vat, fine.ToString());
                    RefreshGridView();
                    lblAmountDetail.Text = (payment).ToString("#,###,###,##0.00");
                    DataGridViewDetail.Rows[0].Selected = false;
                    DataGridViewDetail.Rows[_rowIndex].Selected = true;
                }
                if (e.ColumnIndex == 12)
                {
                    string IdDebt = GridDetail.Rows[e.RowIndex].Cells["ColDebtId"].Value.ToString();
                    bool isChecked = (bool)GridDetail[e.ColumnIndex, e.RowIndex].EditedFormattedValue;
                    if (isChecked)
                    {
                        double penalty = Convert.ToDouble(GridDetail.Rows[e.RowIndex].Cells["colDefaultpenalty"].Value.ToString());
                        if (penalty > 0)
                        {
                            if (MessageBox.Show("คุณต้องการยกเลิกเบี้ยปรับ", "ยกเลิก", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                                return;
                            FormCheckUser checkUser = new FormCheckUser();
                            checkUser.ShowDialog();
                            checkUser.Dispose();
                            if (GlobalClass.StatusForm == 1)
                            {
                                ElectricityPenalty(IdDebt, isChecked);
                                arrears.UpdateStatusDefaultpenalty(true, IdDebt, lblCaDetail.Text);
                                LoadData(labelCa.Text);
                                RefreshGridView();
                                GlobalClass.StatusForm = null;
                            }
                        }
                    }
                    else
                    {
                        DeleteElectricityPenalty(IdDebt);
                        arrears.UpdateStatusDefaultpenalty(false, IdDebt, lblCaDetail.Text);
                        LoadData(labelCa.Text);
                        RefreshGridView();
                    }
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void ElectricityPenalty(string idDebt, bool status)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ResultCancelElectricity result = new ResultCancelElectricity();
                result.Cancel = new List<ClassElectricityPenalty>();
                if (GlobalClass.CancelElectricity != null && GlobalClass.CancelElectricity.Count != 0)
                {
                    foreach (var item in GlobalClass.CancelElectricity)
                    {
                        ClassElectricityPenalty Cancel = new ClassElectricityPenalty();
                        Cancel.ca = item.ca;
                        Cancel.userId = item.userId;
                        Cancel.debtId = item.debtId;
                        Cancel.StatusDefaultpenalty = item.StatusDefaultpenalty;
                        Cancel.approveEmpId = item.approveEmpId;
                        result.Cancel.Add(Cancel);
                    }
                    ClassElectricityPenalty CancelDetail = new ClassElectricityPenalty();
                    CancelDetail.ca = labelCa.Text;
                    CancelDetail.userId = GlobalClass.UserId;
                    CancelDetail.debtId = idDebt;
                    CancelDetail.approveEmpId = GlobalClass.UserId;
                    CancelDetail.StatusDefaultpenalty = status;
                    result.Cancel.Add(CancelDetail);
                }
                else
                {
                    ClassElectricityPenalty Cancel = new ClassElectricityPenalty();
                    Cancel.ca = labelCa.Text;
                    Cancel.userId = GlobalClass.UserId;
                    Cancel.debtId = idDebt;
                    Cancel.StatusDefaultpenalty = status;
                    Cancel.approveEmpId = GlobalClass.UserId;
                    result.Cancel.Add(Cancel);
                }
                GlobalClass.CancelElectricity = result.Cancel;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public bool DeleteElectricityPenalty(string idDebt)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                ClassElectricityPenalty resulRemove = GlobalClass.CancelElectricity.Where(c => c.debtId == idDebt).FirstOrDefault();
                if (resulRemove != null)
                {
                    GlobalClass.CancelElectricity.Remove(resulRemove);
                    tf = true;
                }
            }
            catch (Exception ex)
            {
                tf = false;
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return tf;
        }
        private void CheckBoxSelect_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                SelectCheckboxDetail();
                RefreshGridView();
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void comboBoxDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDetail.SelectedIndex == 1)
            {
                txtCount.Enabled = true;
                btOK.Enabled = true;
            }
            else if (comboBoxDetail.SelectedIndex == 2)
            {
                txtCount.Enabled = false;
                btOK.Enabled = true;
            }
            else
            {
                txtCount.Enabled = false;
                btOK.Enabled = false;
            }
        }
        private void btOK_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                List<inquiryInfoBeanList> lstInfo = new List<inquiryInfoBeanList>();
                int i = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    inquiryInfoBeanList info = new inquiryInfoBeanList();
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                    {
                        info.ca = DataGridViewDetail.Rows[i].Cells["ColCA"].Value.ToString();
                        lstInfo.Add(info);
                    }
                }
                arrears.UpdateCostTDebt(lstInfo, comboBoxDetail.SelectedIndex, txtCount.Text);
                int row = DataGridViewDetail.Rows.Count;
                DataGridViewDetail.Rows[0].Selected = true;
                RefreshGridView();
                LoadDataDetail(0, 0);
                txtCount.Text = string.Empty;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void LoadDataDetail(int status, int rows)
        {
            string Custtype;
            if (status == 0)
            {
                int row;
                if (rows == 0)
                    row = 0;
                else
                    row = DataGridViewDetail.Rows.Count - 1;
                rows = row;
                labelFk.Text = (DataGridViewDetail.Rows[row].Cells["ColCustomerName"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColCustomerName"].Value.ToString() : "";
                labelCa.Text = DataGridViewDetail.Rows[row].Cells["ColCA"].Value.ToString();
                labelAddress.Text = (DataGridViewDetail.Rows[row].Cells["ColAddress"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColAddress"].Value.ToString() : "";
                labelUi.Text = (DataGridViewDetail.Rows[row].Cells["ColUI"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColUI"].Value.ToString() : "";
                labelBranch.Text = (DataGridViewDetail.Rows[row].Cells["ColTaxBranch"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColTaxBranch"].Value.ToString() : "";
                labelTaxId.Text = (DataGridViewDetail.Rows[row].Cells["ColTax20"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColTax20"].Value.ToString() : "";
                Custtype = (DataGridViewDetail.Rows[row].Cells["ColType"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColType"].Value.ToString() : "";
                labelBill.Text = (DataGridViewDetail.Rows[row].Cells["collId"].Value != null) ? DataGridViewDetail.Rows[row].Cells["collId"].Value.ToString() : "";
                lblReceiptName.Text = (DataGridViewDetail.Rows[row].Cells["ColCusName"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColCusName"].Value.ToString() : "";
                if (DataGridViewDetail.Rows[row].Cells["colStatus"].Value.ToString() != "")
                    lblStatusC.Visible = true;
                else
                    lblStatusC.Visible = false;
            }
            else
            {
                labelFk.Text = DataGridViewDetail.CurrentRow.Cells["ColCustomerName"].Value.ToString();
                labelCa.Text = DataGridViewDetail.CurrentRow.Cells["ColCA"].Value.ToString();
                labelAddress.Text = DataGridViewDetail.CurrentRow.Cells["ColAddress"].Value.ToString();
                labelUi.Text = (DataGridViewDetail.CurrentRow.Cells["ColUI"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColUI"].Value.ToString() : "";
                labelBranch.Text = (DataGridViewDetail.CurrentRow.Cells["ColTaxBranch"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColTaxBranch"].Value.ToString() : "";
                labelTaxId.Text = (DataGridViewDetail.CurrentRow.Cells["ColTax20"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColTax20"].Value.ToString() : "";
                Custtype = (DataGridViewDetail.CurrentRow.Cells["ColType"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColType"].Value.ToString() : "";
                labelBill.Text = (DataGridViewDetail.CurrentRow.Cells["collId"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["collId"].Value.ToString() : "";
                lblReceiptName.Text = (DataGridViewDetail.CurrentRow.Cells["ColCusName"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColCusName"].Value.ToString() : "";
                if (DataGridViewDetail.CurrentRow.Cells["colStatus"].Value.ToString() != "")
                    lblStatusC.Visible = true;
                else
                    lblStatusC.Visible = false;
            }
            _custtype = Custtype;
            LoadData(labelCa.Text);
            SumAmount(status, rows);
            SetProgressBar(false);
        }
        private void txtCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void DataGridViewDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.ColumnIndex == 1)
                    LoadDataDetail(1, 0);
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormArrearsNew | Function :: DataGridViewDetail_CellContentDoubleClick()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void buttonZ1_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (MessageBox.Show("คุณต้องการยกเลิกค่าดำเนินการงดจ่ายไฟ", "ยกเลิก", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                FormCheckUser checkUser = new FormCheckUser();
                checkUser.ShowDialog();
                checkUser.Dispose();
                if (GlobalClass.StatusForm == 1)
                {
                    if (GlobalClass.AuthorizeLevel == 2)
                    {
                        string IdDebt = "";
                        int i = 0;
                        var loopTo = GridDetail.Rows.Count - 1;
                        for (i = 0; i <= loopTo; i++)
                        {
                            if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                            {
                                string DebtType = GridDetail.Rows[i].Cells["ColdebtType"].Value.ToString();
                                IdDebt = GridDetail.Rows[i].Cells["ColDebtId"].Value.ToString();
                                DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBoxDetail"];
                                bool newBool = (bool)checkCell.EditedFormattedValue;
                                if (newBool && DebtType == "DIS")
                                {
                                    ResultClassCancelProcessing result = new ResultClassCancelProcessing();
                                    result.Cancel = new List<ClassCancelProcessingFee>();
                                    if (GlobalClass.CancelProcessing != null)
                                    {
                                        foreach (var item in GlobalClass.Cancel)
                                        {
                                            ClassCancelProcessingFee Cancel = new ClassCancelProcessingFee();
                                            Cancel.debtId = item.debtId;
                                            Cancel.userId = item.userId;
                                            result.Cancel.Add(Cancel);
                                        }
                                        ClassCancelProcessingFee CancelDetail = new ClassCancelProcessingFee();
                                        CancelDetail.userId = GlobalClass.UserId;
                                        CancelDetail.debtId = IdDebt;
                                        result.Cancel.Add(CancelDetail);
                                    }
                                    else
                                    {
                                        ClassCancelProcessingFee Cancel = new ClassCancelProcessingFee();
                                        Cancel.userId = GlobalClass.UserId;
                                        Cancel.debtId = IdDebt;
                                        result.Cancel.Add(Cancel);
                                    }
                                    GlobalClass.CancelProcessing = result.Cancel;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void GetDataWaterbill()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                #region
                string TaxId, CheckType, CusCode = "", DueDate, BillNumber, Unit, AmountBill, mwaTaxId, customerCode, billDueDate, billNumber, petVat;
                string BarCode = TextBoxBarcode.Text;
                if (BarCode.Length == 59)
                    CheckType = BarCode.Substring(16, 1);
                else
                    CheckType = BarCode.Substring(17, 1);

                if (CheckType != "7")
                {
                    TaxId = BarCode.Substring(1, 13);
                    mwaTaxId = BarCode.Substring(1, 15);
                    if (BarCode.Length == 59)
                    {
                        customerCode = BarCode.Substring(16, 18);
                        billDueDate = BarCode.Substring(34, 15);
                        billNumber = BarCode.Substring(49, 10);

                        CusCode = BarCode.Substring(18, 9);
                        DueDate = BarCode.Substring(34, 6);
                        BillNumber = BarCode.Substring(27, 7);
                        petVat = BarCode.Substring(40, 2);
                        Unit = BarCode.Substring(42, 7);
                        AmountBill = BarCode.Substring(49, 10);
                    }
                    else
                    {
                        customerCode = BarCode.Substring(17, 18);
                        billDueDate = BarCode.Substring(36, 15);
                        billNumber = BarCode.Substring(52, 10);

                        CusCode = BarCode.Substring(19, 9);
                        DueDate = BarCode.Substring(36, 6);
                        BillNumber = BarCode.Substring(28, 7);
                        petVat = BarCode.Substring(42, 2);
                        Unit = BarCode.Substring(44, 7);
                        AmountBill = BarCode.Substring(52, 10);

                    }
                    string perVat;
                    if (Convert.ToInt16(petVat) >= 10)
                        perVat = "1." + petVat;
                    else
                        perVat = "1.0" + petVat;
                    decimal vat = Convert.ToDecimal(AmountBill) - (Convert.ToDecimal(AmountBill) / Convert.ToDecimal(perVat));

                    ClassItemWater wt = new ClassItemWater();
                    wt.customerName = CusCode;
                    wt.taxId = TaxId;
                    wt.debtIdSet = new Int64[1] { 1 };
                    wt.Amount = AmountBill;
                    wt.vatType = petVat;
                    wt.VatAmount = vat;
                    wt.totalAmount = Convert.ToDecimal(AmountBill) + vat;
                    wt.meaUnit = Unit;
                    wt.mwaTaxId = mwaTaxId;
                    wt.customerCode = customerCode;
                    wt.billDueDate = billDueDate;
                    wt.billNumber = billNumber;
                    wt.check = CheckType;
                    wt.dueDate = DueDate;
                    resul = arrears.addDataWater(wt);
                }
                else
                    MessageBox.Show("ไม่สามารถทำการรับชำระได้ เนื่องจาก ยังมีหนี้เก่าค้างชำระ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                #endregion
                ArrearsController arr = new ArrearsController();
                TextBoxBarcode.Text = CusCode;

                if (resul.Output != null && resul.Output.Count != 0)
                    TextboxTotalList.Text = resul.Output.Count.ToString();

                SetControl();
                this.DataGridViewDetail.DataSource = resul.Output;

                double Amount = 0;
                double TotalVat = 0;
                double Fine = 0;
                int i = 0;
                int countCheck = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;

                for (i = 0; i <= loopTo; i++)
                {
                    if (Convert.ToBoolean(DataGridViewDetail.Rows[i].Cells["ColckCancel"].Value) == true)
                        DataGridViewDetail.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 154, 154);
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                    {
                        Amount += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColTotalAmount"].Value);
                        TotalVat += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColVat"].Value);
                        Fine += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColPenaltyAll"].Value);
                        DataGridViewDetail.Rows[i].Cells[0].Value = true;
                        countCheck++;
                    }
                    else
                        DataGridViewDetail.Rows[i].Cells[0].Value = false;
                }

                if (countCheck == DataGridViewDetail.Rows.Count)
                    CheckBoxSelectAll.Checked = true;
                else
                    CheckBoxSelectAll.Checked = false;

                TextboxPayList.Text = countCheck.ToString();
                txtAmount.Text = (Amount - TotalVat).ToString("#,###,###,##0.00");
                txtVat.Text = TotalVat.ToString("#,###,##0.00");
                txtTotal.Text = Amount.ToString("#,###,###,##0.00");
                txtFine.Text = Fine.ToString("#,###,###,##0.00");
                txtTotalSum.Text = (Amount + Fine).ToString("#,###,###,##0.00");

                ButtonRec.Focus();
                int row = DataGridViewDetail.Rows.Count;
                DataGridViewDetail.Rows[0].Selected = false;
                DataGridViewDetail.Rows[row - 1].Selected = true;
                LoadDataDetail(0, 1);
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormArrearsNew | Function :: DataGridViewDetail_CellContentDoubleClick()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }

    }
}
