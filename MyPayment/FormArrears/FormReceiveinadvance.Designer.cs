﻿namespace MyPayment.FormArrears
{
    partial class FormReceiveinadvance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReceiveinadvance));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelTotalAmount = new System.Windows.Forms.Label();
            this.LabelPayment = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextboxAmount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonOk = new Custom_Controls_in_CS.ButtonZ();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelTotalAmount);
            this.groupBox1.Controls.Add(this.LabelPayment);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TextboxAmount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(485, 181);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // labelTotalAmount
            // 
            this.labelTotalAmount.BackColor = System.Drawing.Color.White;
            this.labelTotalAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalAmount.Location = new System.Drawing.Point(188, 120);
            this.labelTotalAmount.Name = "labelTotalAmount";
            this.labelTotalAmount.Size = new System.Drawing.Size(270, 37);
            this.labelTotalAmount.TabIndex = 1000000019;
            // 
            // LabelPayment
            // 
            this.LabelPayment.BackColor = System.Drawing.Color.White;
            this.LabelPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPayment.Location = new System.Drawing.Point(188, 69);
            this.LabelPayment.Name = "LabelPayment";
            this.LabelPayment.Size = new System.Drawing.Size(270, 37);
            this.LabelPayment.TabIndex = 1000000018;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 129);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 28);
            this.label3.TabIndex = 5;
            this.label3.Text = "จำนวนเงินชำระล่วงหน้า";
            // 
            // TextboxAmount
            // 
            this.TextboxAmount.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.TextboxAmount.Location = new System.Drawing.Point(188, 18);
            this.TextboxAmount.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAmount.Name = "TextboxAmount";
            this.TextboxAmount.Size = new System.Drawing.Size(270, 37);
            this.TextboxAmount.TabIndex = 0;
            this.TextboxAmount.TextChanged += new System.EventHandler(this.TextboxAmount_TextChanged);
            this.TextboxAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxAmount_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(58, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 28);
            this.label1.TabIndex = 3;
            this.label1.Text = "จำนวนเงินรับชำระ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 76);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 28);
            this.label2.TabIndex = 4;
            this.label2.Text = "จำนวนหนี้ที่ต้องชำระ";
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close1;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(343, 200);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(127, 40);
            this.ButtonCancel.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCancel.TabIndex = 15;
            this.ButtonCancel.TextLocation_X = 45;
            this.ButtonCancel.TextLocation_Y = 5;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // ButtonOk
            // 
            this.ButtonOk.BackColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderWidth = 1;
            this.ButtonOk.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonOk.ButtonText = "ตกลง";
            this.ButtonOk.CausesValidation = false;
            this.ButtonOk.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonOk.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonOk.ForeColor = System.Drawing.Color.Black;
            this.ButtonOk.GradientAngle = 90;
            this.ButtonOk.Image = global::MyPayment.Properties.Resources.ok;
            this.ButtonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonOk.Location = new System.Drawing.Point(200, 200);
            this.ButtonOk.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonOk.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonOk.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonOk.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonOk.Name = "ButtonOk";
            this.ButtonOk.ShowButtontext = true;
            this.ButtonOk.Size = new System.Drawing.Size(127, 40);
            this.ButtonOk.StartColor = System.Drawing.Color.LightGray;
            this.ButtonOk.TabIndex = 14;
            this.ButtonOk.TextLocation_X = 45;
            this.ButtonOk.TextLocation_Y = 5;
            this.ButtonOk.Transparent1 = 80;
            this.ButtonOk.Transparent2 = 120;
            this.ButtonOk.UseVisualStyleBackColor = true;
            this.ButtonOk.Click += new System.EventHandler(this.ButtonOk_Click);
            // 
            // FormReceiveinadvance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(507, 248);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonOk);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormReceiveinadvance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รับเงินล่วงหน้า";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        private Custom_Controls_in_CS.ButtonZ ButtonOk;
        private System.Windows.Forms.TextBox TextboxAmount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label LabelPayment;
        internal System.Windows.Forms.Label labelTotalAmount;
    }
}