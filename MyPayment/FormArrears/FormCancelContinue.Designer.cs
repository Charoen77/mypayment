﻿namespace MyPayment.FormArrears
{
    partial class FormCancelContinue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCancelContinue));
            this.DataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.CheckBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ColCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColCusName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCusId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDebtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonOk = new Custom_Controls_in_CS.ButtonZ();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridViewDetail
            // 
            this.DataGridViewDetail.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.DataGridViewDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewDetail.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCheckBox,
            this.ColCusName,
            this.ColCusId,
            this.ColCa,
            this.ColUi,
            this.ColDebtId});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.DefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridViewDetail.Location = new System.Drawing.Point(0, 14);
            this.DataGridViewDetail.Name = "DataGridViewDetail";
            this.DataGridViewDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DataGridViewDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewDetail.Size = new System.Drawing.Size(909, 322);
            this.DataGridViewDetail.TabIndex = 3;
            this.DataGridViewDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewDetail_CellContentClick);
            // 
            // CheckBoxSelectAll
            // 
            this.CheckBoxSelectAll.AutoSize = true;
            this.CheckBoxSelectAll.Location = new System.Drawing.Point(49, 26);
            this.CheckBoxSelectAll.Name = "CheckBoxSelectAll";
            this.CheckBoxSelectAll.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelectAll.TabIndex = 4;
            this.CheckBoxSelectAll.UseVisualStyleBackColor = true;
            this.CheckBoxSelectAll.Click += new System.EventHandler(this.CheckBoxSelectAll_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(909, 14);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ButtonCancel);
            this.panel2.Controls.Add(this.ButtonOk);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 336);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(909, 52);
            this.panel2.TabIndex = 16;
            // 
            // ColCheckBox
            // 
            this.ColCheckBox.DataPropertyName = "SelectCheck";
            this.ColCheckBox.Frozen = true;
            this.ColCheckBox.HeaderText = "";
            this.ColCheckBox.Name = "ColCheckBox";
            this.ColCheckBox.Width = 30;
            // 
            // ColCusName
            // 
            this.ColCusName.DataPropertyName = "cusName";
            this.ColCusName.HeaderText = "ชื่อลูกค้า";
            this.ColCusName.Name = "ColCusName";
            this.ColCusName.Width = 200;
            // 
            // ColCusId
            // 
            this.ColCusId.DataPropertyName = "custId";
            this.ColCusId.HeaderText = "รหัสลูกค้า";
            this.ColCusId.Name = "ColCusId";
            this.ColCusId.Width = 220;
            // 
            // ColCa
            // 
            this.ColCa.DataPropertyName = "ca";
            this.ColCa.HeaderText = "บัญชีแสดงสัญญา";
            this.ColCa.Name = "ColCa";
            this.ColCa.Width = 210;
            // 
            // ColUi
            // 
            this.ColUi.DataPropertyName = "ui";
            this.ColUi.HeaderText = "รหัสเครื่องวัดฯ";
            this.ColUi.Name = "ColUi";
            this.ColUi.Width = 200;
            // 
            // ColDebtId
            // 
            this.ColDebtId.DataPropertyName = "debtId";
            this.ColDebtId.HeaderText = "Debt ID";
            this.ColDebtId.Name = "ColDebtId";
            this.ColDebtId.Visible = false;
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close1;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(779, 6);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(127, 40);
            this.ButtonCancel.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCancel.TabIndex = 3;
            this.ButtonCancel.TextLocation_X = 45;
            this.ButtonCancel.TextLocation_Y = 5;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // ButtonOk
            // 
            this.ButtonOk.BackColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderWidth = 1;
            this.ButtonOk.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonOk.ButtonText = "ตกลง";
            this.ButtonOk.CausesValidation = false;
            this.ButtonOk.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonOk.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonOk.ForeColor = System.Drawing.Color.Black;
            this.ButtonOk.GradientAngle = 90;
            this.ButtonOk.Image = global::MyPayment.Properties.Resources.ok;
            this.ButtonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonOk.Location = new System.Drawing.Point(636, 6);
            this.ButtonOk.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonOk.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonOk.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonOk.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonOk.Name = "ButtonOk";
            this.ButtonOk.ShowButtontext = true;
            this.ButtonOk.Size = new System.Drawing.Size(127, 40);
            this.ButtonOk.StartColor = System.Drawing.Color.LightGray;
            this.ButtonOk.TabIndex = 2;
            this.ButtonOk.TextLocation_X = 45;
            this.ButtonOk.TextLocation_Y = 5;
            this.ButtonOk.Transparent1 = 80;
            this.ButtonOk.Transparent2 = 120;
            this.ButtonOk.UseVisualStyleBackColor = true;
            this.ButtonOk.Click += new System.EventHandler(this.ButtonOk_Click);
            // 
            // FormCancelContinue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 392);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.CheckBoxSelectAll);
            this.Controls.Add(this.DataGridViewDetail);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormCancelContinue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ยกเลิกค่างดจ่ายไฟฟ้า";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        private Custom_Controls_in_CS.ButtonZ ButtonOk;
        private System.Windows.Forms.DataGridView DataGridViewDetail;
        private System.Windows.Forms.CheckBox CheckBoxSelectAll;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCusName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCusId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUi;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDebtId;
    }
}