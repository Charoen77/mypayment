﻿using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormMapCheque : Form
    {
        public CultureInfo usCulture = new CultureInfo("en-US");
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        Control cnt;
        public FormMapCheque()
        {
            InitializeComponent();
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        public void SetControl()
        {
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                bool status = false;
                int i = 0;
                var loopTo = GridDetail.Rows.Count - 1;
                List<ClassPaymentCheque> _payment = new List<ClassPaymentCheque>();
                if (GlobalClass.LstSub != null)
                {
                    foreach (var item in GlobalClass.LstSub)
                    {
                        ClassPaymentCheque payment = new ClassPaymentCheque();
                        payment.Ca = item.Ca;
                        payment.DebtId = item.DebtId;
                        payment.ChequeNo = item.ChequeNo;
                        payment.AmountBalance = item.AmountBalance;
                        payment.Amount = item.Amount;
                        payment.Payed = item.Payed;
                        payment.StatusCheque = true;
                        _payment.Add(payment);
                    }
                    GlobalClass.LstSub = _payment;
                }
                for (i = 0; i <= loopTo; i++)
                {
                    DataGridViewRow rownew = GridDetail.Rows[i];
                    string ChequeNo = labelNumber.Text.Trim();
                    string debtId = rownew.Cells["ColdebtId"].EditedFormattedValue.ToString();
                    string amount = rownew.Cells["ColAmount"].EditedFormattedValue.ToString();
                    string payed = rownew.Cells["ColPayed"].EditedFormattedValue.ToString();
                    //string payedBalance = rownew.Cells["ColPaymentBalance"].EditedFormattedValue.ToString();                    
                    string balance = rownew.Cells["ColBalance"].EditedFormattedValue.ToString();
                    string ca = rownew.Cells["ColCa"].EditedFormattedValue.ToString();
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckbox"];
                    bool isChecked = (bool)checkCell.EditedFormattedValue;

                    //inquiryDebtBeanList carMovement = GlobalClass.LstCost.Find(item => item.debtIdSet.Equals(debtId, StringComparison.CurrentCultureIgnoreCase));
                    if (isChecked)
                    {
                        if (GlobalClass.LstSub != null)
                        {
                            ClassPaymentCheque subPayment = GlobalClass.LstSub.Find(item => item.DebtId.Equals(debtId, StringComparison.CurrentCultureIgnoreCase) && item.ChequeNo.Equals(ChequeNo, StringComparison.CurrentCultureIgnoreCase));
                            if (subPayment != null)
                            {
                                subPayment.ChequeNo = labelNumber.Text;
                                subPayment.AmountBalance = subPayment.AmountBalance - Convert.ToDecimal(payed);
                                subPayment.StatusCheque = true;
                            }
                            else
                            {
                                ClassPaymentCheque payment = new ClassPaymentCheque();
                                payment.Ca = ca;
                                payment.DebtId = debtId;
                                payment.ChequeNo = labelNumber.Text;
                                payment.AmountBalance = Convert.ToDecimal(amount) - Convert.ToDecimal(payed);
                                payment.Amount = Convert.ToDecimal(amount);
                                payment.Payed = Convert.ToDecimal(payed);
                                payment.StatusCheque = true;
                                _payment.Add(payment);
                            }
                        }
                        else
                        {
                            ClassPaymentCheque payment = new ClassPaymentCheque();
                            payment.Ca = ca;
                            payment.DebtId = debtId;
                            payment.ChequeNo = labelNumber.Text;
                            payment.AmountBalance = Convert.ToDecimal(amount) - Convert.ToDecimal(payed);
                            payment.Amount = Convert.ToDecimal(amount);
                            payment.Payed = Convert.ToDecimal(payed);
                            payment.StatusCheque = true;
                            _payment.Add(payment);
                        }

                        inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(item => item.ca.Equals(ca, StringComparison.CurrentCultureIgnoreCase));
                        if (userInfo != null)
                            userInfo.StatusOrderBy = true;

                        //if (carMovement != null)
                        //    carMovement.StatusOrderBy = true;
                        //if (carMovement != null)
                        //{
                        //    if (payed != "")
                        //    {
                        //        carMovement.Payed = Convert.ToDouble(debtId);
                        //        carMovement.ChequeNo = labelNumber.Text;
                        //        if (payed == amount)
                        //            carMovement.StatusCheq = true;
                        //        else
                        //            carMovement.StatusCheq = false;
                        //        status = true;
                        //    }
                        //    if (carMovement.AmountBalance == "0")
                        //        carMovement.AmountBalance = (Convert.ToDouble(amount) - Convert.ToDouble(payed)).ToString("#,###,###,##0.00");
                        //    else
                        //        carMovement.AmountBalance = (Convert.ToDouble(carMovement.AmountBalance) - Convert.ToDouble(payed)).ToString("#,###,###,##0.00");
                        //}
                    }      
                }
                GlobalClass.LstSub = _payment;
                ClassCheque cheque = GlobalClass.LstCheque.Find(item => item.Chno.Equals(labelNumber.Text.ToString(), StringComparison.CurrentCultureIgnoreCase));
                if (cheque != null)
                {
                    cheque.Balance = Convert.ToDecimal(labelBalance.Text);
                    cheque.Status = status;
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormMapCheque | Function :: ButtonConfirm()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
            this.Close();
        }
        public void LoadData()
        {
            List<inquiryGroupDebtBeanList> lst = GlobalClass.LstCost.Where(c => c.status == true).OrderBy(g => g.ca).ToList();
            List<inquiryGroupDebtBeanList> GetCheqeNo = lst.Where(c => c.ChequeNo == labelNumber.Text).OrderBy(g => g.ca).ToList();
            if (GetCheqeNo.Count > 0)
                lst = lst.Where(c => c.ChequeNo == labelNumber.Text || c.StatusCheq == false).OrderBy(g => g.ca).ToList();
            else
                lst = lst.Where(c => c.StatusCheq == false && c.AmountBalance != "0.00").OrderBy(g => g.ca).ToList();
            List<inquiryGroupDebtBeanList> _lst = new List<inquiryGroupDebtBeanList>();
            decimal sumPayed = 0;
            foreach (var item in lst)
            {
                inquiryGroupDebtBeanList cost = new inquiryGroupDebtBeanList();
                cost.status = item.StatusCheq;
                cost.debtIdSet = item.debtIdSet;
                cost.ca = item.ca;
                cost.ui = item.ui;
                cost.reqNo = item.reqNo;
                cost.debtType = item.debtType;
                cost.debtBalance = item.debtBalance;           
                cost.Defaultpenalty = item.Defaultpenalty;
                cost.Payed = item.debtBalance;//Convert.ToDouble(item.debtBalance);
                cost.AmountBalance = Convert.ToDouble(item.debtBalance).ToString("#,###,###,##0.00");

                if (GlobalClass.LstSub != null)
                {
                    var lstSub = GlobalClass.LstSub.Where(c => c.DebtId == item.debtIdSet.ToString()).FirstOrDefault();
                    if (lstSub != null)
                    {
                        cost.AmountBalance = Convert.ToDouble(item.AmountBalance).ToString("#,###,###,##0.00");
                        cost.Payed = Convert.ToDecimal(item.AmountBalance);
                        if (GetCheqeNo.Count > 0)
                        {
                            cost.Payed = lstSub.Payed;
                            sumPayed += lstSub.Payed;
                        }
                    }
                }
                _lst.Add(cost);
            }
            sumPayed = Convert.ToDecimal(labelAmount.Text) - sumPayed;
            labelBalance.Text = sumPayed.ToString("#,###,###,##0.00");
            GridDetail.DataSource = _lst;
        }
        private void FormMapCheque_Load(object sender, EventArgs e)
        {
            SetControl();
            LoadData();
        }
        private void GridDetail_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.TextChanged += new EventHandler(tb_TextChanged);
            cnt = e.Control;
            cnt.TextChanged += tb_TextChanged;
        }
        void tb_TextChanged(object sender, EventArgs e)
        {
            if (cnt.Text != string.Empty) 
            {
                var isNumeric = int.TryParse(cnt.Text, out int n);
                if (isNumeric)
                {
                    int i = 0;
                    double balance = 0;
                    var loopTo = GridDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewRow rownew = GridDetail.Rows[i];
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBox"];
                        bool isChecked = (bool)checkCell.EditedFormattedValue;
                        if (isChecked)
                        {
                            if (Convert.ToDouble(rownew.Cells["ColPayed"].EditedFormattedValue.ToString()) > Convert.ToDouble(rownew.Cells["ColBalance"].EditedFormattedValue.ToString()) &&
                                Convert.ToDouble(rownew.Cells["ColAmount"].EditedFormattedValue.ToString()) != Convert.ToDouble(rownew.Cells["ColPayed"].EditedFormattedValue.ToString()))
                            {
                                MessageBox.Show("ยอดเงินเกินจำนวน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                cnt.Text = string.Empty;
                                cnt.Focus();
                                return;
                            }
                            if (rownew.Cells["ColPayed"].EditedFormattedValue.ToString() != "")
                                balance += Convert.ToDouble(rownew.Cells["ColPayed"].EditedFormattedValue);
                        }
                    }
                    double CountAmount = Convert.ToDouble(labelAmount.Text) - balance;
                    if (CountAmount < 0)
                    {
                        MessageBox.Show("ยอดเงินเกินจำนวนยอดเงินคงเหลือ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        cnt.Text = string.Empty;
                        cnt.Focus();
                        return;
                    }
                    else
                        labelBalance.Text = (CountAmount).ToString("#,###,###,##0.00");
                }
                else
                    return;
            }
        }
        private void GridDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                int i = 0;
                var loopTo = GridDetail.Rows.Count - 1;
                double balance = 0;
                for (i = 0; i <= loopTo; i++)
                {
                    if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBox"];
                        DataGridViewTextBoxCell txtAmount = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["ColPayed"];
                        bool isChecked = (bool)checkCell.EditedFormattedValue;
                        if (isChecked)
                        {
                            if (GridDetail.Rows[i].Cells["ColPayed"].EditedFormattedValue.ToString() != "")
                                balance += Convert.ToDouble(GridDetail.Rows[i].Cells["ColPayed"].EditedFormattedValue);

                            double CountAmount = Convert.ToDouble(labelAmount.Text) - balance;
                            labelBalance.Text = (CountAmount).ToString("#,###,###,##0.00");
                            txtAmount.ReadOnly = false;
                        }
                        else
                            txtAmount.ReadOnly = true;
                    }
                }
            }
        }
    }
}
