﻿using MetroFramework.Properties;
using MyPayment.Controllers;
using MyPayment.FormArrears;
using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment
{
    public partial class Main : Form
    {
        Controllers.FormControl formControl = new Controllers.FormControl();
        FormArrears.FormArrears arrears = new FormArrears.FormArrears();
        System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
        ArrearsController arr = new ArrearsController();
        ClassLogsService meaLog = new ClassLogsService("Logout", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("Logout", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("Logout", "ErrorLog");
        public CultureInfo _usCultureTh = new CultureInfo("th-TH");
        public Main()
        {
            InitializeComponent();
        }
        private Form Find(string name)
        {
            CloseAllChildForm();
            return (from c in this.MdiChildren
                    select c).FirstOrDefault();
        }
        private void CloseAllChildForm()
        {
            foreach (Form f in this.MdiChildren)
            {
                f.Close();
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            arrears.MdiParent = this;
            MenuItemPayment_Click(sender, e);           
            SetDataPanel();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
        }
        private void SetDataPanel()
        {
            if (GlobalClass.IsConnectOnline == true)
            {
                MenuItemReport1.Visible = false;
                MenuItemReport2.Visible = false;
                MenuItemReport3.Visible = false;
                MenuItemReport4.Visible = false;
            }
            else
            {
                MenuItemReport1.Visible = true;
                MenuItemReport2.Visible = true;
                MenuItemReport3.Visible = true;
                MenuItemReport4.Visible = true;
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            labelDateTime.Text = DateTime.Now.ToString("dd MMMM yyyy เวลา HH:mm:ss", _usCultureTh);
        }
        private void ButtonClos_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (MessageBox.Show("ยืนยันการออกจากโปรแกรม ", "ออกจากโปรแกรม", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (GlobalClass.IsConnectOnline == true)
                    {
                        ClassLogin login = new ClassLogin();
                        login.empId = GlobalClass.UserId.ToString();
                        login.userId = GlobalClass.UserIdLogin;
                        login.ipAddress = GlobalClass.IpConfig;
                        ClassUser clsLogin = arr.loadDataLogin(login, "logout");
                        if (clsLogin.result_code == "SUCCESS")
                        {
                            meaLog.WriteData("Success ==> System Connect :: Online | User :: " + GlobalClass.UserId, method, LogLevel.Debug);
                            Environment.Exit(0);
                        }
                        else
                        {
                            meaLogEvent.WriteDataEvent("Fail ==>  System Connect :: Online | User :: " + GlobalClass.UserId + " | Message :: " + clsLogin.result_message, method, LogLevel.Debug);
                            MessageBox.Show(clsLogin.result_message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        meaLog.WriteData("Success ==> System Connect :: Offline | User :: " + GlobalClass.UserId, method, LogLevel.Debug);
                        Environment.Exit(0);
                    }
                }
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
           
        }
        private void ButtonClos_MouseHover(object sender, EventArgs e)
        {
            ToolTip1.SetToolTip(this.ButtonClos, "จบโปรแกรม");
        }             
        private void MenuItemPayment_Click(object sender, EventArgs e)
        {
            if (GlobalClass.IsConnectOnline == true)
            {
                Form form = Find("FormArrears");
                form = null;
                if (form == null)
                {
                    //form = new FormArrears.FormArrearsNew();
                    form = new FormArrears.FormArrears();
                    form.MdiParent = this;
                    form.Dock = DockStyle.Fill;
                    form.FormBorderStyle = FormBorderStyle.None;
                    formControl.Add(form);
                }
                form.Show();
                form.BringToFront();
            }
            else
            {
                Form form = Find("FormOffline");
                form = null;
                if (form == null)
                {
                    form = new FormOffline.FormOfflineInput();
                    form.MdiParent = this;
                    form.Dock = DockStyle.Fill;
                    form.FormBorderStyle = FormBorderStyle.None;
                    formControl.Add(form);
                }
                form.Show();
                form.BringToFront();
            }
            MenuItem(1);        
        }
        private void MenuItemOther_Click(object sender, EventArgs e)
        {
            Form form = Find("FormOtherNew");
            form = null;
            if (form == null)
            {
                form = new FormOtherPayments.FormOtherNew();
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                formControl.Add(form);
            }
            MenuItem(2);
            form.Show();
            form.BringToFront();
        }
        private void MenuItemCancelContinue_Click(object sender, EventArgs e)
        {
            MenuItem(3);
            FormCancelReceipt cancel = new FormCancelReceipt();
            cancel.ShowDialog();            
        }
        private void MenuItemCancelReceipt_Click(object sender, EventArgs e)
        {
            MenuItem(4);
            FormCancelContinue cancel = new FormCancelContinue();
            cancel.ShowDialog();
            //cancel.Dispose();           
        }
        private void MenuItemRemittance_Click(object sender, EventArgs e)
        {
            Form form = Find("FormRemittanceSearch");
            form = null;
            if (form == null)
            {
                form = new ReportDoc.FormRemittanceSearch();
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                formControl.Add(form);
            }
            form.Show();
            form.BringToFront();
            MenuItem(5);
        }
        private void MenuItemReport1_Click(object sender, EventArgs e)
        {
            Form form = new Form();
            form = null;
            if (form == null)
            {
                form = new FormReport.FormSummaryCashdeskOffline();
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                formControl.Add(form);
            }
            form.Show();
            form.BringToFront();
            MenuItem(6);
        }
        private void MenuItemReport2_Click(object sender, EventArgs e)
        {
            Form form = new Form();
            form = null;
            if (form == null)
            {
                form = new FormReport.FormSummaryPaymentReport();
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                formControl.Add(form);
            }
            form.Show();
            form.BringToFront();
            MenuItem(7);
        }
        private void MenuItemReport3_Click(object sender, EventArgs e)
        {
            Form form = new Form();
            form = null;
            if (form == null)
            {
                form = new FormReport.FormRevertReport();
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                formControl.Add(form);
            }
            form.Show();
            form.BringToFront();
            MenuItem(8);
        }
        private void MenuItemReport4_Click(object sender, EventArgs e)
        {
            Form form = new Form();
            form = null;
            if (form == null)
            {
                form = new FormReport.FormChequeReport();
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                formControl.Add(form);
            }
            form.Show();
            form.BringToFront();
            MenuItem(9);
        }
        public void MenuItem(int idItem)
        {
            MenuItemPayment.BackColor = SystemColors.Control;
            MenuItemOther.BackColor = SystemColors.Control;
            MenuItem2.BackColor = SystemColors.Control;
            MenuItemCancelContinue.BackColor = SystemColors.Control;
            MenuItemCancelReceipt.BackColor = SystemColors.Control;
            MenuItemReport.BackColor = SystemColors.Control;
            MenuItemRemittance.BackColor = SystemColors.Control;
            MenuItemReport1.BackColor = SystemColors.Control;
            MenuItemReport2.BackColor = SystemColors.Control;
            MenuItemReport3.BackColor = SystemColors.Control;
            MenuItemReport4.BackColor = SystemColors.Control;
            MenuItem7.BackColor = SystemColors.Control;
            MenuItemCloseCard.BackColor = SystemColors.Control;

            if (idItem == 1)
                MenuItemPayment.BackColor = Color.FromArgb(255, 165, 51);
            else if (idItem == 2)
                MenuItemOther.BackColor = Color.FromArgb(255, 165, 51);
            else if (idItem == 3)
            {
                MenuItemCancelContinue.BackColor = Color.FromArgb(255, 165, 51);
                MenuItem2.BackColor = Color.FromArgb(255, 165, 51);
            }
            else if (idItem == 4)
            {
                MenuItemCancelReceipt.BackColor = Color.FromArgb(255, 165, 51);
                MenuItem2.BackColor = Color.FromArgb(255, 165, 51);
            }
            else if (idItem == 5)
            {
                MenuItemReport.BackColor = Color.FromArgb(255, 165, 51);
                MenuItemRemittance.BackColor = Color.FromArgb(255, 165, 51);
            }
            else if (idItem == 6)
            {
                MenuItemReport.BackColor = Color.FromArgb(255, 165, 51);
                MenuItemReport1.BackColor = Color.FromArgb(255, 165, 51);
            }
            else if (idItem == 7)
            {
                MenuItemReport.BackColor = Color.FromArgb(255, 165, 51);
                MenuItemReport2.BackColor = Color.FromArgb(255, 165, 51);
            }
            else if (idItem == 8)
            {
                MenuItemReport.BackColor = Color.FromArgb(255, 165, 51);
                MenuItemReport3.BackColor = Color.FromArgb(255, 165, 51);
            }
            else if (idItem == 9)
            {
                MenuItemReport.BackColor = Color.FromArgb(255, 165, 51);
                MenuItemReport4.BackColor = Color.FromArgb(255, 165, 51);
            }
            else if (idItem == 10)
            {
                MenuItemReport.BackColor = Color.FromArgb(255, 165, 51);
                MenuItemInquiryPayment.BackColor = Color.FromArgb(255, 165, 51);
            }
            else
            {
                MenuItem7.BackColor = Color.FromArgb(255, 165, 51);
                MenuItemCloseCard.BackColor = Color.FromArgb(255, 165, 51);
            }
        }
        private void MenuItemInquiryPayment_Click(object sender, EventArgs e)
        {
            Form form = Find("FormSearchData");
            form = null;
            if (form == null)
            {
                form = new FormArrears.FormSearchData();
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                formControl.Add(form);
            }
            form.Show();
            form.BringToFront();            
            MenuItem(10);
        }
    }
}
