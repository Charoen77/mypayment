﻿namespace MyPayment
{
    partial class UserControlTextbox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Textbox = new System.Windows.Forms.TextBox();
            this.LabelHolder = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Textbox
            // 
            this.Textbox.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.Textbox.Location = new System.Drawing.Point(0, 0);
            this.Textbox.Margin = new System.Windows.Forms.Padding(2);
            this.Textbox.Name = "Textbox";
            this.Textbox.Size = new System.Drawing.Size(270, 37);
            this.Textbox.TabIndex = 1;
            this.Textbox.TextChanged += new System.EventHandler(this.Textbox_TextChanged);
            // 
            // LabelHolder
            // 
            this.LabelHolder.AutoSize = true;
            this.LabelHolder.BackColor = System.Drawing.Color.White;
            this.LabelHolder.Font = new System.Drawing.Font("TH Sarabun New", 17.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LabelHolder.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.LabelHolder.Location = new System.Drawing.Point(9, 2);
            this.LabelHolder.Name = "LabelHolder";
            this.LabelHolder.Size = new System.Drawing.Size(83, 31);
            this.LabelHolder.TabIndex = 2;
            this.LabelHolder.Text = "กรุณากรอก";
            this.LabelHolder.Click += new System.EventHandler(this.LabelHolder_Click);
            // 
            // UserControlTextbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LabelHolder);
            this.Controls.Add(this.Textbox);
            this.Name = "UserControlTextbox";
            this.Size = new System.Drawing.Size(270, 37);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Textbox;
        private System.Windows.Forms.Label LabelHolder;
    }
}
