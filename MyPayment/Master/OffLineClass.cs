﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;
using System.IO.Compression;
using System.IO;
using MyPayment.Models;
using System.Globalization;
using System.Security.Principal;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;
using aejw.Network;
using System.Security.Cryptography;

namespace MyPayment.Master
{
    class OffLineClass
    {

        // public static string FilePath = @"E:\MEA\TestOffline\";
        public static string FilePath = AppDomain.CurrentDomain.BaseDirectory + @"\Offline\";
        public const string SecurityKey = "MEA-MyPayment";
        public string CreateBillNumber()
        {
            try
            {
                string strResult = DateTime.Now.ToString("ddMMyy") + GlobalClass.UserCode;
                int BillIndex = GlobalClass.BillIndex;
                if (BillIndex == 0)
                {
                    string strDate = DateTime.Now.ToString("yyyyMMdd");
                    var objData = ReadAllOffLineFileFromCSV(strDate);
                    if (objData == null || objData.Count == 0)
                    {
                        BillIndex = 1;
                    }
                    else
                    {
                        string strMaxCode = objData.OrderByDescending(m => m.BillNo).FirstOrDefault().BillNo;
                        //BillIndex = int.Parse(strMaxCode.Substring(13, 3));
                        BillIndex = int.Parse(strMaxCode.Substring(strMaxCode.Length - 3));
                        BillIndex++;
                    }
                    GlobalClass.BillIndex = BillIndex;
                    strResult = strResult + BillIndex.ToString("D3");
                }
                else
                {
                    BillIndex++;
                    GlobalClass.BillIndex = BillIndex;
                    strResult = strResult + BillIndex.ToString("D3");
                }




                return strResult;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public ItemCodeData ReadQrCode(string strQrCode)
        {
            try
            {
                //strQrCode = @"|099400016520011
                //              012306566000018320
                //              000665203674180162
                //              816351";


                //strQrCode = @"|099400016520011012306566000018320000665203674180162816351";

                //09940001652000001230656600665203674111801620001832816351 Std.Barcode
                strQrCode = (strQrCode.IndexOf("|") > -1 ? "" : "|") + strQrCode;
                Random random = new Random();
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string random_decimal = "0123456789";
                string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

                ItemCodeData Rec = new ItemCodeData();
                //  strQrCode = strQrCode.Replace(" ", "").Replace("\r\n","#");
                string Suffix = strQrCode.Substring(14, 2);

                string TaxId = "";
                string ContractAccount = "";
                string PowerUnit = "";
                string InvoiceNumber = "";
                string DebtCode = "";
                string DueDate = "";
                string Amount = "";
                string BillNumber = "";
                if (Suffix == "11")
                {
                    TaxId = strQrCode.Substring(1, 13);
                    ContractAccount = strQrCode.Substring(16, 9);
                    InvoiceNumber = strQrCode.Substring(34, 12);
                    DueDate = strQrCode.Substring(46, 6);
                    Amount = strQrCode.Substring(52, strQrCode.Length - 52);
                    Amount = Amount.Insert(Amount.Length - 2, ".");
                    BillNumber = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                    PowerUnit = strQrCode.Substring(25, 8);

                    Rec.Selected = true;
                    Rec.BillNumber = BillNumber;
                    Rec.TaxId = TaxId;
                    Rec.ContractAccount = ContractAccount;
                    Rec.InvoiceNumber = InvoiceNumber;
                    Rec.DueDate = DueDate.Substring(0, 2) + "/" + DueDate.Substring(2, 2) + "/" + DueDate.Substring(4, 2);
                    Rec.AmountExVat = Amount;
                    Rec.PowerUnit = PowerUnit;
                    Rec.AmountExVat = double.Parse(Rec.AmountExVat).ToString("N2");

                }
                return Rec;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ItemCodeData ReadStdBarCode(string strQrCode)
        {
            try
            {
                //strQrCode = @"|099400016520011
                //              012306566000018320
                //              000665203674180162
                //              816351";


                //    strQrCode = "09940001652000001230656600665203674111801620001832816351";


                Random random = new Random();
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string random_decimal = "0123456789";
                string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                ItemCodeData Rec = new ItemCodeData();
                strQrCode = "|" + strQrCode.Replace(" ", "").Replace("\r\n", "#").Trim();
                string ServiceCode = strQrCode.Substring(36, 2);

                string TaxId = "";
                string ContractAccount = "";
                string PowerUnit = "";
                string InvoiceNumber = "";
                string DebtCode = "";
                string DueDate = "";
                string Amount = "";
                string BillNumber = "";

                if (ServiceCode == "11")
                {


                    strQrCode = strQrCode.Replace("|", "");

                    TaxId = strQrCode.Substring(0, 13);

                    ContractAccount = strQrCode.Substring(15, 9);
                    InvoiceNumber = strQrCode.Substring(24, 11);
                    DueDate = strQrCode.Substring(37, 6);
                    PowerUnit = strQrCode.Substring(45, 6);
                    BillNumber = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                    Amount = strQrCode.Substring(50, strQrCode.Length - 50);
                    Amount = Amount.Insert(Amount.Length - 2, ".");

                    Rec.Selected = true;
                    Rec.BillNumber = BillNumber;
                    Rec.TaxId = TaxId;
                    Rec.ContractAccount = ContractAccount;
                    Rec.InvoiceNumber = InvoiceNumber;
                    Rec.DueDate = DueDate.Substring(0, 2) + "/" + DueDate.Substring(2, 2) + "/" + DueDate.Substring(4, 2);
                    Rec.AmountExVat = Amount;
                    Rec.PowerUnit = PowerUnit;
                    Rec.AmountExVat = double.Parse(Rec.AmountExVat).ToString("N2");


                }
                return Rec;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ItemCodeData TestGenOfflineInputRec()
        {
            try
            {
                ItemCodeData Rec = new ItemCodeData();

                Random random = new Random();
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string random_decimal = "0123456789";
                string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";


                DateTime RandomDay()
                {
                    DateTime start = new DateTime(2019, 9, 27);
                    int range = (DateTime.Today.AddDays(5) - start).Days;
                    return start.AddDays(random.Next(range));
                }


                Rec.DueDate = RandomDay().ToString("dd/MM/yyyy");
                Rec.Selected = true;
                Rec.BillNumber = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.ContractAccount = new string(Enumerable.Repeat(random_decimal, 9).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.TaxId = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.InvoiceNumber = new string(Enumerable.Repeat(random_decimal, 12).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.AmountExVat = new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.PowerUnit = new string(Enumerable.Repeat(random_decimal, 3).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.AmountExVat = double.Parse(Rec.AmountExVat).ToString("N2");
                Rec.ServiceType = "A";
                return Rec;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string ToStringMoney(string strData)
        {
            try
            {
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string StringMoneyToDouble(string strData)
        {
            try
            {
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public void ReadOffLineFileFromCSV()
        {
            try
            {


                ItemOfflineModel UserModel = new ItemOfflineModel();
                TextReader readFile = new StreamReader(@"c:\\textwriter.csv");
                UserModel = CsvSerializer.DeserializeFromReader<ItemOfflineModel>(readFile);

            }
            catch (Exception ex)
            {

            }
        }

        public List<ItemOfflineModel> ReadAllOffLineFileFromCSV(string strDate)
        {
            try
            {

                //string strPath = @"E:\MEA\TestOffline\" + strDate;
                string strPath = FilePath + @"\" + strDate;

                List<ItemOfflineModel> Result = new List<ItemOfflineModel>();

                string[] files = System.IO.Directory.GetFiles(strPath, "*.csv").Where(m => m.Contains("OFFLINE")).ToArray();
                List<ItemOfflineModel> LstItemModel = new List<ItemOfflineModel>();
                TextReader readFile;
                int RowNo = 0;
                foreach (var path in files)
                {
                    RowNo++;
                    readFile = new StreamReader(path);
                    LstItemModel = CsvSerializer.DeserializeFromReader<List<ItemOfflineModel>>(readFile);
                    //  ItemModel.RowNo = RowNo.ToString();
                    Result.AddRange(LstItemModel);
                }

                return Result;

            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public List<ItemOfflineLogToServiceModel> ReadOfflineLogToServiceCSV(string strDate)
        {
            try
            {
                string strPath = @"E:\MEA\TestOffline\" + strDate;
                List<ItemOfflineLogToServiceModel> Result = new List<ItemOfflineLogToServiceModel>();

                string[] files = System.IO.Directory.GetFiles(strPath, "*.csv").Where(m => m.Contains("SERVICE")).ToArray();
                List<ItemOfflineLogToServiceModel> ItemModel = new List<ItemOfflineLogToServiceModel>();
                TextReader readFile;
                int RowNo = 0;
                foreach (var path in files)
                {
                    RowNo++;
                    readFile = new StreamReader(path);
                    ItemModel = CsvSerializer.DeserializeFromReader<List<ItemOfflineLogToServiceModel>>(readFile);
                    //ItemModel.RowNo = RowNo.ToString();
                    Result.AddRange(ItemModel);
                }

                return Result;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ItemOfflineModel> ReadCancelOffLineFileFromCSV(string strDate)
        {
            try
            {
                string strPath = FilePath + @"\" + strDate;
                List<ItemOfflineModel> Result = new List<ItemOfflineModel>();
                //string[] files = System.IO.Directory.GetFiles(@"\\172.20.14.147\MeaShare", "*.csv");
                string[] files = System.IO.Directory.GetFiles(strPath, "*.csv").Where(m => m.Contains("CANCEL")).ToArray();
                ItemOfflineModel ItemModel = new ItemOfflineModel();
                TextReader readFile;
                foreach (var path in files)
                {
                    readFile = new StreamReader(path);
                    ItemModel = CsvSerializer.DeserializeFromReader<ItemOfflineModel>(readFile);

                    Result.Add(ItemModel);
                }

                return Result;

            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public ItemOfflineSumBanknote GetOfflineSumBanknote(string strDate)
        {
            try
            {
                ItemOfflineSumBanknote Result = new ItemOfflineSumBanknote();
                List<ItemOfflineModel> CashData = GetSummaryCashdeskOfflineReport(strDate);
                List<ItemtSummaryChequeModel> ChequeData = GetSummaryChequeReport(strDate);

                string Cash = CashData.Where(m => m.Cash != null).Sum(m => double.Parse(m.Cash)).ToString("N2");
                string Cheque = ChequeData.Where(m => m.Cheque != null).Sum(m => double.Parse(m.Cheque)).ToString("N2");
                Result.Cash = Cash;
                Result.Cheque = Cheque;

                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region WriteFile

        public void WriteOffLineToCSV_Bak(ItemOfflineModel itemModel)
        {
            try
            {

                string strDate = DateTime.Now.ToString("yyyyMMdd");

                string strPath = FilePath + strDate;
                string strFileName = "OFFLINE_" + itemModel.ContractAccount + "_" + itemModel.InvoiceNumber;




                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }

                TextWriter writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");

                CsvSerializer.SerializeToWriter(itemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }

        public void WriteCancelOffLineToCSV(ItemtRevertTaxInvoiceModel itemModel)
        {
            try
            {

                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 543;
                }
                string strDate = year.ToString() + DateTime.Now.ToString("MMdd");

                string strPath = FilePath + strDate;
                string strFileName = "CANCEL_" + itemModel.InvoiceNumber;




                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }



                TextWriter writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");

                CsvSerializer.SerializeToWriter(itemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }

        public void WriteChequeOffLineToCSV(List<ItemtSummaryChequeModel> LstItemModel, string strDateTime)
        {
            try
            {

                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 543;
                }
                string strDate = year.ToString() + DateTime.Now.ToString("MMdd");

                string strPath = FilePath + strDate;

                string strFileName = "CHEQUE_" + strDateTime;
                TextWriter writer;


                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }

                writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");
                CsvSerializer.SerializeToWriter(LstItemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;


            }
            catch (Exception ex)
            {

            }
        }

        public void WriteOffLineLogToServiceToCSV(List<ItemOfflineLogToServiceModel> LstItemModel)
        {
            try
            {
                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 543;
                }
                string strDate = year.ToString() + DateTime.Now.ToString("MMdd");

                string strPath = FilePath + strDate;
                ItemOfflineLogToServiceModel itemModel = LstItemModel.FirstOrDefault();
                string strFileName = "";
                TextWriter writer;


                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                strFileName = "SERVICE_" + itemModel.ContractAccount + "_" + itemModel.InvoiceNumber + "_" + itemModel.Transact;
                writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");
                CsvSerializer.SerializeToWriter(LstItemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }


        public void WriteOffLineToCSV(List<ItemOfflineModel> LstItemModel, string strDateTime)
        {
            try
            {

                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 543;
                }
                string strDate = year.ToString() + DateTime.Now.ToString("MMdd");

                string strPath = FilePath + strDate;
                string strFileName = "OFFLINE_" + strDateTime;



                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }

                TextWriter writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");

                CsvSerializer.SerializeToWriter(LstItemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }
        public bool WriteOfflineTxtLog()
        {
            try
            {
                List<ItemOfflineLogToServiceModel> LstItemOffline = ReadOfflineLogToServiceCSV("20191002");
                string strItem = "";
                string TransactionType = "";
                string PaymentDate = "";
                string ServiceType = "";
                string ContractAccount = "";
                string RefNumber = "";
                string TaxInvoice = "";
                string Amount = "";
                string Interest = "";
                string DayInterest = "";
                string RefDbOps = "";

                strItem = @"@MEA" + Environment.NewLine;
                foreach (var item in LstItemOffline)
                {
                    strItem = strItem + "|";

                    TransactionType = "CSH";
                    PaymentDate = item.PaymentDate;
                    ServiceType = "A";
                    ContractAccount = item.ContractAccount.PadLeft(9, '0');
                    RefNumber = "XBLNR";
                    TaxInvoice = item.BillNo;
                    Amount = (item.AmountIncVat.IndexOf('.') > -1 ? item.AmountIncVat : item.AmountIncVat + ".00");
                    Interest = "xxxx.xx";
                    DayInterest = "yy";
                    RefDbOps = "zzzz";

                    strItem = strItem + TransactionType + "|" + PaymentDate + "|" + ServiceType + "|" + ContractAccount + "|" + RefNumber + "|" + TaxInvoice + "|" + Amount + "|" + Interest + "|" + DayInterest + "|" + RefDbOps + Environment.NewLine;
                }
                strItem = strItem + "#";
                /****************************************************/

                string strDate = DateTime.Now.ToString("yyyyMMdd");

                string strPath = FilePath + strDate;
                string strFileName = "LOG_" + strDate + ".txt";




                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }

                File.AppendAllText(strPath + "\\" + strFileName, strItem);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string ConvertPaymentDateForService(string strData)
        {
            try
            {
                string strYear = "";
                string strResult = "";
                DateTime myDate = DateTime.ParseExact(strData, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (myDate.Year > 2100)
                {
                    strYear = (myDate.Year - 543).ToString();
                }
                else
                {
                    strYear = myDate.Year.ToString();
                }
                strResult = myDate.ToString("ddMM") + strYear;
                return strResult;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        #endregion

        #region GetReport
        public List<ItemtSummaryChequeModel> GetSummaryChequeReport(string strDate)
        {
            try
            {
                string strPath = FilePath + strDate;
                List<ItemtSummaryChequeModel> Result = new List<ItemtSummaryChequeModel>();
                string[] files = System.IO.Directory.GetFiles(strPath, "*.csv").Where(m => m.ToUpper().Contains("CHEQUE")).ToArray();
                List<ItemtSummaryChequeModel> ItemModel = new List<ItemtSummaryChequeModel>();
                TextReader readFile;
                foreach (var path in files)
                {
                    readFile = new StreamReader(path);
                    ItemModel = CsvSerializer.DeserializeFromReader<List<ItemtSummaryChequeModel>>(readFile);

                    Result.AddRange(ItemModel);
                }

                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ItemOfflineModel> GetSummaryCashdeskOfflineReport(string strDate)
        {
            List<ItemOfflineModel> LstOfflineAll = new List<ItemOfflineModel>();
            try
            {
                LstOfflineAll = ReadAllOffLineFileFromCSV(strDate);
                return LstOfflineAll;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public SummaryPaymentObj GetSummaryPaymentReport(string strDate)
        {
            List<ItemSummaryPaymentModel> LstISPM = new List<ItemSummaryPaymentModel>();
            List<ItemOfflineModel> LstOfflineAll = new List<ItemOfflineModel>();
            ItemSummaryPaymentModel RecModel = new ItemSummaryPaymentModel();
            SummaryPaymentObj ResultObj = new SummaryPaymentObj();

            decimal sumCash = 0;
            decimal sumCashChange = 0;
            decimal sumCredit = 0;
            decimal sumCheque = 0;
            decimal sumChequeChange = 0;
            decimal sumTotal = 0;
            decimal sumGainLoss = 0;
            try
            {
                LstOfflineAll = ReadAllOffLineFileFromCSV(strDate);
                string Branch = LstOfflineAll.FirstOrDefault().Branch;
                string CashierNo = LstOfflineAll.FirstOrDefault().CashierNo;
                string UserId = LstOfflineAll.FirstOrDefault().UserId;
                var LstOfflineCheque = GetSummaryChequeReport(strDate);


                DateTime PaymentDate = DateTime.ParseExact(strDate, "yyyyMMdd", null);

                sumCash = LstOfflineAll.Where(m => m.Cash != null).Sum(m => decimal.Parse(m.Cash));
                sumCashChange = LstOfflineAll.Where(m => m.CashChange != null).Sum(m => decimal.Parse(m.CashChange));

                sumCredit = 0;
                sumCheque = LstOfflineCheque.Where(m => m.Cheque != null).Sum(m => decimal.Parse(m.Cheque));
                sumChequeChange = LstOfflineCheque.Where(m => m.ChequeChange != null).Sum(m => decimal.Parse(m.ChequeChange));
                sumGainLoss = LstOfflineAll.Where(m => m.GainLoss != null).Sum(m => decimal.Parse(m.GainLoss));
                sumTotal = sumCash + sumCredit + sumCheque;

                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "วันที่รับชำระ";
                RecModel.Description = PaymentDate.ToString("dd/MM/yyyy");
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "เขต";
                RecModel.Description = Branch;
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "โต๊ะ";
                RecModel.Description = CashierNo;
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รหัสพนักงาน";
                RecModel.Description = UserId;
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รับเช็ค";
                RecModel.Description = string.Format("{0:n}", sumCheque);
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "เงินทอนเช็ค";
                RecModel.Description = string.Format("{0:n}", sumChequeChange);
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รับเงินสด";
                RecModel.Description = string.Format("{0:n}", sumCash);
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รวม Gain(+)/Loss(-)";
                RecModel.Description = string.Format("{0:n}", sumGainLoss);
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รับเครดิต";
                RecModel.Description = string.Format("{0:n}", sumCredit);
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รวม";
                RecModel.Description = string.Format("{0:n}", sumTotal);
                LstISPM.Add(RecModel);

                /*****************************************/
                ItemSummaryPaymentModel_Display objDisplay = new ItemSummaryPaymentModel_Display();
                objDisplay.UserId = UserId;
                objDisplay.PaymentDate = PaymentDate.ToString("dd/MMyyyy");
                objDisplay.Branch = Branch;
                objDisplay.CashierNo = CashierNo;
                objDisplay.SumCheque = string.Format("{0:n}", sumCheque);
                objDisplay.SumChequeChange = string.Format("{0:n}", sumChequeChange);
                objDisplay.SumCash = string.Format("{0:n}", sumCash);
                objDisplay.SumGainLoss = string.Format("{0:n}", sumGainLoss);
                objDisplay.SumCredit = string.Format("{0:n}", sumCredit);
                objDisplay.SumTotal = string.Format("{0:n}", sumTotal);

                ResultObj.Display = new List<ItemSummaryPaymentModel_Display>();
                ResultObj.Display.Add(objDisplay);
                /*****************************************/
                ResultObj.Report = LstISPM;

                return ResultObj;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ItemtRevertTaxInvoiceModel> GetRevertReport(string strDate)
        {
            List<ItemOfflineModel> LstOfflineAll = new List<ItemOfflineModel>();
            try
            {
                string strPath = FilePath + strDate;
                List<ItemtRevertTaxInvoiceModel> Result = new List<ItemtRevertTaxInvoiceModel>();
                string[] files = System.IO.Directory.GetFiles(strPath, "*.csv").Where(m => m.ToUpper().Contains("CANCEL")).ToArray();
                ItemtRevertTaxInvoiceModel ItemModel = new ItemtRevertTaxInvoiceModel();
                TextReader readFile;
                foreach (var path in files)
                {
                    readFile = new StreamReader(path);
                    ItemModel = CsvSerializer.DeserializeFromReader<ItemtRevertTaxInvoiceModel>(readFile);

                    Result.Add(ItemModel);
                }

                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        #endregion


        #region Update Program
        public void MapNetworkDrive()
        {
            NetworkDrive oNetDrive = new NetworkDrive();
            try
            {
                //oNetDrive.Force = false;
                //oNetDrive.Persistent = true;
                //oNetDrive.LocalDrive = "Z:";
                //oNetDrive.PromptForCredentials = false;
                //oNetDrive.ShareName = @"\\10.118.6.153\C$";
                //oNetDrive.SaveCredentials = false;
                ////match call to options provided
                //oNetDrive.MapDrive("Administrator", "P@ssw0rd");

                RemoveMapNetworkDrive();


                string DRIVE = ConfigurationManager.AppSettings["DRIVE"].ToString();
                string SHARENAME = ConfigurationManager.AppSettings["SHARENAME"].ToString();
                string USERNAME = ConfigurationManager.AppSettings["USERNAME"].ToString();
                string PASSWORD = ConfigurationManager.AppSettings["PASSWORD"].ToString();

                oNetDrive.Force = false;
                oNetDrive.Persistent = true;
                oNetDrive.LocalDrive = DRIVE;
                oNetDrive.PromptForCredentials = false;
                oNetDrive.ShareName = @SHARENAME;
                oNetDrive.SaveCredentials = false;
                //match call to options provided
                oNetDrive.MapDrive(USERNAME, PASSWORD);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void RemoveMapNetworkDrive()
        {
            NetworkDrive oNetDrive = new NetworkDrive();
            try
            {
                if (System.IO.DriveInfo.GetDrives().Where(m => m.Name == @"Z:\").Any())
                {
                    oNetDrive.Force = false;
                    oNetDrive.LocalDrive = "Z:";
                    oNetDrive.UnMapDrive();
                }


            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);
            }
        }

        public bool isVersionUpdate()
        {
            try
            {
                MapNetworkDrive();

                string LOCAL_VERSION = ConfigurationManager.AppSettings["VERSION"].ToString();
                string URL_UPDATE = ConfigurationManager.AppSettings["URL_UPDATE"].ToString();
                string FileVersion = URL_UPDATE + @"\Version.txt";
                var LstLine = File.ReadAllLines(FileVersion).ToList();
                string txtVersion = "Version:";
                var ServerVersion = LstLine.Where(m => m.Contains(txtVersion)).FirstOrDefault();
                if (ServerVersion != null && ServerVersion != "")
                {
                    ServerVersion = ServerVersion.Replace(txtVersion, "").Trim();
                    if (String.Compare(ServerVersion, LOCAL_VERSION) > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }




        #endregion

        #region Encrypt Data

        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader =
                                                new AppSettingsReader();
            // Get the key from config file

            //string key = (string)settingsReader.GetValue("SecurityKey",typeof(String));

            string key = SecurityKey;

            //System.Windows.Forms.MessageBox.Show(key);
            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            System.Configuration.AppSettingsReader settingsReader =
                                                new AppSettingsReader();
            //Get your key from config file to open the lock!
            //  string key = (string)settingsReader.GetValue("SecurityKey",typeof(String));
            string key = SecurityKey;

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static string Base64Encode(string strData)
        {
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(strData);
                string base64 = Convert.ToBase64String(bytes);
                return base64;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string Base64Decode(string strData)
        {
            try
            {

                byte[] bytes = Convert.FromBase64String(strData);
                string str = Encoding.UTF8.GetString(bytes);
                return str;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string EncryptToken(string strData)
        {
            try
            {
                string Token = Encrypt(strData, true);
                Token = Base64Encode(Token);
                return Token;
            }
            catch (Exception ex)
            {
                return "";

            }
        }
        public static string DecryptToken(string Token)
        {
            try
            {
                string strData = Base64Decode(Token);
                strData = Decrypt(strData, true);
                return strData;
            }
            catch (Exception ex)
            {
                return "";

            }
        }
        #endregion

        #region Login Offline

        public bool LoginOffline(string userName, string password)
        {
            try
            {
                string strAppPath = AppDomain.CurrentDomain.BaseDirectory;
                System.Collections.Generic.IEnumerable<String> lines = File.ReadLines(strAppPath + @"\\sys.txt");

                string Ref1 = lines.ToArray()[0];
                string Ref2 = lines.ToArray()[1];
                string Ref3 = lines.ToArray()[2];

                Ref2 = Ref2.Replace("Ref2=", "");
                Ref3 = Ref3.Replace("Ref3=", "");

                // string key = Encrypt("admin", true);


                string sysPassword = Decrypt(Ref3, true);

                if (password == sysPassword)
                {
                    return true;
                }

                return false;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

    }
}
